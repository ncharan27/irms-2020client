import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from '../constants/constants';
import { Toast } from '../core/decorators/toast.decorator';

@Injectable()
export class AccountService {


  private url = `${BASE_URL}api/account`;
  constructor(private http: HttpClient) {
  }

  recoverPasswordByEmail(model: any) {
    const url = `${this.url}/recoverPassword/emailWithCaptcha`;
    return this.http.post(url, model);
  }

  recoverPasswordBySms(model: any) {
    const url = `${this.url}/recoverPassword/smsWithCaptcha`;
    return this.http.post(url, model);
  }

  resendCodeBySms(model: any) {
    const url = `${this.url}/recoverPassword/sms`;
    return this.http.post(url, model);
  }

  getTokenByCode(model: any) {
    const url = `${this.url}/getTokenByCode`;
    return this.http.post(url, model);
  }

  setNewPassword(model: any) {
    const url = `${this.url}/setNewPassword`;
    return this.http.post(url, model);
  }

  verifyOTP(model: any) {
    const url = `${this.url}/verifyOTP`;
    return this.http.post(url, model);
  }

  @Toast('OPT Code sent.')
  resendOtp() {
    const url = `${this.url}/resendOTP`;
    return this.http.get(url);
  }
}
