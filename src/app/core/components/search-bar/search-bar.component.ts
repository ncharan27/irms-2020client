import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FuseConfigService } from '../../services/config.service';
import { Subscription } from 'rxjs/Subscription';
import { QueryService } from '../../../main/content/services/query.service';

@Component({
    selector: 'fuse-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class FuseSearchBarComponent implements OnInit {
    collapsed: boolean;
    toolbarColor: string;
    @Output() onInput: EventEmitter<any> = new EventEmitter();
    onSettingsChanged: Subscription;
    queryString: string;

    constructor(
        private fuseConfig: FuseConfigService,
        private queryService: QueryService
    ) {
        this.collapsed = true;
        this.onSettingsChanged =
            this.fuseConfig.onSettingsChanged
                .subscribe(
                    (newSettings) => {
                        this.toolbarColor = newSettings.colorClasses.toolbar;
                    }
                );
    }

    ngOnInit() {

    }

    collapse() {
        this.collapsed = true;
        this.queryString = '';
        //   this.queryService.resetFilters();

        this.onInput.emit('');
        this.queryService.resetFilters = undefined;
    }

    expand() {
        this.collapsed = false;
    }

    search(event) {
        const value = event.target.value;

        this.onInput.emit(value);
    }

}
