import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterKeyword'
})
export class FilterKeywordPipe implements PipeTransform {
  transform(items: any[], searchText: string, property: string = 'label'): any[] {
    if (!items) { return []; }
    if (!searchText) { return items; }
    searchText = searchText.toLowerCase();
    return items.filter( it => {
      return it[property].toLowerCase().includes(searchText);
    });
   }
}
