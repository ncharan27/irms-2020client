import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { AuthGuard } from './auth/guard/auth.guard';
import { RoleType, Section } from './constants/constants';
import { AccountModule } from './account/account.module';
import { MainComponent } from './main/main.component';
import { ModuleWithProviders } from '@angular/core';
import { SelectivePreloadingStrategy } from './auth/selective-preloading-strategy';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: `/${Section.Dashboard}`,
    pathMatch: 'full'
  },
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: `${Section.Profile}`,
        loadChildren: () => import('app/main/content/user-profile/user-profile.module').then(m => m.UserProfileModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
      },
      {
        path: `${Section.Dashboard}`,
        loadChildren: () => import('app/main/content/dashboard/dashboard.module').then(m => m.DashboardModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
      },
      {
        path: `${Section.Events}`,
        loadChildren: () => import('app/main/content/event/event.module').then(m => m.EventModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
      },
      {
        path: `${Section.GlobalGuest}`,
        canLoad: [AuthGuard],
        loadChildren: () => import('app/main/content/global-guest/global-guest.module').then(m => m.GlobalGuestModule),
        data: {expectedRoles: [RoleType.TenantAdmin]}
      },
      {
        path: `${Section.GlobalContact}`,
        loadChildren: () => import('app/main/content/global-contact/global-contact.module').then(m => m.GlobalContactModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
      }
    ],
    data: { preload: true, delay: 0, expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `${Section.Response}`,
        loadChildren: () => import('app/main/content/response/response.module').then(m => m.ResponseModule),
  },
  {
    path: `${Section.RfiResponse}`,
        loadChildren: () => import('app/main/content/rfi-response/rfi-response.module').then(m => m.RfiResponseModule),
  },
  {
    path: `${Section.EmailDesigner}`,
        loadChildren: () => import('app/main/content/email-designer/email-designer.module').then(m => m.EmailDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `data-review/${Section.EmailDesigner}`,
        loadChildren: () => import('app/main/content/email-designer/email-designer.module').then(m => m.EmailDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `${Section.DataReviewFormDesigner}`,
        loadChildren: () => import('app/main/content/data-review-form-designer/data-review-form-designer.module').then(m => m.DataReviewFormDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `${Section.SmsDesigner}`,
        loadChildren: () => import('app/main/content/sms-designer/sms-designer.module').then(m => m.SmsDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `data-review/${Section.SmsDesigner}`,
        loadChildren: () => import('app/main/content/sms-designer/sms-designer.module').then(m => m.SmsDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `${Section.WhatsappDesigner}`,
        loadChildren: () => import('app/main/content/whatsapp-designer/whatsapp-designer.module').then(m => m.WhatsappDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `data-review/${Section.WhatsappDesigner}`,
        loadChildren: () => import('app/main/content/whatsapp-designer/whatsapp-designer.module').then(m => m.WhatsappDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `${Section.FormDesigner}`,
        loadChildren: () => import('app/main/content/form-designer/form-designer.module').then(m => m.FormDesignerModule),
        canLoad: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] }
  },
  {
    path: `account`,
    loadChildren: 'app/account/account.module#AccountModule',
    data: { preload: true, delay: 0 }
  },
  {
    path: '**',
    redirectTo: ''
  }
];

export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: 'always'
};
export const appRoutingModule: ModuleWithProviders = RouterModule.forRoot(
  appRoutes,
  routingConfiguration //,
  // {
  //   preloadingStrategy: SelectivePreloadingStrategy
  // }
);

