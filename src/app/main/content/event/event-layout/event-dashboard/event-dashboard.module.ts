import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventDashboardComponent } from './event-dashboard.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { RouterModule } from '@angular/router';

const routes = [{
  path: '',
  component: EventDashboardComponent, 
  canLoad: [AuthGuard], 
  data: { expectedRoles: [RoleType.TenantAdmin] },
}];

@NgModule({
declarations: [
  EventDashboardComponent
],
imports: [
  MainModule,
  ContentModule,
  CommonModule,
  RouterModule.forChild(routes)
]
})
export class EventDashboardModule { }
