import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiListComponent } from './event-data-campaign-rfi-list.component';

describe('EventDataCampaignRfiListComponent', () => {
  let component: EventDataCampaignRfiListComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
