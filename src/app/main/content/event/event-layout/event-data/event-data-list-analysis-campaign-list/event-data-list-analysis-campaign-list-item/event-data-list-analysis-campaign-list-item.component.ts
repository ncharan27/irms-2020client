import { Component, Input, OnInit } from '@angular/core';
import { campaignStatus } from 'app/constants/constants';

@Component({
    selector: 'irms-event-data-list-analysis-campaign-list-item',
    templateUrl: './event-data-list-analysis-campaign-list-item.component.html',
    styleUrls: ['./event-data-list-analysis-campaign-list-item.component.scss']
})
export class EventDataListAnalysisCampaignListItemComponent implements OnInit {

    @Input() data;
    status = campaignStatus;
    constructor() { }

    ngOnInit() {
        this.data.title = JSON.parse(this.data.title).title;
    }
}
