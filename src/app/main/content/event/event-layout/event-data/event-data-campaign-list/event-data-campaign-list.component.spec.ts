import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignListComponent } from './event-data-campaign-list.component';

describe('EventDataCampaignListComponent', () => {
  let component: EventDataCampaignListComponent;
  let fixture: ComponentFixture<EventDataCampaignListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
