import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignListItemComponent } from './event-data-campaign-list-item.component';

describe('EventDataCampaignListItemComponent', () => {
  let component: EventDataCampaignListItemComponent;
  let fixture: ComponentFixture<EventDataCampaignListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
