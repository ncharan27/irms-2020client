import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from 'app/core/animations';
import { EventDataDataService } from '../event-data.data.service';
import { EventDataModService } from '../event-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'irms-event-data-campaign-rfi-response-details',
  templateUrl: './event-data-campaign-rfi-response-details.component.html',
  styleUrls: ['./event-data-campaign-rfi-response-details.component.scss'],
  animations: fuseAnimations
})
export class EventDataCampaignRfiResponseDetailsComponent implements OnInit {
  guestName = 'Guest';
  response;
  isDataLoaded = false;
  pollTypes = ['select', 'selectSearch'];
  listTypes = ['singleLineText', 'multilineText', 'date', 'phone', 'email'];
  constructor(protected route: ActivatedRoute,
              protected dataService: EventDataDataService,
              protected sectionService: EventDataModService,
              public datepipe: DatePipe) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.dataService.getGuestResponse({
        contactId: params.rid,
        formId: params.fid
      })
      .subscribe(result => {
        result.responseData = result.responseData.map(x => {
          const type = JSON.parse(x.question).type;
          x.question = JSON.parse(x.question);
          if (type === 'date'){
            x.questionResponse = new Date(x.questionResponse).toString() === 'Invalid Date' ? new Date(null) : new Date(x.questionResponse);
            x.questionResponse = this.datepipe.transform(x.questionResponse, 'dd/MM/yyyy');
          }
          if (x.question.type === 'selectSearch') {
            x.questionResponse = JSON.parse(x.questionResponse);
          }
          return x;
        });
        this.isDataLoaded = true;
        this.guestName = result.guestName;
        this.response = result;
      });
    });
  }
  
  emptyArray(num): any {
    return new Array(parseInt(num, 10));
  }

}
