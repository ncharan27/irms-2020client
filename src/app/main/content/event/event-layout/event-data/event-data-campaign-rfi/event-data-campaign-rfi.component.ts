import { Component, OnInit } from '@angular/core';
import { EventDataDataService } from '../event-data.data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {Location} from '@angular/common';
import { fuseAnimations } from '@fuse/animations';
import { EventDataModService } from '../event-data.service';
@Component({
  selector: 'irms-event-data-campaign-rfi',
  templateUrl: './event-data-campaign-rfi.component.html',
  styleUrls: ['./event-data-campaign-rfi.component.scss'],
  animations: fuseAnimations
})
export class EventDataCampaignRfiComponent implements OnInit {
  formName = 'RFI Form';
  responsesCount = 0;
  public tabGroup = [
    {
      label: 'Summary',
      link: './summary',
      icon: 'timeline'
    },
    {
      label: 'Responses',
      link: './responses',
      icon: 'insert_drive_file'

    }

  ];
  rfiType: any;
  constructor(protected route: ActivatedRoute,
              private router: Router,
              protected location: Location,
              private dataService: EventDataDataService,
              private service: EventDataModService) { }

  ngOnInit() {
    this.service.getResponsesCount()
      .subscribe(x => this.responsesCount = x);

    this.route.params.subscribe((params: Params) => {
      this.rfiType = params.rfiType;
      this.service.targetRFIFormId = params.fid;
      this.dataService.getRfiFormName({
        id: params.fid
      })
      .subscribe(result => {
        this.formName = result.name;
        this.service.setResponsesCount(result.responsesCount)
      });
    });
  }

}
