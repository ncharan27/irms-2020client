import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-event-data-campaign-rfi-responses-item',
  templateUrl: './event-data-campaign-rfi-responses-item.component.html',
  styleUrls: ['./event-data-campaign-rfi-responses-item.component.scss']
})
export class EventDataCampaignRfiResponsesItemComponent implements OnInit {
  @Input() data;
  constructor() { }

  ngOnInit() {
  }

}
