import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventDataCampaignRfiResponsesExportModalComponent } from './event-data-campaign-rfi-responses-export-modal.component';

describe('EventDataCampaignRfiResponsesExportModalComponent', () => {
  let component: EventDataCampaignRfiResponsesExportModalComponent;
  let fixture: ComponentFixture<EventDataCampaignRfiResponsesExportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventDataCampaignRfiResponsesExportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDataCampaignRfiResponsesExportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
