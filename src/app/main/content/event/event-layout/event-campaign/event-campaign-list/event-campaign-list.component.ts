import { Component, OnInit, OnDestroy } from '@angular/core';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { EventCampaignModel } from '../event-campaign.model';
import { IResponseModel } from 'types';
import { EventCampaignService } from '../event-campaign.service';
import { EventCampaignDataService } from '../event-campaign-data.service';
import { Section } from 'app/constants/constants';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { fuseAnimations } from 'app/core/animations';
import { EventCampaignListItemComponent } from './event-campaign-list-item/event-campaign-list-item.component';
import { of } from 'rxjs';

@Component({
  selector: 'irms-event-campaign-list',
  templateUrl: './event-campaign-list.component.html',
  styleUrls: ['./event-campaign-list.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignListComponent extends SectionListComponent<EventCampaignModel> implements OnInit, OnDestroy {

  listHeaders = ['Campaign', 'Delivery', 'Accepted', 'Rejected', 'Pending'];
  public listItemComponent: Object = EventCampaignListItemComponent;

  isPopulated = true;
  isLoading = true;

  sub: any;
  constructor(public sectionService: EventCampaignService,
              protected dataService: EventCampaignDataService,
              router: Router, public route: ActivatedRoute) {
    super(Section.EventCampaigns, sectionService, dataService, router);
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe((params: Params) => {
      this.sectionService.setFilter({ eventId: params['id'] });
      super.ngOnInit();
      this.sectionService.doRefreshList();
      this.sub = this.data.subscribe(x => {
        if (x && x.hasOwnProperty('items')) {
          if (x.items.length > 0) {
            this.isPopulated = true;
            
          }
          
          setTimeout(() => this.isLoading = false, 1000);
        }
        

      });
    });
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    this.isPopulated = false;
    this.isLoading = false;

    super.ngOnDestroy();
  }
}

