import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from 'app/core/animations';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { EventCampaignModel } from '../event-campaign.model';
import { EventCampaignService } from '../event-campaign.service';
import { EventCampaignDataService } from '../event-campaign-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Section } from 'app/constants/constants';

@Component({
  selector: 'irms-event-campaign-create',
  templateUrl: './event-campaign-create.component.html',
  styleUrls: ['./event-campaign-create.component.scss'],
  animations: fuseAnimations
})
export class EventCampaignCreateComponent extends SectionCreateComponent<EventCampaignModel> {
  eventId: any;
  whatsappLoading = false;
  rsvpLoading = false;

  constructor(public sectionService: EventCampaignService,
    protected dataService: EventCampaignDataService,
    protected router: Router,
    public dialog: MatDialog,
    protected route: ActivatedRoute) {
    super(Section.EventCampaigns, sectionService, dataService, router);
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];
    });
  }

  create(): void {
    const model = { eventId: this.eventId };

    this.rsvpLoading = true;
    this.dataService.create(model).subscribe(id => {
      this.rsvpLoading = false;
      this.router.navigate([`../${id}/edit/config`], { relativeTo: this.route });
    });
  }

  whatsAppOptIn(): void {
    const model = { eventId: this.eventId };

    this.whatsappLoading = true;
    this.dataService.create(model).subscribe(id => {
      this.whatsappLoading = false;
      this.router.navigate([`../${id}/whatsapp`], { relativeTo: this.route });
    });
  }

}
