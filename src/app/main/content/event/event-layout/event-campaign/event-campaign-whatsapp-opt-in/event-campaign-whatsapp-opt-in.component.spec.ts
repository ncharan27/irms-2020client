import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignWhatsappOptInComponent } from './event-campaign-whatsapp-opt-in.component';

describe('EventCampaignWhatsappOptInComponent', () => {
  let component: EventCampaignWhatsappOptInComponent;
  let fixture: ComponentFixture<EventCampaignWhatsappOptInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignWhatsappOptInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignWhatsappOptInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
