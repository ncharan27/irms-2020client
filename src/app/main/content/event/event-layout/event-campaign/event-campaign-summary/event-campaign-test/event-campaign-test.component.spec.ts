import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignTestComponent } from './event-campaign-test.component';

describe('EventCampaignTestComponent', () => {
  let component: EventCampaignTestComponent;
  let fixture: ComponentFixture<EventCampaignTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
