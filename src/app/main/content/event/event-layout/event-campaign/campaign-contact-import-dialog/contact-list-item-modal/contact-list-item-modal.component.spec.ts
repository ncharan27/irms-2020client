import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactListItemModalComponent } from './contact-list-item-modal.component';

describe('ContactListItemModalComponent', () => {
  let component: ContactListItemModalComponent;
  let fixture: ComponentFixture<ContactListItemModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactListItemModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactListItemModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
