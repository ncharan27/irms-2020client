import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { QueryService } from 'app/main/content/services/query.service';
import { CampaignInvitationAcceptedDataService } from './campaign-invitation-accepted-data.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationAcceptedService extends SectionService<EventCampaignModel> {

  constructor(
    protected dataService: CampaignInvitationAcceptedDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
