import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedMessageConfigComponent } from './rejected-message-config.component';

describe('RejectedMessageConfigComponent', () => {
  let component: RejectedMessageConfigComponent;
  let fixture: ComponentFixture<RejectedMessageConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectedMessageConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedMessageConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
