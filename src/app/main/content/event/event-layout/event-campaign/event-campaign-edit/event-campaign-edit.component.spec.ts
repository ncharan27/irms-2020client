import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCampaignEditComponent } from './event-campaign-edit.component';

describe('EventCampaignEditComponent', () => {
  let component: EventCampaignEditComponent;
  let fixture: ComponentFixture<EventCampaignEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCampaignEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCampaignEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
