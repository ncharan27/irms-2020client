import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, SubscriptionLike } from 'rxjs';
import { Validators, FormBuilder } from '@angular/forms';
import { timePeriodUnits, Section, sideDialogConfig, invitationWorkflowStep } from 'app/constants/constants';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EventCampaignEditService } from '../../event-campaign-edit.service';
import { CampaignConfigurationsService } from '../../campaign-configurations/campaign-configurations.service';
import { CampaignConfigurationsDataService } from '../../campaign-configurations/campaign-configurations-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { EventCampaignModel } from '../../../event-campaign.model';
import { CampaignInvitationRejectedDataService } from '../campaign-invitation-rejected-data.service';
import { CampaignInvitationRejectedService } from '../campaign-invitation-rejected.service';
import { CampaignFlowNodesService } from '../../campaign-flow-nodes/campaign-flow-nodes.service';
import { CampaignRfiDesignModalComponent } from '../../campaign-invitation/campaign-rfi-design-modal/campaign-rfi-design-modal.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';

@Component({
  selector: 'irms-rejected-message-config',
  templateUrl: './rejected-message-config.component.html',
  styleUrls: ['./rejected-message-config.component.scss']
})
export class RejectedMessageConfigComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit, OnDestroy, CanExit {

  subscriptions: Subscription[] = [];
  isFirstNode = true;
  isLoaded = false;
  step = invitationWorkflowStep.Rejected;
  public rejectedForm = this.fb.group({
    name: ['', [Validators.required]],
    isInstant: [false, [Validators.required]],
    delay: this.fb.group({
      interval: [0],
      intervalType: [timePeriodUnits.Minutes, [Validators.required]]
    }),
  });
  campaignId: any;

  smsTemplate: any;
  emailTemplate: any;
  rfiForm: any;
  whatsappTemplate: any;
  activeMedia: any;

  constructor(protected fb: FormBuilder,
              private dialog: MatDialog,
              public service: EventCampaignEditService,
              public configurationService: CampaignConfigurationsService,
              public sectionService: CampaignInvitationRejectedService,
              protected dataService: CampaignInvitationRejectedDataService,
              protected router: Router,
              protected route: ActivatedRoute,
              protected nodeService: CampaignFlowNodesService,
              public animDialog: DialogAnimationService) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
  }
  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.isLoaded = false;
      this.dataService.get(params['rid']).subscribe(m => {
        this.isLoaded = true;
        this.model = m;
        if (m) {
          this.rejectedForm.controls['name'].setValue(m.title);
          this.isDataLoaded = true;
          this.patchModelInForms();
          this.activeMedia = m.hasOwnProperty('activeMedia') ? m.activeMedia : {sms: true, whatsapp: true, email: true};
          this.smsTemplate = this.service.proceedSmsTemplate(m);
          this.emailTemplate = this.service.proceedEmailTemplate(m);
          this.whatsappTemplate = this.service.proceedWhatsappTemplate(m);
          this.rfiForm = this.service.proceedRfiTemplate(m);
        }
      });
    });

    this.rejectedForm.get('isInstant').valueChanges.takeUntil(this.componentDestroyed).subscribe((value: boolean) => {
      if (!value) {
        this.rejectedForm.controls.delay.get('interval').setValidators([Validators.required, Validators.min(1)]);
        this.rejectedForm.controls.delay.get('interval').updateValueAndValidity();
      } else {
        this.rejectedForm.controls.delay.get('interval').clearValidators();
        this.rejectedForm.controls.delay.get('interval').updateValueAndValidity();
      }
    });

    // save form data on parent save button
    this.subscriptions.push(this.service.getCampaignSave().subscribe(() => {
      this.rejectedForm.markAllAsTouched();
      this.save();
    }));
    /// is first node
    this.isFirstNode = this.nodeService.firstNode;
    this.subscriptions.push(this.nodeService.isFirstNode().subscribe(flag => {
      this.isFirstNode = flag;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  canDeactivate(): any {
    if (this.rejectedForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

  // patch form
  patchModelInForms() {
    this.rejectedForm.patchValue(this.model);
    if (!this.model['isInstant']) {
      this.rejectedForm.controls.delay.patchValue(this.model);
    } else {
      this.rejectedForm.controls.delay.patchValue({interval: 0, intervalType: 2});
    }
  }

  /// save 
  save(): void {
    if (this.rejectedForm.valid) {
      this.sectionService.loading = true;
      const model = this.rejectedForm.controls.delay.value;
      model.id = this.model.id;
      model.isInstant = this.rejectedForm.value.isInstant;
      if (model.isInstant) {
        model.interval = 0;
        model.intervalType = timePeriodUnits.Minutes;
      }
      model.title = this.rejectedForm.controls['name'].value;

      this.dataService.create(model).subscribe(() => {
        this.rejectedForm.markAsPristine();
        this.sectionService.loading = false;
        this.sectionService.setFilter({ campaignId: this.campaignId });
        this.sectionService.doRefreshList();
        this.configurationService.updateNodes.next();
      });
    }
  }

  /// Create RFI form
  createForm(): void {
    // this.router.navigateByUrl(`/form-designer/${this.model.id}`);
        const dialogConfig: any = sideDialogConfig;
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.minWidth = '80vw';
        dialogConfig.data = {
          id: this.emailTemplate.id
        };

        const dialogRef = this.animDialog.open(CampaignRfiDesignModalComponent, dialogConfig);
    // CampaignRfiDesignModalComponent
  }

}
