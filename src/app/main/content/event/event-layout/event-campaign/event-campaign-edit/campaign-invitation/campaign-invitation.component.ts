import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EventCampaignEditService } from '../event-campaign-edit.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { EventCampaignModel } from '../../event-campaign.model';
import { invitationWorkflowStep, Section } from 'app/constants/constants';
import { CampaignInvitationService } from './campaign-invitation.service';
import { CampaignInvitationDataService } from './campaign-invitation-data.service';
import { CampaignConfigurationsDataService } from '../campaign-configurations/campaign-configurations-data.service';

@Component({
  selector: 'irms-campaign-invitation',
  templateUrl: './campaign-invitation.component.html',
  styleUrls: ['./campaign-invitation.component.scss']
})
export class CampaignInvitationComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit, OnDestroy, CanExit {

  campaignId: string;
  invitationId: string;
  now = new Date();

  isLoaded = false;
  step = invitationWorkflowStep.Invitation;
  public invitationForm = this.fb.group({
    isInstant: [true, [Validators.required]],
    startDate: [''],
    emailTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      subject: '',
      sender: '',
    }),
    smsTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      sender: '',
    }),
    whatsappTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      sender: '',
    })
  });
  subscription: Subscription;
  activeMedia: any;
  constructor(protected fb: FormBuilder,
              protected route: ActivatedRoute,
              protected router: Router,
              private dialog: MatDialog,
              private campaignDataService: CampaignConfigurationsDataService,
              private campaignService: EventCampaignEditService,
              public service: EventCampaignEditService,
              public sectionService: CampaignInvitationService,
              protected dataService: CampaignInvitationDataService) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
  }

  ngOnInit(): void {
    // save form data form global save
    this.subscription = this.service.getCampaignSave().subscribe(x => {
      this.invitationForm.markAllAsTouched();
      this.save();
    });

    this.isLoaded = false;
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.reloadCampaignMetadata();
      this.reloadCampaignName();
    });
    this.invitationForm.get('isInstant').valueChanges.takeUntil(this.componentDestroyed).subscribe((value: boolean) => {
      if (!value) {
        this.setValidators('invitationForm', 'startDate', [Validators.required]);
      } else {     
        this.invitationForm.get('startDate').reset();
        this.invitationForm.get('startDate').markAsPristine();       
        this.clearValidators('invitationForm', ['startDate']);        
        this.invitationForm.get('startDate').setErrors(null); 
        this.invitationForm.updateValueAndValidity();
      }
    });

    this.invitationForm.get('startDate').valueChanges.takeUntil(this.componentDestroyed).subscribe(value => {
      const now = new Date();
      const enteredDate = new Date(value);
      if (enteredDate < now) {
        this.invitationForm.get('startDate').setErrors({ dateError: true });
      }
    });
  }

  // If needed
  reloadCampaignName(): void {
    if (!this.campaignService.getInstantCampaignName()) {
      this.campaignDataService.get(this.campaignId).subscribe(x => {
        this.campaignService.setCampaignName(x.name);
      });
    }
  }

  reloadCampaignMetadata(): void {
    this.dataService.get(this.campaignId).subscribe(m => {
      this.isLoaded = true;
      this.model = m;
      if (m) {
        this.invitationId = this.model.id;
        this.invitationForm.controls.emailTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.emailTemplate['controls']['sender'].setValue(m.emailSender);
        this.invitationForm.controls.emailTemplate['controls']['subject'].setValue(m.emailSubject);
        this.invitationForm.controls.emailTemplate['controls']['image'].setValue(m.emailImage);
        this.invitationForm.controls.smsTemplate['controls']['sender'].setValue(m.smsSender);
        this.invitationForm.controls.smsTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.smsTemplate['controls']['image'].setValue(m.smsImage);
        // whatsapp below
        this.invitationForm.controls.whatsappTemplate['controls']['sender'].setValue(m.whatsappSender);
        this.invitationForm.controls.whatsappTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.whatsappTemplate['controls']['image'].setValue(m.whatsappImage);
        this.isDataLoaded = true;
        this.activeMedia = m.hasOwnProperty('activeMedia') ? m.activeMedia : {sms: true, whatsapp: true, email: true};
        if (!m.isInstant) {
          this.setValidators('invitationForm', 'startDate', [Validators.required]);
          this.invitationForm.get('startDate').markAsTouched();
        } else {
          m.startDate = null;
        }
        this.patchModelInForms();
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  canDeactivate(): any {
    if (this.invitationForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

  patchModelInForms(): void {
    this.invitationForm.patchValue(this.model);
  }

  /// Save 
  save(mediaType: string = ''): void {
    // mediaType param used when invitation id not present, so as to route to mediaType invitation editor after saving
    // call Save APIs
    if (this.invitationForm.valid) {
      const model = this.sectionService.trimValues(this.invitationForm.value);
      model.id = this.invitationId;
      model.campaignId = this.campaignId;
      // this.sectionService.loading = true;
      this.dataService.createInvitation(model).subscribe(id => {
        // this.sectionService.loading = false;
        this.invitationId = id.toString();
        this.invitationForm.controls.emailTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.smsTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.whatsappTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.markAsPristine();

        //// Route to media type editor if invitation just created
        if (mediaType === 'email') {
          this.router.navigate([`/email-designer/${this.invitationId}/design`]);
        }
        if (mediaType === 'sms') {
          this.router.navigate([`/sms-designer/${this.invitationId}/rsvp-form`]);
        }
        if (mediaType === 'whatsapp') {
          this.router.navigate([`/whatsapp-designer/${this.invitationId}/rsvp-form`]);
        }
      }, () => {
        // this.sectionService.loading = false;
      });
    }
  }

}
