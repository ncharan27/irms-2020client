import { Component, OnInit, OnDestroy } from '@angular/core';
import { MediaType, timePeriodUnits, Section, invitationWorkflowStep } from 'app/constants/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { EventCampaignEditService } from '../../event-campaign-edit.service';
import { Subscription } from 'rxjs';
import { CampaignConfigurationsService } from '../../campaign-configurations/campaign-configurations.service';
import { CampaignConfigurationsDataService } from '../../campaign-configurations/campaign-configurations-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { EventCampaignModel } from '../../../event-campaign.model';
import { CampaignInvitationPendingDataService } from '../campaign-invitation-pending-data.service';
import { CampaignInvitationPendingService } from '../campaign-invitation-pending.service';
import { CampaignFlowNodesService } from '../../campaign-flow-nodes/campaign-flow-nodes.service';

@Component({
  selector: 'irms-pending-reminder-config',
  templateUrl: './pending-reminder-config.component.html',
  styleUrls: ['./pending-reminder-config.component.scss']
})
export class PendingReminderConfigComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit, OnDestroy, CanExit {

  subscriptions: Subscription[] = [];
  isFirstNode;
  data: any;
  isLoaded = false;
  step = invitationWorkflowStep.Pending;
  public pendingForm = this.fb.group({
    name: ['', [Validators.required]],
    delay: this.fb.group({
      interval: ['', [Validators.required, Validators.min(1)]],
      intervalType: [timePeriodUnits.Minutes, [Validators.required]]
    }),
  });

  campaignId: any;

  smsTemplate: any;
  emailTemplate: any;
  whatsappTemplate: any;
  activeMedia: any;

  constructor(protected fb: FormBuilder,
              private dialog: MatDialog,
              public service: EventCampaignEditService,
              public configurationService: CampaignConfigurationsService,
              public sectionService: CampaignInvitationPendingService,
              protected dataService: CampaignInvitationPendingDataService,
              protected router: Router,
              protected route: ActivatedRoute,
              protected nodeService: CampaignFlowNodesService) {
    super(Section.EventCampaigns, sectionService, dataService, router, route);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.isLoaded = false;
      this.campaignId = params['cid'];
      this.dataService.get(params['rid']).subscribe(m => {
        this.isLoaded = true;
        this.model = m;
        this.data = m;
        if (m) {
          this.pendingForm.controls['name'].setValue(m.title);
          this.isDataLoaded = true;
          this.patchModelInForms();
          this.activeMedia = m.hasOwnProperty('activeMedia')? m.activeMedia: {sms:true, whatsapp:true, email:true}
          this.smsTemplate = this.service.proceedSmsTemplate(m);
          this.emailTemplate = this.service.proceedEmailTemplate(m);
          this.whatsappTemplate = this.service.proceedWhatsappTemplate(m);

        }
      });
    });

    // save form data form global save
    this.subscriptions.push(this.service.getCampaignSave().subscribe(x => {
      this.markFormGroupTouched(this.pendingForm);
      this.save();
    }));

    /// is first node
    this.isFirstNode = this.nodeService.firstNode;
    this.subscriptions.push(this.nodeService.isFirstNode().subscribe(flag => {
      this.isFirstNode = flag;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  canDeactivate(): any {
    if (this.pendingForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

  patchModelInForms() {
    this.pendingForm.controls.delay.patchValue(this.data);
    if(!this.data.interval) {
      this.pendingForm.controls.delay.patchValue({interval: 0, intervalType: 2});
    }
  }

  /// save 
  save(): void {
    if (this.pendingForm.valid) {
      this.sectionService.loading = true;
      const model = this.pendingForm.controls.delay.value;
      model.id = this.model.id;
      model.title = this.pendingForm.controls['name'].value;

      this.dataService.create(model).subscribe(response => {
        this.pendingForm.markAsPristine();
        this.sectionService.refreshList();
        this.sectionService.setFilter({ campaignId: this.campaignId });
        this.configurationService.updateNodes.next();
        this.sectionService.loading = false;
      });
    }
  }
}
