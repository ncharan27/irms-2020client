import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { QueryService } from 'app/main/content/services/query.service';
import { CampaignInvitationPendingDataService } from './campaign-invitation-pending-data.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationPendingService extends SectionService<EventCampaignModel> {

  constructor(
    protected dataService: CampaignInvitationPendingDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
