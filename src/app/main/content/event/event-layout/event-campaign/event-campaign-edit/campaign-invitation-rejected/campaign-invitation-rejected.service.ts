import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { QueryService } from 'app/main/content/services/query.service';
import { CampaignInvitationRejectedDataService } from './campaign-invitation-rejected-data.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationRejectedService extends SectionService<EventCampaignModel> {

  constructor(
    protected dataService: CampaignInvitationRejectedDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
