import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { QueryService } from 'app/main/content/services/query.service';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { CampaignConfigurationsDataService } from './campaign-configurations-data.service';
import { EventCampaignModel } from '../../event-campaign.model';
import { Subscription, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignConfigurationsService extends SectionService<EventCampaignModel> {

  public updateNodes = new Subject();
  listId: string;

  constructor(
    protected dataService: CampaignConfigurationsDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
