import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'irms-campaign-rfi-design-modal',
  templateUrl: './campaign-rfi-design-modal.component.html',
  styleUrls: ['./campaign-rfi-design-modal.component.scss']
})
export class CampaignRfiDesignModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CampaignRfiDesignModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
