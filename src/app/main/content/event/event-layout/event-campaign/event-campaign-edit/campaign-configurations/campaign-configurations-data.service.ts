import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { EventCampaignModel } from '../../event-campaign.model';
import { IResponseModel } from 'types';

@Injectable({
  providedIn: 'root'
})
export class CampaignConfigurationsDataService implements SectionDataService<EventCampaignModel> {
 

  private url = `${BASE_URL}api/campaign`;
  private campaignInvitationUrl = `${BASE_URL}api/campaignInvitation`;
  private dictionaryUrl = `${BASE_URL}api/dictionary`;

  constructor(private http: HttpClient) { }

 addReminder(data): Observable<any> {
    return of({
      "id": "b9a371d8-72d4-4a27-8d19-be2df5607e93",
      "isInstant": false,
      "interval": 0,
      "title": "Message 1",
      "intervalType": null,
      "emailSubject": null,
      "emailSender": null,
      "smsSender": null,
      "whatsappSender": null,
      "formCreated": false,
      "formBackgroundPath": null,
      "smsImage": null,
      "emailImage": null,
      "whatsappImage": null,
      "startDate": "2020-08-19T08:13:06.990Z",
    });
  }
  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}`);
  }

  @Toast('Successfully updated')
  update(model) {
    return this.http.put(`${this.url}`, model);
  }

  @Toast('Successfully updated')
  createInvitation(model) {
    return this.http.post(`${this.campaignInvitationUrl}`, model);
  }

  checkReachability(listId): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${listId}/check-reachability`);
  }

  ///preffered media
  loadPrefferedMediaStats(campaignId, listId): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${campaignId}/${listId}/campaign-preffered-media`);
  }

  updateGuestsPreferredMedia(model): Observable<any> {
    return this.http.put<any>(`${this.url}/prefferedMedia`, model);
  }

  //lookups
  getCampaignCriteriaTypes(): Observable<any> {
    return this.http.get<any>(`${this.dictionaryUrl}/campaign-criteria-type`);
  }

  getguestLists(id): Observable<any> {
    return this.http.get<any>(`${this.dictionaryUrl}/${id}/event-guest-list`);
  }

  getList(filterParam): Observable<IResponseModel> {
    return of();
  }

  getContactsReachabilityList(filterParam): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/contacts-reachability`, filterParam);
  }

  getContactsWithoutReachabilityList(filterParam): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/contacts-without-reachability`, filterParam);
  }

  collectInfoPrefMedia(model: any) { //Update API for Pref Media in Collect Info for List Analysis
    return this.http.post<IResponseModel>(`${this.url}/API-HERE`, model);
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }
  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  
}
