import { Injectable } from '@angular/core';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationViewRejectedDataService implements SectionDataService<EventCampaignModel>{

  private url = `${BASE_URL}api/CampaignInvitation`;
  constructor(private http: HttpClient) { }

  public get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/rejected/${id}`);
  }

  public getList(campaignId: any): Observable<any> {
    return this.http.get(`${this.url}/${campaignId}/rejected`);
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }

  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
}
