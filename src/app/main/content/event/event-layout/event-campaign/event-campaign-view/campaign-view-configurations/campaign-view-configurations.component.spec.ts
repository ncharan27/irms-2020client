import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignViewConfigurationsComponent } from './campaign-view-configurations.component';

describe('CampaignViewConfigurationsComponent', () => {
  let component: CampaignViewConfigurationsComponent;
  let fixture: ComponentFixture<CampaignViewConfigurationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignViewConfigurationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewConfigurationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
