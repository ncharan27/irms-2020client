import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedViewMessageConfigComponent } from './rejected-view-message-config.component';

describe('RejectedViewMessageConfigComponent', () => {
  let component: RejectedViewMessageConfigComponent;
  let fixture: ComponentFixture<RejectedViewMessageConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectedViewMessageConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedViewMessageConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
