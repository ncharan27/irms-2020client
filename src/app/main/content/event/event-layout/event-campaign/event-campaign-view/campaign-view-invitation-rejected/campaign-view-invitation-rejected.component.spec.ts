import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignViewInvitationRejectedComponent } from './campaign-view-invitation-rejected.component';

describe('CampaignViewInvitationRejectedComponent', () => {
  let component: CampaignViewInvitationRejectedComponent;
  let fixture: ComponentFixture<CampaignViewInvitationRejectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignViewInvitationRejectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewInvitationRejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
