import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { Section } from 'app/constants/constants';
import { CampaignViewConfigurationsService } from './campaign-view-configurations.service';
import { CampaignViewConfigurationsDataService } from './campaign-view-configurations-data.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { EventCampaignViewService } from '../event-campaign-view.service';

@Component({
  selector: 'irms-campaign-view-configurations',
  templateUrl: './campaign-view-configurations.component.html',
  styleUrls: ['./campaign-view-configurations.component.scss']
})
export class CampaignViewConfigurationsComponent extends SectionViewEditComponent<EventCampaignModel> implements OnInit, OnDestroy {

  public configForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
    entryCriteriaTypeId: ['', [Validators.required]],
    guestListId: ['', [Validators.required]],
    exitCriteria: ['', [Validators.required]],
    exitCriteriaType: ['']
  });

  subscription: Subscription;
  campaignId;
  prefferedMediaStats: any;

  entryCriteria;
  exitCriteria;
  guestListName;


  constructor(
    protected fb: FormBuilder,
    private service: EventCampaignViewService,
    public sectionService: CampaignViewConfigurationsService,
    protected dataService: CampaignViewConfigurationsDataService,
    protected router: Router,
    protected route: ActivatedRoute,
    private dialog: MatDialog
  ) { 
    super(Section.EventCampaigns, sectionService, dataService, router, route);
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.dataService.get(params['cid']).subscribe(m => {
        this.model = m;
        if(m) {
          this.loadLookups(params['id'], m.entryCriteriaTypeId, m.guestListId);
          this.exitCriteria = m.exitCriteriaType;
          this.isDataLoaded = true;
          this.patchModelInForms();
        }
      });
    });
  }

  // load drop downs
  loadLookups(eventId, entryCriteriaTypeId, guestListId) {
    this.dataService.getCampaignCriteriaTypes().subscribe(result => {
      // this.entryCriteria = result.find(x=> x.id == entryCriteriaTypeId).value;
      this.entryCriteria = [{key: 1, value: result.find(x => x.id == entryCriteriaTypeId).value}]
      this.configForm.controls['entryCriteriaTypeId'].setValue(1);
    });
    this.dataService.getguestLists(eventId).subscribe(result => {
      this.guestListName = [{key: 1, value: result.find(x => x.id == guestListId).value}]
      this.configForm.controls['guestListId'].setValue(1);
    });
  }

  public patchModelInForms() {
    // emit campaign name
    this.service.setCampaignName(this.model.name);

    this.configForm.patchValue(this.model);
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

}
