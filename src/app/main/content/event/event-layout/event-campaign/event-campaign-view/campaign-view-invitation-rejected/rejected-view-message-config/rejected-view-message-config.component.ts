import { Component, OnInit } from '@angular/core';
import { CampaignInvitationViewRejectedDataService } from '../campaign-invitation-view-rejected-data.service';
import { CampaignConfigurationsDataService } from '../../../event-campaign-edit/campaign-configurations/campaign-configurations-data.service';
import { EventCampaignEditService } from '../../../event-campaign-edit/event-campaign-edit.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'irms-rejected-view-message-config',
  templateUrl: './rejected-view-message-config.component.html',
  styleUrls: ['./rejected-view-message-config.component.scss']
})
export class RejectedViewMessageConfigComponent implements OnInit {

  campaignId: string;
  invitationId: string;
  now = new Date();
  model;
  isLoaded = false;
  
  public invitationForm = this.fb.group({
    dateInfo: [{}],
    emailTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      subject: '',
      sender: '',
    }),
    smsTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      sender: '',
    }),
    whatsappTemplate: this.fb.group({
      id: this.invitationId,
      image: '',
      sender: '',
    }),
    statistics: this.fb.group({
      email: this.fb.group({
        triggered: 0,
        processed: 0,
        delivered: 0,
        opens: 0,
        uniqueOpens: 0,
        clicks: 0,
        uniqueClicks: 0,
        bounces: 0,
        spamReports: 0,
        blocks: 0
      }),
      sms: this.fb.group({
        triggered: 0,
        sent: 0,
        delivered: 0,
        unDelivered: 0,
        failed: 0
      }),
      whatsapp: this.fb.group({
        initiated: 0,
        sent: 0,
        delivered: 0,
        read: 0,
        failed: 0
      })
    })
  });
  subscription: Subscription;

  constructor(
    protected fb: FormBuilder,
    private campaignDataService: CampaignConfigurationsDataService,
    protected dataService: CampaignInvitationViewRejectedDataService,
    private campaignService: EventCampaignEditService,
    protected route: ActivatedRoute,
    protected router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.campaignId = params['cid'];
      this.invitationId = params['rid'];
      this.reloadCampaignMetadata();
    });
  }
  getStatistics() {
    const stats = this.invitationForm.controls['statistics'];
    return stats;
  }

  loadTemplates() {

  }
  
  reloadCampaignMetadata() {
    this.dataService.get(this.invitationId).subscribe(m => {
      this.isLoaded = true;
      this.model = m;
      if (m) {
        this.invitationId = this.model.id;
        this.invitationForm.controls.emailTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.emailTemplate['controls']['sender'].setValue(m.emailSender);
        this.invitationForm.controls.emailTemplate['controls']['subject'].setValue(m.emailSubject);
        this.invitationForm.controls.emailTemplate['controls']['image'].setValue(m.emailImage);
        this.invitationForm.controls.smsTemplate['controls']['sender'].setValue(m.smsSender);
        this.invitationForm.controls.smsTemplate['controls']['id'].setValue(this.invitationId);
        this.invitationForm.controls.smsTemplate['controls']['image'].setValue(m.smsImage);
        this.invitationForm.controls.whatsappTemplate['controls']['image'].setValue(m.whatsappImage);
        this.invitationForm.controls.whatsappTemplate['controls']['sender'].setValue(m.whatsappSender);
        
        this.isLoaded = true;
        this.patchModelInForms();
        this.invitationForm.controls['dateInfo'].setValue({
          isInstant: m.isInstant,
          interval: m.interval,
          intervalType: m.intervalType
        });
      }
    });
  }

  patchModelInForms() {
    this.invitationForm.patchValue(this.model);
  }

}
