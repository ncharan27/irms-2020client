import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignViewInvitationAcceptedComponent } from './campaign-view-invitation-accepted.component';

describe('CampaignViewInvitationAcceptedComponent', () => {
  let component: CampaignViewInvitationAcceptedComponent;
  let fixture: ComponentFixture<CampaignViewInvitationAcceptedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignViewInvitationAcceptedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignViewInvitationAcceptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
