import { Injectable } from '@angular/core';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionService } from 'app/main/content/components/shared/section/section.service';
import { CampaignViewConfigurationsDataService } from './campaign-view-configurations-data.service';
import { Router } from '@angular/router';
import { QueryService } from 'app/main/content/services/query.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignViewConfigurationsService extends SectionService<EventCampaignModel> {


  constructor(
    protected dataService: CampaignViewConfigurationsDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
   }
}
