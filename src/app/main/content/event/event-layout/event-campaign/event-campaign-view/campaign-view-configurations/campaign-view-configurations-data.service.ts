import { Injectable } from '@angular/core';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { IResponseModel } from 'types';

@Injectable({
  providedIn: 'root'
})
export class CampaignViewConfigurationsDataService implements SectionDataService<EventCampaignModel> {

  private url = `${BASE_URL}api/campaign`;
  private campaignInvitationUrl = `${BASE_URL}api/campaignInvitation`;
  private dictionaryUrl = `${BASE_URL}api/dictionary`;

  constructor(private http: HttpClient) { }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}`);
  }

  @Toast('Successfully updated')
  update(model) {
    return this.http.put(`${this.url}`, model);
  }

  @Toast('Successfully updated')
  createInvitation(model) {
    return this.http.post(`${this.campaignInvitationUrl}`, model);
  }

  checkReachability(listId): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${listId}/check-reachability`);
  }

  ///preffered media
  loadPrefferedMediaStats(campaignId, listId): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/${campaignId}/${listId}/campaign-preffered-media`);
  }

  updateGuestsPreferredMedia(model): Observable<any> {
    return this.http.put<any>(`${this.url}/prefferedMedia`, model);
  }

  //lookups
  getCampaignCriteriaTypes(): Observable<any> {
    return this.http.get<any>(`${this.dictionaryUrl}/campaign-criteria-type`);
  }

  getguestLists(id): Observable<any> {
    return this.http.get<any>(`${this.dictionaryUrl}/${id}/event-guest-list`);
  }

  getList(filterParam): Observable<IResponseModel> {
    return of();
  }

  getContactsReachabilityList(filterParam): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/contacts-reachability`, filterParam);
  }

  getContactsWithoutReachabilityList(filterParam): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/contacts-without-reachability`, filterParam);
  }
  
  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }
  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
}
