import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationViewAcceptedDataService implements SectionDataService<EventCampaignModel>{

  private url = `${BASE_URL}api/CampaignInvitation`;
  constructor(private http: HttpClient) { }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/accepted/${id}`);
  }

  getList(campaignId: any): Observable<any> {
    return this.http.get(`${this.url}/${campaignId}/accepted`);
  }

  create(model: any): Observable<string> {
    throw new Error("Method not implemented.");
  }

  delete(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
  update(model: any): Observable<any> {
    throw new Error("Method not implemented.");
  }
  
}
