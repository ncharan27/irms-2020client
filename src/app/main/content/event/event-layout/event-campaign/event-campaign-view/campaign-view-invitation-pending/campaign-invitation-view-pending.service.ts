import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CampaignInvitationViewPendingDataService } from './campaign-invitation-view-pending-data.service';
import { QueryService } from 'app/main/content/services/query.service';
import { EventCampaignModel } from '../../event-campaign.model';
import { SectionService } from 'app/main/content/components/shared/section/section.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignInvitationViewPendingService extends SectionService<EventCampaignModel>  {

  constructor(
    protected dataService: CampaignInvitationViewPendingDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
  }
}
