import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventCampaignComponent } from './event-campaign.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { RouterModule } from '@angular/router';
import { EventCampaignCreateComponent } from './event-campaign-create/event-campaign-create.component';
import { EventCampaignListComponent } from './event-campaign-list/event-campaign-list.component';
import { EventCampaignService } from './event-campaign.service';
import { EventCampaignDataService } from './event-campaign-data.service';
import { FuseNavigationModule } from '@fuse/components';
import { EventCampaignEditComponent } from './event-campaign-edit/event-campaign-edit.component';
import { CampaignConfigurationsComponent } from './event-campaign-edit/campaign-configurations/campaign-configurations.component';
import { CampaignInvitationComponent } from './event-campaign-edit/campaign-invitation/campaign-invitation.component';
import { CampaignInvitationPendingComponent } from './event-campaign-edit/campaign-invitation-pending/campaign-invitation-pending.component';
import { CampaignInvitationAcceptedComponent } from './event-campaign-edit/campaign-invitation-accepted/campaign-invitation-accepted.component';
import { CampaignInvitationRejectedComponent } from './event-campaign-edit/campaign-invitation-rejected/campaign-invitation-rejected.component';
import { CanExitGuard } from 'app/main/content/services/can-exit.guard';
import { EventCampaignListItemComponent } from './event-campaign-list/event-campaign-list-item/event-campaign-list-item.component';
import { CampaignFlowNodesComponent } from './event-campaign-edit/campaign-flow-nodes/campaign-flow-nodes.component';
import { PendingReminderConfigComponent } from './event-campaign-edit/campaign-invitation-pending/pending-reminder-config/pending-reminder-config.component';
import { AcceptedMessageConfigComponent } from './event-campaign-edit/campaign-invitation-accepted/accepted-message-config/accepted-message-config.component';
import { RejectedMessageConfigComponent } from './event-campaign-edit/campaign-invitation-rejected/rejected-message-config/rejected-message-config.component';
import { EventCampaignSummaryComponent } from './event-campaign-summary/event-campaign-summary.component';
import { EventCampaignViewComponent } from './event-campaign-view/event-campaign-view.component';
import { CampaignViewConfigurationsComponent } from './event-campaign-view/campaign-view-configurations/campaign-view-configurations.component';
import { CampaignViewInvitationRsvpComponent } from './event-campaign-view/campaign-view-invitation-rsvp/campaign-view-invitation-rsvp.component';
import { CampaignViewInvitationAcceptedComponent } from './event-campaign-view/campaign-view-invitation-accepted/campaign-view-invitation-accepted.component';
import { CampaignViewInvitationPendingComponent } from './event-campaign-view/campaign-view-invitation-pending/campaign-view-invitation-pending.component';
import { CampaignViewInvitationRejectedComponent } from './event-campaign-view/campaign-view-invitation-rejected/campaign-view-invitation-rejected.component';
import { AcceptedViewMessageConfigComponent } from './event-campaign-view/campaign-view-invitation-accepted/accepted-view-message-config/accepted-view-message-config.component';
import { PendingViewMessageConfigComponent } from './event-campaign-view/campaign-view-invitation-pending/pending-view-message-config/pending-view-message-config.component';
import { RejectedViewMessageConfigComponent } from './event-campaign-view/campaign-view-invitation-rejected/rejected-view-message-config/rejected-view-message-config.component';
import { InvitationMediaStatisticsGroupComponent } from 'app/main/content/components/shared/invitation-media-statistics-group/invitation-media-statistics-group.component';
import { InvitationViewMediaTemplateComponent } from 'app/main/content/components/shared/invitation-media-statistics-group/invitation-view-media-template/invitation-view-media-template.component';
import { EventCampaignTestComponent } from './event-campaign-summary/event-campaign-test/event-campaign-test.component';
import { CampaignContactImportDialogComponent } from './campaign-contact-import-dialog/campaign-contact-import-dialog.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { ContactListItemComponent } from '../event-guest/event-guest-import/select-guest/contact-list-item/contact-list-item.component';
import { ContactListItemModalComponent } from './campaign-contact-import-dialog/contact-list-item-modal/contact-list-item-modal.component';
import { EventCampaignWhatsappOptInComponent } from './event-campaign-whatsapp-opt-in/event-campaign-whatsapp-opt-in.component';
import { MediaTemplateDesignModule } from 'app/main/content/components/shared/media-template-design/media-template-design.module';
import { CampaignRfiDesignModalComponent } from './event-campaign-edit/campaign-invitation/campaign-rfi-design-modal/campaign-rfi-design-modal.component';
import { FormBuilderModule } from 'app/main/content/components/shared/form-builder/form-builder.module';
import { EventCampaignGoLiveModalComponent } from './event-campaign-summary/event-campaign-go-live-modal/event-campaign-go-live-modal.component';
import { PreferredMediaModule } from 'app/main/content/components/shared/preferred-media/preferred-media.module';
import { CampaignPreferredMediaDialogComponent } from 'app/main/content/components/shared/preferred-media/campaign-preferred-media-dialog/campaign-preferred-media-dialog.component';
import { PreferredMediaListItemComponent } from 'app/main/content/components/shared/preferred-media/campaign-preferred-media-dialog/preferred-media-list-item/preferred-media-list-item.component';

const routes = [
  {
    path: '',
    component: EventCampaignComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        component: EventCampaignListComponent,
      },
      {
        path: 'create',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventCampaignCreateComponent
      },
      {
        path: ':cid/whatsapp',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventCampaignWhatsappOptInComponent
      },
      {
        path: ':cid/edit',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventCampaignEditComponent,
        children: [
          {
            path: '',
            component: CampaignConfigurationsComponent,
            canDeactivate: [CanExitGuard]
          },
          {
            path: 'config',
            component: CampaignConfigurationsComponent,
            canDeactivate: [CanExitGuard]
          },
          {
            path: 'invitation',
            component: CampaignInvitationComponent,
            canDeactivate: [CanExitGuard]
          },
          {
            path: 'invitation-pending',
            component: CampaignInvitationPendingComponent,
            canDeactivate: [CanExitGuard],
            children: [
              {
                path: ':rid',
                component: PendingReminderConfigComponent,
                canDeactivate: [CanExitGuard]
              }
            ]
          },
          {
            path: 'invitation-accepted',
            component: CampaignInvitationAcceptedComponent,
            canDeactivate: [CanExitGuard],
            children: [
              {
                path: ':rid',
                component: AcceptedMessageConfigComponent,
                canDeactivate: [CanExitGuard]
              }
            ]
          },
          {
            path: 'invitation-rejected',
            component: CampaignInvitationRejectedComponent,
            canDeactivate: [CanExitGuard],
            children: [
              {
                path: ':rid',
                component: RejectedMessageConfigComponent,
                canDeactivate: [CanExitGuard]
              }
            ]
          },
          {
            path: 'done',
            component: EventCampaignSummaryComponent,
            canDeactivate: [CanExitGuard],
          },
        ]
      },
      {
        path: ':cid/summary',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventCampaignSummaryComponent,
        canDeactivate: [CanExitGuard]
      },
      {
        path: ':cid/view',
        canActivateChild: [AuthGuard],
        data: { expectedRoles: [RoleType.TenantAdmin] },
        component: EventCampaignViewComponent,
        children: [
          {
            path: '',
            component: CampaignViewConfigurationsComponent,
            canDeactivate: [CanExitGuard]
          },
          {
            path: 'config',
            component: CampaignViewConfigurationsComponent,
            canDeactivate: [CanExitGuard]
          },
          {
            path: 'invitation',
            component: CampaignViewInvitationRsvpComponent,
            canDeactivate: [CanExitGuard]
          },
          {
            path: 'invitation-pending',
            component: CampaignViewInvitationPendingComponent,
            canDeactivate: [CanExitGuard],
            children: [
              {
                path: ':rid',
                component: PendingViewMessageConfigComponent,
                canDeactivate: [CanExitGuard]
              }
            ]
          },
          {
            path: 'invitation-accepted',
            component: CampaignViewInvitationAcceptedComponent,
            canDeactivate: [CanExitGuard],
            children: [
              {
                path: ':rid',
                component: AcceptedViewMessageConfigComponent,
                canDeactivate: [CanExitGuard]
              }
            ]
          },
          {
            path: 'invitation-rejected',
            component: CampaignViewInvitationRejectedComponent,
            canDeactivate: [CanExitGuard],
            children: [
              {
                path: ':rid',
                component: RejectedViewMessageConfigComponent,
                canDeactivate: [CanExitGuard]
              }
            ]
          },
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    EventCampaignComponent,
    EventCampaignCreateComponent,
    EventCampaignListComponent,
    EventCampaignEditComponent,
    CampaignConfigurationsComponent,
    CampaignInvitationComponent,
    CampaignInvitationPendingComponent,
    CampaignInvitationAcceptedComponent,
    CampaignInvitationRejectedComponent,
    EventCampaignListItemComponent,
    CampaignFlowNodesComponent,
    PendingReminderConfigComponent,
    InvitationMediaStatisticsGroupComponent,
    InvitationViewMediaTemplateComponent,
    AcceptedMessageConfigComponent,
    RejectedMessageConfigComponent,
    EventCampaignSummaryComponent,
    EventCampaignViewComponent,
    CampaignViewConfigurationsComponent,
    CampaignViewInvitationRsvpComponent,
    CampaignViewInvitationAcceptedComponent,
    CampaignViewInvitationPendingComponent,
    CampaignViewInvitationRejectedComponent,
    AcceptedViewMessageConfigComponent,
    PendingViewMessageConfigComponent,
    RejectedViewMessageConfigComponent,
    EventCampaignTestComponent,
    CampaignContactImportDialogComponent,
    ContactListItemModalComponent,
    EventCampaignWhatsappOptInComponent,
    CampaignRfiDesignModalComponent,
    EventCampaignGoLiveModalComponent
  ],
  imports: [
    MainModule,
    ContentModule,
    CommonModule,
    FuseNavigationModule,
    MediaTemplateDesignModule,
    FormBuilderModule,
    PreferredMediaModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [EventCampaignGoLiveModalComponent,CampaignRfiDesignModalComponent,ContactListItemModalComponent,  CampaignContactImportDialogComponent, EventCampaignListItemComponent,CampaignPreferredMediaDialogComponent,PreferredMediaListItemComponent, EventCampaignTestComponent],
  exports: [RouterModule],
  providers: [
   DialogAnimationService, EventCampaignService, EventCampaignDataService
  ]
})
export class EventCampaignModule { }
