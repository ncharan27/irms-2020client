import { Component, OnInit } from '@angular/core';
import {eventNavigation} from './event-navigation';
@Component({
  selector: 'irms-event-navbar',
  templateUrl: './event-navbar.component.html',
  styleUrls: ['./event-navbar.component.scss']
})
export class EventNavbarComponent implements OnInit {
  navigation = eventNavigation;
  constructor() { }

  ngOnInit() {
  }

}
