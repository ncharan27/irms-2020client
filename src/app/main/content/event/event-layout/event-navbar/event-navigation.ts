import { FuseNavigation } from '@fuse/types';

export const eventNavigation: FuseNavigation[] = [
    {
        id: 'dashboard',
        title: 'Event dashboard',        
        translate: '',
        type: 'item',
        icon: 'event_note',
        url: `dashboard`
    },
    {
        id: 'campaign',
        title: 'Campaigns',        
        translate: '',
        type: 'item',
        icon: 'speaker_notes',
        url: `campaigns`
    },
    {
        id: 'data',
        title: 'Data',        
        translate: '',
        type: 'item',
        icon: 'folder_special',
        url: `data`
    },
    {
        id: 'guests',
        title: 'Event guests',        
        translate: '',
        type: 'item',
        icon: 'group',
        url: `guests`
    },
    {
        id: 'settings',
        title: 'Event settings',        
        translate: '',
        type: 'item',
        icon: 'settings',
        url: `settings`
    }
];
