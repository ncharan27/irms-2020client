import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventGuestImportFormService } from '../event-guest-import-form.service';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { EventGuestModel } from '../../event-guest.model';
import { Section } from 'app/constants/constants';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { IResponseModel } from 'types';
import { ContactListItemComponent } from './contact-list-item/contact-list-item.component';
import { fuseAnimations } from 'app/core/animations';
import { map } from 'rxjs/operators';
import { ContactsGroupedListService } from 'app/main/content/components/shared/contacts-grouped-list/contacts-grouped-list.service';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { appInjector } from 'app/main/app-injector';

@Component({
  selector: 'irms-select-guest',
  templateUrl: './select-guest.component.html',
  styleUrls: ['./select-guest.component.scss'],
  animations: fuseAnimations
})
export class SelectGuestComponent extends SectionCreateComponent<EventGuestModel> implements OnInit, OnDestroy {
  public listItemComponent: Object = ContactListItemComponent;
  guestImportFormStep: FormGroup;
  private subscription;
  public selectable = true;
  public data: Observable<IResponseModel>;
  public listId = '';
  public listName = '';

  public isContactSearching = false;
  public isGuestsSearching = false;

  public contactsListFiltered: any[];
  public guestListFiltered: any[];

  importFrom = 'contacts';
  selectedListsIds = {
    contacts: [],
    guests: []
  };
  selectedToImport = [];
  selectedToImportLists = [];
  selectedGuests = [];
  public guestList = [];
  public allImportCheck = false;
  public allGuestCheck = false;
  eventId = '';
  selectedListNames = [];
  isImporting: boolean;


  constructor(
    private fb: FormBuilder,
    public formService: EventGuestImportFormService,
    public sectionService: EventGuestService,
    protected dataService: EventGuestDataService,
    protected router: Router,
    private groupedListService: ContactsGroupedListService,
    public route: ActivatedRoute,
  ) {
    super(Section.EventGuests, sectionService, dataService, router);
    this.guestImportFormStep = this.fb.group({
      selectedGuests: ['', Validators.required],
    });
    this.formService.stepReady(this.guestImportFormStep, 'selectGuest');
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];
    });
    this.subscription = this.formService.selectGuestForm.subscribe((res: any) => {
      if (res.value.listId) {
        const listId: string = res.value.listId.id;
        const listName: string = res.value.listId.name;
        this.listName = listName;
        this.listId = listId;
        this.dataService.getContactsByListId([listId])
          .subscribe(result => {
            result.forEach(person => {
              this.formService.guestList.push(person.item);
            });
          });
      }       
      // if (res.value.listName) {
      //   const listName = res.value.listName;
      //   const data = {
      //     name: res.value.listName,
      //     eventId: this.eventId
      //   };
      //   this.dataService.createList(data)
      //     .subscribe(listId => {
      //       const toasterService = appInjector().get(ToasterService);
      //       if (listId) {
      //         toasterService.pop('success', null, 'Successfully created');
      //       } else {
      //         toasterService.pop('error', null, 'List name must be unique.');
      //         router.navigate(['events', route.snapshot.params.id , 'guests']);
      //       }
            
      //       this.listId = listId;
      //       this.listName = listName;
      //     });
      // }
    });
  }

  ngOnInit(): void{
    //this.guestList = [];
  }

  ngOnDestroy(): void{
    this.subscription.unsubscribe();
  }

  /// Select lists to import form dropdown 
  filterDropdown(data): void {
    if (data == null) {
      return;
   }
    this.selectedListNames = [ ...data.loadedData.contacts.map(a => a.name), ...data.loadedData.guests.map(a => a.name)];
    this.selectedToImportLists = [];
    const contacts$ = this.loadContacts(data);
    const guests$ = this.loadGuests(data);
    this.joinQUeries(contacts$, guests$);
  }

  // Contacts loading from server
  loadContacts(data: any): any{
    return this.dataService.getContactsByListId(data.selectedListsIds.contacts)
      .pipe(map(res => {
        data.loadedData.contacts.forEach(x => {
          const selectedItems = res.filter(y => y.contactListId === x.id);
          
          this.selectedToImportLists.push({
            listId: x.id,
            listName: x.name,
            listCount: selectedItems.length,
            items: selectedItems,
            type: 0
          });
        
        });
      }));
  }

  // Guests loading from server
  loadGuests(data: any): any{
    return this.dataService.getContactsByListId(data.selectedListsIds.guests)
      .pipe(map(res => {
        data.loadedData.guests.forEach(x => {
          const selectedItems = res.filter(y => y.contactListId === x.id);

          this.selectedToImportLists.push({
            listId: x.id,
            listName: x.name,
            listCount: selectedItems.length,
            items: selectedItems,
            type: 1
          });
          
        });
      }));
  }

  /// On import selection change 
  onImportCheckChange(event): void {
    this.selectedToImport = event;
  }

  /// On guests selection change 
  onGuestCheckChange(event): void {
    this.selectedGuests = event;
  }
  /// Select all guest
  selectAllGuests(event): void {
    const arr = [];
    if (event) {
      this.formService.guestList.forEach(guest => {
        arr.push(guest.id);
      });
    }
    this.selectedGuests = arr;
  }

  /// Import to guests
  import(): void {
    this.isImporting = true;
    this.selectedToImport.forEach(list => {
      list.selected.forEach(contact => {
        const item = contact.item != null ? contact.item : contact;
        if (list.selectedIds.filter(f => f === item.id).length === 1 || !list.selectedIds[0]) {
          if (this.formService.guestList.filter(f => f.id === item.id).length === 0) {
            this.formService.toBeGuestsList.push(item);
            this.formService.guestList.push(item);
          }
        }
      });
    });
    this.isImporting = false
    this.selectAllList(true);
    setTimeout(() => this.selectAllList(false), 0);    
    this.selectedToImport.length = 0;
  }

  selectGuest(event, guest){
    if(event){
      this.selectedGuests.push(guest.id);
    } else {
      this.selectedGuests = this.selectedGuests.filter(x => x != guest.id)
      
    }
  }

  /// Remove from guests
  remove(): void {
    this.selectedGuests.forEach(guestId => {
      const index = this.formService.guestList.findIndex(x => x.id === guestId);
      if (index > -1) {
        this.formService.guestList.splice(index, 1);
      }
      const index2 = this.formService.toBeGuestsList.findIndex(x => x.id === guestId);
      if (index2 > -1) {
        this.formService.toBeGuestsList.splice(index2, 1);
      }
    });
    // clear selected
    this.selectedGuests.length = 0;
    this.allGuestCheck = false;
  }

  /// Select all toggle for all list
  selectAllList(event): void {
    if (event) {
      this.groupedListService.selectAll();
      this.allImportCheck = true;
    } else {
      this.groupedListService.deselectAll();
      this.allImportCheck = false;
      this.selectedToImport.length = 0;
    }
  }

  // Save guests list on server
  // importGuestList(): void{
  //   const guestIds = this.formService.guestList.map(guest => guest.id);
  //   const data = {
  //     guestIds: guestIds,
  //     listId: this.listId != null ? this.listId : null
  //   };
  //   this.dataService.importGuestList(data).subscribe(result=>{
  //     if(result){
  //       this.formService.selectedGuestList = this.listId;
  //       this.formService.currentStepIndex = 2;
  //     } 
  //   });
  // }

  // Searching in contacts block
  searchForContacts(keyword: string): void{
    keyword = keyword.toUpperCase();
    if (keyword && keyword.length > 0) {
      this.isContactSearching = true;
      const items = [];

      this.selectedToImportLists.forEach(list => {
        const choosenItems = list.items.filter(x => x.item.fullName.toUpperCase().includes(keyword));
        items.push({
          listId: list.listId,
          listName: list.listName,
          listCount: choosenItems.length,
          type: list.type,
          items: choosenItems
        });
      });

      this.contactsListFiltered = items;
    } else {
      this.isContactSearching = false;

    }
  }

  // String searching in guests block
  searchForGuests(keyword: string): void{
    keyword = keyword.toUpperCase();
    if (keyword && keyword.length > 0) {
      this.isGuestsSearching = true;
      this.formService.guestListFiltered = this.formService.guestList.filter(x => x.fullName.toUpperCase().includes(keyword));
    } else {
      this.isGuestsSearching = false;
    }
  }

  // Perform joined queries with contacts and guests
  joinQUeries(request1$: Observable<void>, request2$: Observable<void>): void{
    forkJoin([request1$, request2$])
      .subscribe();
  }
}
