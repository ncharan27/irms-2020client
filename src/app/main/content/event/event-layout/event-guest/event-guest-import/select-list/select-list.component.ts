import { Component, OnInit } from '@angular/core';
import { EventGuestImportFormService } from '../event-guest-import-form.service';
import { FormBuilder, Validators, FormGroup, AsyncValidatorFn } from '@angular/forms';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { EventGuestModel } from '../../event-guest.model';
import { Section } from 'app/constants/constants';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { distinctUntilChanged, debounceTime, map, switchMap, first } from 'rxjs/operators';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { appInjector } from 'app/main/app-injector';




@Component({
  selector: 'irms-select-list',
  templateUrl: './select-list.component.html',
  styleUrls: ['./select-list.component.scss']
})
export class SelectListComponent extends SectionCreateComponent<EventGuestModel> implements OnInit {

  guestImportListStep: FormGroup;
  lists = [];
  eventId = '';
  guestList = [];
  isValid = true;
  listId: any;
  listName: any;

  constructor(private fb: FormBuilder,
              public formService: EventGuestImportFormService,
              public sectionService: EventGuestService,
              protected dataService: EventGuestDataService,
              protected router: Router,
              public route: ActivatedRoute) {
    super(Section.EventGuests, sectionService, dataService, router);
    this.guestImportListStep = this.fb.group({
      list: ['existing', [Validators.required]],
      listName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
      listId: ['', [Validators.required]]
    });
    this.formService.stepReady(this.guestImportListStep, 'selectList');
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];
    });
    const data = {
      pageNo: 1,
      pageSize: 100,
      searchText: '',
      eventId: this.eventId
    };
    this.dataService.getList(data).subscribe(res => {
      this.lists = res.items;
    });
    this.guestImportListStep.controls.listId.valueChanges.subscribe(data => {
      this.formService.guestList = [];
      this.formService.toBeGuestsList.forEach(guest => {
        this.formService.guestList.push(guest);
      });
      const id = data.id;
      if (id){
        this.dataService.getContactsByListId([id]).subscribe(result => {
          result.forEach(person => {
            this.formService.add(person.item);
          });
        });
      }
    });
  }

  ngOnInit(): void {
    // Select list form custom validator
    this.guestImportListStep.get('list').valueChanges
      .subscribe((value: string) => {
        if (value === 'new') {
          this.setValidators('guestImportListStep', 'listName', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]);
          this.guestImportListStep.controls['listName'].setAsyncValidators(this.validateListNameUniqueness());
          this.guestImportListStep.controls['listName'].valueChanges.subscribe(x => {
            this.isValid = false;
          });
          this.clearValidators('guestImportListStep', ['listId']);
        } else if (value === 'existing') {
          this.setValidators('guestImportListStep', 'listId', [Validators.required]);
          this.clearValidators('guestImportListStep', ['listName']);
          this.guestImportListStep.controls['listName'].setValue('');
        } else {
          this.clearValidators('guestImportListStep', ['listName', 'listId']);
        }
      });
  }

  goToImportGuests(): void {
    this.formService.selectedGuestList = this.guestImportListStep.controls.listId.value.id;
    if (this.guestImportListStep.controls.listName.value) {
      const listName = this.guestImportListStep.controls.listName.value;
      const data = {
        name: this.guestImportListStep.controls.listName.value,
        eventId: this.eventId
      };
      this.dataService.createList(data)
        .subscribe(listId => {
          const toasterService = appInjector().get(ToasterService);
          if (listId) {
            toasterService.pop('success', null, 'Successfully created');
            this.listId = listId;
            this.importGuestList();

          } else {
            toasterService.pop('error', null, 'List name must be unique.');
            // router.navigate(['events', route.snapshot.params.id , 'guests']);
          }

          this.listId = listId;
        });
    } else {
    this.importGuestList();
    }
  }

  importGuestList(): void {
    this.guestList = this.formService.guestList;
    const guestIds = this.guestList.map(guest => guest.id);
    const data = {
      guestIds: guestIds,
      listId: this.listId ? this.listId : this.guestImportListStep.controls.listId.value.id
    };
    this.dataService.importGuestList(data).subscribe(result => {
      if (result) {
        this.formService.toBeGuestsList = [];
        this.formService.guestList = [];
        this.formService.selectedGuestList = this.listId ? this.listId : this.guestImportListStep.controls.listId.value.id;
        this.formService.currentStepIndex = 1;
      }
    });
  }

  validateListNameUniqueness(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(value => this.dataService.isListNameUnique(control.value)),
        map((unique: boolean) => {
          this.isValid = !unique;
          return !unique ? null : { listNameTaken: true };
        }),
        first());
  }
}
