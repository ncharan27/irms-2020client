import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from 'app/core/animations';
import { EventGuestImportFormService } from './event-guest-import-form.service';

@Component({
  selector: 'irms-event-guest-import',
  templateUrl: './event-guest-import.component.html',
  styleUrls: ['./event-guest-import.component.scss'],
  animations: fuseAnimations
})
export class EventGuestImportComponent implements OnInit, OnDestroy{
  constructor(public formService: EventGuestImportFormService) { }
  ngOnDestroy(): void {
    this.formService.currentStepIndex = 0;
    this.formService.toBeGuestsList = [];
    this.formService.guestList = [];
  }
  ngOnInit(): void {
    
  }

  resetIndex(): void{
    this.formService.currentStepIndex = 0;
  }

}
