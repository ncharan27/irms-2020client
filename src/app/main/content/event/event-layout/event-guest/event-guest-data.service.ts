import { Injectable } from '@angular/core';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { EventGuestModel } from './event-guest.model';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { Observable, of } from 'rxjs';
import { IResponseModel, IPage, IGuestPage } from 'types';
import { Toast } from 'app/core/decorators/toast.decorator';
import { filter } from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class EventGuestDataService implements SectionDataService<EventGuestModel> {



    private url = `${BASE_URL}api/Contact`;
    private contactDetailUrl = `${BASE_URL}api/contactDetail`;
    private customFieldUrl = `${BASE_URL}api/customField`;
    private campaignUrl = `${BASE_URL}api/campaign`;
    private campaignInvitationUrl = `${BASE_URL}api/campaignInvitation`;
    private listAnalysisUrl = `${BASE_URL}api/listAnalysis`;
    private rfiUrl = `${BASE_URL}api/rfiForm`;

    constructor(private http: HttpClient) { }
    @Loader()
    getList(filterParam: IPage): Observable<IResponseModel> {
        return this.http.post<IResponseModel>(`${this.url}/event-guest-lists`, filterParam);
    }

    getContactFields(filterParam): Observable<any> {
        return this.http.post<any>(`${this.url}/contacts-custom-fields`, filterParam);
    }

    @Toast('Successfully deleted RFI Campaign')
    cancelListAnalysis(filterParam: any): Observable<any> {
        return this.http.post<any>(`${this.listAnalysisUrl}/delete`, filterParam);
    }

    getRFI(id): Observable<any> {
        return this.http.get<any>(`${this.rfiUrl}/${id}`);
    }

    @Toast('Successfully updated form')
    upsertRFI(model): Observable<any> {
        return this.http.post(`${this.rfiUrl}`, model);
    }

    getValidationsCount(filterParam): Observable<any> {
        return this.http.post<any>(`${this.listAnalysisUrl}/validations-count`, filterParam);
    }

    sendCollectForm(campaignId): Observable<any> {
        return this.http.get(`${this.campaignUrl}/list-analysis-go-live/${campaignId}`);
    }

    getSelectedImportantFields(listId): Observable<any> {
        return this.http.get(`${this.listAnalysisUrl}/important-fields/${listId}`);
    }

    setSelectedImportantFields(params): Observable<any> {
        return this.http.post(`${this.listAnalysisUrl}/important-fields`, params);
    }

    getStep(params): Observable<any> {

        return this.http.post(`${this.listAnalysisUrl}/get-list-state`, params);
    }

    setStep(params: any) {
        return this.http.post(`${this.listAnalysisUrl}/set-list-state`, params);
    }

    getAllCustomFields(filterParam): Observable<any> {
        return this.http.post(`${this.customFieldUrl}/get-all`, filterParam);
    }

    getEventGuestBasicInfo(listId, id): Observable<any> {
        return this.http.post<any>(`${this.contactDetailUrl}/personal-details`, {
            guestListId: listId,
            contactId: id
        });
    }

    getGuestAssociatedLists(data): Observable<any> {
        return this.http.get<any>(`${this.contactDetailUrl}/associated-lists/${data.guestId}`);
    }

    getGuestCustomFields(filterParam: any): Observable<any> {
        return this.http.get<any>(`${this.contactDetailUrl}/custom-fields/${filterParam.guestId}`)
    }

    updateGuestCustomFields(id, data): Observable<any> {
        return this.http.post<any>(`${this.contactDetailUrl}/custom-fields/${id}`, data);
    }

    getEventGuestOrganizationInfo(id): Observable<any> {
        return this.http.get<any>(`${this.contactDetailUrl}/organization-info/${id}`);
    }

    @Toast('Successfully updated')
    updateEventGuestOrganizationInfo(model): Observable<any> {
        return this.http.post<any>(`${this.contactDetailUrl}/organization-info`, model);
    }

    getEventGuestAdmissionInfo(params): Observable<any> {
        return this.http.get<any>(`${this.contactDetailUrl}/event-admission/${params.listId}/${params.guestId}`)
    }

    @Toast('Successfully updated')
    updateEventGuestAdmissionInfo(params): Observable<any> {
        return this.http.post<any>(`${this.contactDetailUrl}/event-admission`, params);
    }

    getReviewFormDetails(id): Observable<any> {
        return this.http.get<any>(`${this.campaignInvitationUrl}/list-analysis/${id}`);
    }

    get(id): Observable<EventGuestModel> {
        return this.http.get<EventGuestModel>(`${this.contactDetailUrl}/personal-info/${id}`);
    }

    updatePersonalInfo(data): Observable<any> {
        return this.http.post<any>(`${this.contactDetailUrl}/personal-info`, data);
    }

    getEventGuestsCount(): any {
        return this.http.get<any>(`${this.url}/guests-count`);
    }

    getEventGuests(id): Observable<any> {
        return this.http.get<any>(`${this.url}/${id}/guests`);
    }


    @Toast('Successfully created')
    create(model: any): Observable<string> {
        const header = new HttpHeaders();
        return this.http.post<string>(`${this.url}`, model, { headers: header });
    }

    isListNameUnique(name): Observable<boolean> {
        return this.http.post<boolean>(`${this.url}/list-name-uniqueness`, {
            name
        });
    }

    @Toast('Successfully updated')
    update(model) {
        return this.http.put(`${this.url}`, model);
    }

    @Toast('Successfully updated')
    createListCampaign(data): Observable<any> {
        return this.http.post<any>(`${this.campaignUrl}/list-analysis`, data);
    }

    @Toast('Successfully deleted')
    delete(model) {
        return this.http.request('delete', `${this.url}`, { body: model });
    }

    createList(model: any): Observable<string> {
        const header = new HttpHeaders();
        return this.http.post<string>(`${this.url}/create-event-guest-list`, model, { headers: header });
    }

    getlocalGuestContactByListId(form: IGuestPage): Observable<any> {
        return this.http.post<IResponseModel>(`${this.url}/local-guest-contact-by-list-id`, form);
    }

    getGuestLists(): Observable<any> {
        return this.http.get<any>(`${this.url}/global-guest-list`);
    }

    getContactLists(): Observable<any> {
        return this.http.get<any>(`${this.url}/contact-list`);
    }

    getContactsByListId(contactList): Observable<any> {
        return this.http.post(`${this.url}/contacts-by-list-id`, contactList);
    }

    importGuestList(data): Observable<any> {
        return this.http.post(`${this.url}/add-guests-in-guest-list`, data);
    }

    deleteGuestList(data): Observable<any> {
        return this.http.post(`${this.url}/delete-contact-list`, data);
    }

    renameList(data): Observable<any> {
        return this.http.post(`${this.url}/rename-list`, data);
    }
    
    @Toast('Successfully exported')
    exportContacts(data): Observable<any> {
        return this.http.post(`${this.url}/export-users`, data, {
            responseType: 'blob' as 'json',
            observe: 'response' as 'body',
        });
    }

    removeUsers(data): Observable<any> {
        return this.http.post(`${this.url}/remove-users`, data);
    }

    getListName(data): Observable<any> {
        return this.http.post(`${this.url}/list-name`, data);
    }

    checkEmailUniqueness(email): Observable<any> {
        return this.http.post(`${this.url}/CheckEmailUniqueness`, email);
    }

    checkPhoneUniqueness(phone): Observable<any> {
        return this.http.post(`${this.url}/CheckPhoneUniqueness`, phone);
    }
}
