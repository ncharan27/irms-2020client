import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventGuestComponent } from './event-guest.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { EventGuestListComponent } from './event-guest-list/event-guest-list.component';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { FuseNavigationModule } from '@fuse/components';
import { RouterModule } from '@angular/router';
import { EventGuestImportComponent } from './event-guest-import/event-guest-import.component';
import { EventGuestDataService } from './event-guest-data.service';
import { EventGuestService } from './event-guest.service';
import { SelectGuestComponent } from './event-guest-import/select-guest/select-guest.component';
import { ContactListItemComponent } from './event-guest-import/select-guest/contact-list-item/contact-list-item.component';
import { SelectListComponent } from './event-guest-import/select-list/select-list.component';
import { ContactGuestListSearchComponent } from 'app/main/content/components/shared/contact-guest-list-search/contact-guest-list-search.component';
import { EventGuestListItemComponent } from './event-guest-list/event-guest-list-item/event-guest-list-item.component';
import { EventGuestListCreateComponent } from './event-guest-list-create/event-guest-list-create.component';
import { EventGuestListGuestsComponent } from './event-guest-list-guests/event-guest-list-guests.component';
import { EventGuestListGuestsItemComponent } from './event-guest-list-guests/event-guest-list-guests-item/event-guest-list-guests-item.component';
import { EventGuestListGuestsItemLockedComponent } from './event-guest-list-guests/event-guest-list-guests-item-locked/event-guest-list-guests-item-locked.component';
import { GenericListService } from 'app/main/content/components/shared/generic-list/generic-list.service';
import { ToasterModule } from 'angular5-toaster/dist/angular5-toaster';
import { EventGuestListItemRenameModalComponent } from './event-guest-list/event-guest-list-item/event-guest-list-item-rename-modal/event-guest-list-item-rename-modal.component';
import { EventGuestDetailsComponent } from './event-guest-details/event-guest-details.component';
import { EventGuestPersonalInfoComponent } from './event-guest-details/event-guest-personal-info/event-guest-personal-info.component';
import { EventGuestOrganizationInfoComponent } from './event-guest-details/event-guest-organization-info/event-guest-organization-info.component';
import { EventGuestCustomFieldsComponent } from './event-guest-details/event-guest-custom-fields/event-guest-custom-fields.component';
import { EventGuestAssociatedListsComponent } from './event-guest-details/event-guest-associated-lists/event-guest-associated-lists.component';
import { EventGuestEventAdmissionComponent } from './event-guest-details/event-guest-event-admission/event-guest-event-admission.component';
import { MatTableModule } from '@angular/material';
import { EventGuestListAnalysisComponent } from './event-guest-list-analysis/event-guest-list-analysis.component';
import { ImportantFieldsComponent } from './event-guest-list-analysis/important-fields/important-fields.component';
import { ImportAnalysisComponent } from './event-guest-list-analysis/import-analysis/import-analysis.component';
import { CollectImportantFieldsComponent } from './event-guest-list-analysis/collect-important-fields/collect-important-fields.component';
import { MediaTemplateDesignModule } from 'app/main/content/components/shared/media-template-design/media-template-design.module';
import { PreferredMediaModule } from 'app/main/content/components/shared/preferred-media/preferred-media.module';
import { CampaignPreferredMediaDialogComponent } from 'app/main/content/components/shared/preferred-media/campaign-preferred-media-dialog/campaign-preferred-media-dialog.component';
import { PreferredMediaListItemComponent } from 'app/main/content/components/shared/preferred-media/campaign-preferred-media-dialog/preferred-media-list-item/preferred-media-list-item.component';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';
import { ListAnalysisImportantFieldModalComponent } from './event-guest-list-analysis/collect-important-fields/list-analysis-important-field-modal/list-analysis-important-field-modal.component';
import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling';
import { RfiDataReviewModalComponent } from './event-guest-list-analysis/collect-important-fields/rfi-data-review-modal/rfi-data-review-modal.component';
import { DataReviewFormBuilderModule } from 'app/main/content/components/shared/data-review-form-builder/data-review-form-builder.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AddCustomFieldModalComponent } from 'app/main/content/components/shared/add-custom-field-modal/add-custom-field-modal.component';

const routes = [
  {
      path: '',
      component: EventGuestComponent,
      canLoad: [AuthGuard],
      data: { expectedRoles: [RoleType.TenantAdmin] },
      children: [
          {
              path: '',
              canActivateChild: [AuthGuard],
              component: EventGuestListComponent,
          },
          {
            path: 'import',
            canActivateChild: [AuthGuard],
            component: EventGuestImportComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: ':glid',
            canActivateChild: [AuthGuard],
            component: EventGuestListGuestsComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: ':glid/import-analysis',
            canActivateChild: [AuthGuard],
            component: EventGuestListAnalysisComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] },
          },
          {
            path: ':glid/guest/:gid',
            canActivateChild: [AuthGuard],
            component: EventGuestDetailsComponent,
            data: { expectedRoles: [RoleType.TenantAdmin] },
            children: [
              {
                path: 'personal-info',
                canActivateChild: [AuthGuard],
                component: EventGuestPersonalInfoComponent,
                data: { expectedRoles: [RoleType.TenantAdmin] },
              },
              {
                path: 'organization-info',
                canActivateChild: [AuthGuard],
                component: EventGuestOrganizationInfoComponent,
                data: { expectedRoles: [RoleType.TenantAdmin] },
              },
              {
                path: 'custom-fields',
                canActivateChild: [AuthGuard],
                component: EventGuestCustomFieldsComponent,
                data: { expectedRoles: [RoleType.TenantAdmin] },
              },
              {
                path: 'associated-lists',
                canActivateChild: [AuthGuard],
                component: EventGuestAssociatedListsComponent,
                data: { expectedRoles: [RoleType.TenantAdmin] },
              },
              {
                path: 'event-admission',
                canActivateChild: [AuthGuard],
                component: EventGuestEventAdmissionComponent,
                data: { expectedRoles: [RoleType.TenantAdmin] },
              }
            ]
          }
      ]
  }
];

@NgModule({
  declarations: [EventGuestComponent,
     EventGuestListComponent,
      EventGuestImportComponent,
      SelectGuestComponent,
      ContactListItemComponent,
      SelectListComponent,
      EventGuestListItemComponent,
      EventGuestListCreateComponent,
      EventGuestListGuestsComponent,
      EventGuestListGuestsItemComponent,
      EventGuestListGuestsItemLockedComponent,
      EventGuestListItemRenameModalComponent,
      EventGuestDetailsComponent,
      EventGuestPersonalInfoComponent,
      EventGuestOrganizationInfoComponent,
      EventGuestCustomFieldsComponent,
      EventGuestAssociatedListsComponent,
      EventGuestEventAdmissionComponent,
      ImportantFieldsComponent,
      ImportAnalysisComponent,
      EventGuestListAnalysisComponent,
      CollectImportantFieldsComponent,
      ListAnalysisImportantFieldModalComponent,
      RfiDataReviewModalComponent],
  imports: [
    ToasterModule,
    MainModule,
    ContentModule,
    CommonModule,
    MediaTemplateDesignModule,
    FuseNavigationModule,
    MatTableModule,
    DragDropModule,
    PreferredMediaModule,
    ScrollingModule,
    DataReviewFormBuilderModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  entryComponents: [ContactListItemComponent,
    ContactGuestListSearchComponent,
    EventGuestListItemComponent,
    EventGuestListItemRenameModalComponent,
    CampaignPreferredMediaDialogComponent,PreferredMediaListItemComponent,
    EventGuestListCreateComponent, EventGuestListGuestsItemComponent, EventGuestListGuestsItemLockedComponent,
    ListAnalysisImportantFieldModalComponent,
    RfiDataReviewModalComponent,
    AddCustomFieldModalComponent,
  ],
  providers : [
    EventGuestDataService, EventGuestService, GenericListService, DialogAnimationService
  ]
})
export class EventGuestModule { }
