import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormArray, Validators, ValidatorFn, FormControl, FormGroup } from '@angular/forms';
import { EventGuestService } from '../../event-guest.service';
import { EventGuestDataService } from '../../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Section, RegexpPattern, reservedFieldsWithValidation } from 'app/constants/constants';
import { SectionCreateComponent } from 'app/main/content/components/shared/section/section-create/section-create.component';
import { EventGuestModel } from '../../event-guest.model';
import { fuseAnimations } from '@fuse/animations';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { ImportAnalysisFormService } from '../import-analysis-form.service';
import { CdkVirtualScrollViewport, FixedSizeVirtualScrollStrategy, VIRTUAL_SCROLL_STRATEGY } from '@angular/cdk/scrolling';
import { DataSource } from '@angular/cdk/table';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { ArrayDataSource } from '@angular/cdk/collections';

const PAGESIZE = 25;
const ROW_HEIGHT = 48;

export class GridTableDataSource extends DataSource<any> {
    private _data: any[];

    get allData(): any[] {
        return this._data.slice();
    }

    set allData(data: any[]) {
        this._data = data;
        this.viewport.scrollToOffset(0);
        this.viewport.setTotalContentSize(this.itemSize * data.length);
        this.visibleData.next(this._data.slice(0, PAGESIZE));
    }

    offset = 0;
    offsetChange = new BehaviorSubject(0);
    updatingScroll = false;
    constructor(initialData: any[], private viewport: CdkVirtualScrollViewport, private itemSize: number) {
        super();
        this._data = initialData;
    }

    private readonly visibleData: BehaviorSubject<any[]> = new BehaviorSubject([]);

    connect(collectionViewer: import('@angular/cdk/collections').CollectionViewer): Observable<any[] | ReadonlyArray<any>> {
        return this.visibleData;
    }

    disconnect(collectionViewer: import('@angular/cdk/collections').CollectionViewer): void {
    }
}

/**
 * Virtual Scroll Strategy
 */
export class CustomVirtualScrollStrategy extends FixedSizeVirtualScrollStrategy {
    constructor() {
        super(ROW_HEIGHT, 1000, 2000);
    }

    attach(viewport: CdkVirtualScrollViewport): void {
        this.onDataLengthChanged();
    }
}


@Component({
    selector: 'irms-import-analysis',
    templateUrl: './import-analysis.component.html',
    styleUrls: ['./import-analysis.component.scss'],
    providers: [{ provide: VIRTUAL_SCROLL_STRATEGY, useClass: CustomVirtualScrollStrategy }],
    animations: fuseAnimations
})

export class ImportAnalysisComponent extends SectionCreateComponent<EventGuestModel> implements OnInit {

    contactsPlainArray = [];

    contacts: any;
    disabled = ['email', 'mobileNumber'];
    headers = [];
    mandatory = ['fullName'];

    pageSizeOptions = [5, 10, 25, 50];
    totalCount;
    guestListPage: { listId: any; pageSize: number; pageNo: number; };
    doNotShow: boolean;
    headersToLoop: string[];
    @ViewChild('scroll_list', { static: true }) viewport: CdkVirtualScrollViewport;

    importAnalysisForm = this.fb.group({
        contacts: new FormArray([])
    });

    // dataSource: any;
    importedContacts: any;
    listId: any;
    eventId: any;
    customFields: any;
    customFieldsinHeader = [];    
    customFieldsHeaderNames = [];
    
    requiredFieldsinHeader: any = {};
    // Validation Counters
    fieldsErrors: number;
    fieldsMissing: any;
    missing = 0;
    errors = 0;

    // lookups
    nationalities: any;
    documentTypes: any;
    issuingCountries: any;
    titles: string[];

    loading: boolean;
    loadingContacts = false;
    submitting: boolean;
    isValidated: boolean;
    rowHeight = 48;
    headerHeight = 48;
    placeholderHeight = 0;
    rows: Observable<Array<any>> = of([]);
    displayedColumns: string[] = [];
    dataSource: GridTableDataSource;

    contactEditMode = false;
    missingGuests = [];
    localMissing = 0;


    // Searchbars for lookup dropdowns
    issuingCountriesSearchControl = new FormControl('');
    documentTypesSearchControl = new FormControl('');
    nationalitySearchControl = new FormControl('');
    isNext: boolean;
    isBack: boolean;

    constructor(
        private fb: FormBuilder,
        private dialog: MatDialog,
        private formService: ImportAnalysisFormService,
        public sectionService: EventGuestService,
        protected dataService: EventGuestDataService,
        protected router: Router,
        public route: ActivatedRoute,
        public appStateService: AppStateService,
        dateTimeAdapter: DateTimeAdapter<any>) {
        super(Section.EventGuests, sectionService, dataService, router);
        dateTimeAdapter.setLocale('en-GB');
    }

    ngOnInit(): void {
        this.loadingContacts = true;
        this.route.params.subscribe((params: Params) => {
            this.listId = params['glid'];
            this.sectionService.listId = this.listId;
            this.eventId = params['id'];
        });
        if (this.sectionService.stepConfig) {
            this.guestListPage = {
                listId: this.listId,
                pageSize: this.sectionService.pageConfig.pageSize,
                pageNo: this.sectionService.pageConfig.pageNo
            };

        } else {
            this.guestListPage = {
                listId: this.listId,
                pageSize: 5,
                pageNo: 1
            };
        }

        if (this.formService.selectedFields) {
            this.patchData(this.formService.getSelectedFields());
        } else {
            let custList = [];
            this.dataService.getSelectedImportantFields(this.listId)
                .subscribe(fields => {
                    // Handle Custom Fields
                    const selectedReservedFields = fields.reservedFields;
                    if (fields.customsFields.length > 0) {
                        this.dataService.getAllCustomFields({
                    fields: fields.customsFields.map(c => c.value)
                        }).subscribe(data => {
                            custList = data;
                            // Get previously saved important fields for the list
                            fields.customsFields.forEach(x => {
                        const cObj = custList.find(f => f.id === x.value);
                        if (cObj) {
                            x.customFieldType = cObj.customFieldType;
                            x.maxValue = cObj.maxValue;
                            x.minValue = cObj.minValue;
                            x.label = cObj.name;
                        }
                            });
                            this.formService.setSelectedFields({ reserved: fields.reservedFields, custom: fields.customsFields });
                            this.patchData({ reserved: fields.reservedFields, custom: fields.customsFields});
                        });
                } else {
                    this.formService.setSelectedFields({ reserved: fields.reservedFields, custom: [] });
                    this.patchData({ reserved: fields.reservedFields, custom: []});
                    }
                });
        }

    }

    placeholderWhen(index: number, _: any): boolean {
        return index === 0;
    }

    patchData(x): void {
        this.customFieldsinHeader = x.custom;
        this.customFieldsHeaderNames = x.custom.map(e => e.label);
        this.headers = x.reserved.map(r => r.value).concat(x.custom.map(c => c.label));
        this.loadLookups();
        
        // keep account of all required fields
        x.reserved.forEach(field => {
            this.requiredFieldsinHeader[field.value] = field.isRequired;
        });
        x.custom.forEach(field => {
            this.requiredFieldsinHeader[field.label] = field.isRequired;
        });

        const temp = ['#'];
        this.headersToLoop = this.headers;
        this.headers = temp.concat(this.headers);

        this.getGuestData();
        this.getValidationsCount(x.reserved.map(e => e.value), x.custom.map(e => e.value));
    }

    getValidationsCount(reservedFields, customFields): void {
        const filter = {
            listId: this.listId,
            reservedFields,
            customFields
        };

        this.dataService.getValidationsCount(filter).subscribe(result => {
            this.errors = result.errors;
            this.missing = result.guestWarnings;
            this.fieldsMissing = result.fieldWarnings;
            result.missingGuests.forEach(element => { this.missingGuests.push(element); });
        });

    }

    editContact(contactIndex): void {
        if (this.contactEditMode) {
            return;
        }
        this.contactEditMode = !this.contactEditMode;
        this.contactsPlainArray[contactIndex - 1].editing = true;
        // this.dataSource.allData = this.contactsPlainArray;

        const formValue = this.contactsArray.get(`${contactIndex - 1}`).value;
        const reserved = this.formService.getSelectedFields().reserved;
        const custHeaders = this.customFieldsHeaderNames;

        // Get missing Fields count for contact in edit view
        reserved.forEach(field => {
            if (formValue[field] === '' || formValue[field] == null) {
                this.localMissing++;
            }
        });
        for (let i = 0; i < formValue.customFields; i++) {
            if ((formValue.customFields[i].value == null || formValue.customFields[i].value == null) && custHeaders.indexOf(formValue.customFields[i].name) > -1) {
                this.localMissing++;
            }
        }
    }
    getValid(i): any {
        return this.contactsArray.get(`${i - 1}`).valid;
    }

    saveContactChanges(contactIndex: number): void {
        const headersToLoop = this.headers;

        if (this.contactsArray.get(`${contactIndex - 1}`).valid) {
            const formValue = this.contactsArray.get(`${contactIndex - 1}`).value;
            headersToLoop.forEach(header => {
                if (header !== 'actions' && header !== '#') {
                    this.contactsPlainArray[contactIndex - 1][header] = formValue[header];
                }
            });

            // custom fields
            const custHeaders = this.customFieldsHeaderNames;
            const custArr = [];
            for (let i = 0; (i < custHeaders.length) && !formValue.customFields; i++) {
                const obj = {
                    id: this.customFieldsinHeader[i].id,
                    name: this.customFieldsinHeader[i].name,
                    value: formValue[custHeaders[i]]
                };
                custArr.push(obj);
                delete formValue[custHeaders[i]];
            }
            if (custArr.length > 0 && !formValue.hasOwnProperty('customFields')) {
                formValue.customFields = custArr;
            } else {
                formValue.customFields = null;
            }

            this.dataService.update(formValue).subscribe(result => {
                this.contactsPlainArray[contactIndex - 1].editing = false;
                this.contactEditMode = !this.contactEditMode;
                if (result) {
                    // Get missing fields number after user have placed changes
                    const reserved = this.formService.getSelectedFields().reserved;
                    let isMissing = false;
                    let missingFields = 0;
                    reserved.forEach(field => {
                        if (formValue[field] === '' || formValue[field] == null) {
                            if (this.missingGuests.indexOf(formValue.id) < 0) {
                                this.missingGuests.push(formValue.id);
                            }
                            missingFields++;
                            isMissing = true;
                        }
                    });
                    for (let i = 0; i < formValue.customFields; i++) {
                        if ((formValue.customFields[i].value == null || formValue.customFields[i].value == null) && custHeaders.indexOf(formValue.customFields[i].name) > -1) {
                            if (this.missingGuests.indexOf(formValue.id) < 0) {
                                this.missingGuests.push(formValue.id);
                            }
                            missingFields++;
                            isMissing = true;
                        }
                    }
                    if (!isMissing) {
                        this.missingGuests[this.missingGuests.indexOf(formValue.id)] = -1;
                        this.fieldsMissing = this.fieldsMissing - this.localMissing;
                        this.missing = this.missing - 1 < 0 ? 0 : this.missing - 1;
                    } else {
                        if (this.localMissing > missingFields) {
                            this.fieldsMissing = this.fieldsMissing - missingFields;
                        } else if (this.localMissing < missingFields) {
                            this.fieldsMissing = this.fieldsMissing + (missingFields - this.localMissing);
                            if (this.localMissing === 0) {
                                this.missing++;
                            }
                        }
                    }
                }
                this.localMissing = 0;
            });
        } else {
        }
    }

    discardContactChanges(contactIndex): void {
        const headersToLoop = this.headers;
        const formValue = this.contactsPlainArray[contactIndex - 1];
        headersToLoop.forEach(header => {
            if (header !== 'actions' && header !== '#') {
                this.contactsArray.get(`${contactIndex - 1}.${header}`).setValue(formValue[header]);
            }
        });
        this.localMissing = 0;
        this.contactsPlainArray[contactIndex - 1].editing = false;
        this.contactEditMode = !this.contactEditMode;
    }

    getGuestData(): void {
        this.dataService.getContactFields(this.guestListPage).subscribe(result => {
            this.contacts = result.items;
            this.totalCount = result.totalCount;
            this.pushFields();
        });
    }

    goNext(): void {
        this.isNext = true;
        const model = {
            listId: this.listId,
            eventId: this.eventId,
            listAnalysisStep: 2,
            listAnalysisPageNo: null,
            listAnalysisPageSize: null
        };
        this.dataService.setStep(model).subscribe(res => {
            if (res) {
                this.submitting = true;
                this.headersToLoop.forEach(element => this.formService.headers.push(element));
                this.formService.linear = false;
                this.isNext = false;
                this.formService.setStepperIndex(2);
                setTimeout(() => { this.formService.linear = true; });
            }
        });

    }

    public markFormGroupTouched(formGroup: FormGroup): void {
        (Object as any).values(formGroup.controls).forEach(control => {
            control.markAsTouched();
            if (control.controls) {
                this.markFormGroupTouched(control);
            }
        });
    }

    // Lookups mappers for dropdowns
    getNat(nat): any {
        if (!this.nationalities) { return ''; }
        return this.nationalities.find(item => item.id === nat).nationality ? this.nationalities.find(item => item.id === nat).nationality : '';
    }

    getCountry(country): any {
        if (!this.issuingCountries) { return ''; }
        return this.issuingCountries.find(item => item.id === country).value ? this.issuingCountries.find(item => item.id === country).value : '';
    }

    getDoc(doc): any {
        if (!this.documentTypes) { return ''; }
        return this.documentTypes.find(item => item.id === doc).name ? this.documentTypes.find(item => item.id === doc).name : '';
    }

    getDate(date): any {
        if (date) {
            return new Date(date);
        } else {
            return '';
        }
    }


    get contactsArray(): FormArray {
        return this.importAnalysisForm.get('contacts') as FormArray;
    }

    // pushed fields to formGroup
    pushFields(): void {
        if (this.headers.indexOf('actions') > -1) {
            this.headers.splice(this.headers.indexOf('actions'), this.headers.indexOf('actions'));
        }
        this.importAnalysisForm = this.fb.group({
            contacts: new FormArray([])
        });
        this.contactsPlainArray = [];
        const bar = new Promise((resolve, reject) => {
            const headersToLoop = this.headers;
            let missing = 0;
            let errors = 0;
            let fieldsMissing = 0;
            let fieldsError = 0;
            this.contacts.forEach((element, index) => {
                let contact: any = {};
                let contactForm: FormGroup;
                if (element.customFields) {
                    this.customFields = element.customFields;
                    delete element.customFields;
                    contact = element;
                    contactForm = this.fb.group(element);
                    this.customFieldsinHeader.forEach(field => {
                        const fieldRespIndex = this.customFields.map(e => e.id).indexOf(field.value);
                        if (fieldRespIndex < 0) {
                            contact[field.label] = '';
                            contactForm.addControl(field.label, new FormControl(''));
                        } else {
                            contact[field.label] = this.customFields[fieldRespIndex].value;
                            contactForm.addControl(field.label, new FormControl(this.customFields[fieldRespIndex].value));

                        }
                        const validators = this.computeValidators(field);
                        contactForm.get(field.label).setValidators(validators);
                    });
                } else {
                    delete element.customFields;
                    contact = element;
                    contactForm = this.fb.group(element);
                    this.customFieldsinHeader.forEach(field => {
                        contact[field.label] = '';
                        contactForm.addControl(field.label, new FormControl(''));
                        const validators = this.computeValidators(field);
                        contactForm.get(field.label).setValidators(validators);
                    });
                }

                if (this.headers.indexOf('title') > -1) {
                    contactForm.get('title').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
                }
                if (this.headers.indexOf('fullName') > -1) {
                    contactForm.get('fullName').setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(100)]);
                }
                if (this.headers.indexOf('preferredName') > -1) {
                    contactForm.get('preferredName').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
                }
                if (this.headers.indexOf('gender') > -1) {
                    contactForm.get('gender').setValidators(this.isValidGender());
                }
                if (this.headers.indexOf('email') > -1) {
                    // contactForm.get('email').disable();
                    // Note: use disable carefully; using disable doesn't include value in form value
                }
                if (this.headers.indexOf('alternativeEmail') > -1) {
                    contactForm.get('alternativeEmail').setValidators(Validators.pattern(RegExp(RegexpPattern.email)));
                }
                if (this.headers.indexOf('workNumber') > -1) {
                    contactForm.get('workNumber').setValidators(Validators.pattern(RegExp(RegexpPattern.intlPhone)));
                }
                if (this.headers.indexOf('mobileNumber') > -1) {
                    // contactForm.get('mobileNumber').disable();
                    // Note: use disable carefully; using disable doesn't include value in form value
                }
                if (this.headers.indexOf('secondaryMobileNumber') > -1) {
                    contactForm.get('secondaryMobileNumber').setValidators([Validators.pattern(RegExp(RegexpPattern.intlPhone))]);
                }
                if (this.headers.indexOf('nationalityId') > -1) {
                    contactForm.get('nationalityId').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
                }
                if (this.headers.indexOf('position') > -1) {
                    contactForm.get('position').setValidators([Validators.minLength(2), Validators.maxLength(50)]);
                }
                if (this.headers.indexOf('organization') > -1) {
                    contactForm.get('organization').setValidators([Validators.minLength(2), Validators.maxLength(50)]);
                }
                if (this.headers.indexOf('documentNumber') > -1) {
                    contactForm.get('documentNumber').setValidators([Validators.minLength(2), Validators.maxLength(50)]);
                }


                contact['#'] = index + 1;
                contactForm.markAsTouched();
                contactForm.updateValueAndValidity();
                if (contactForm.invalid) {
                    errors++;
                    contact.isError = true;
                    for (let j = 1; j < headersToLoop.length; j++) {
                        if (this.importAnalysisForm.get(`contacts.${index}.${headersToLoop[j]}`).invalid) { fieldsError++; }
                    }
                    this.fieldsErrors = fieldsError;
                }
                let isMissing = false;
                let i = 0;
                while (i < headersToLoop.length) {
                    if (headersToLoop[i] !== '#' && headersToLoop[i] !== 'fullName') {
                        if (element[headersToLoop[i]] === null || element[headersToLoop[i]] === '') {
                            isMissing = true;
                            fieldsMissing++;
                        }
                    }
                    i++;
                }
                if (isMissing) {
                    missing++;
                    contact.isMissing = true;
                }

                this.contactsArray.push(contactForm);
                this.contactsPlainArray.push(contact);
                if (index === this.contacts.length - 1) { resolve(); }
            });

            this.missing = missing;
            this.errors = errors;
            this.fieldsMissing = fieldsMissing;
            this.fieldsErrors = fieldsError;
        });

        bar.then(() => {
            // sorting of headers => required Reserved > required Custom > optional Reserved > optional Custom
            let orderedFields = [];
            const allFields = this.formService.getSelectedFields();
            const resv = allFields.reserved;
            const cust = allFields.custom;
            const resvReq = resv.filter(r => r.isRequired).map(f => f.value);
            const custReq = cust.filter(c => c.isRequired).map(f => f.label);
            const resvOpt = resv.filter(r => !r.isRequired).map(f => f.value);
            const custOpt = cust.filter(c => !c.isRequired).map(f => f.label);
            orderedFields.push('#');
            if (resvReq.length > 0) { orderedFields = orderedFields.concat(resvReq); }
            if (custReq.length > 0) { orderedFields = orderedFields.concat(custReq); }
            if (resvOpt.length > 0) { orderedFields = orderedFields.concat(resvOpt); }
            if (custOpt.length > 0) { orderedFields = orderedFields.concat(custOpt); }
            orderedFields.push('actions');
            this.displayedColumns = orderedFields;
            const range = 6;
            this.loadingContacts = false;
            this.isValidated = true;
            this.dataSource = new GridTableDataSource(this.contactsPlainArray, this.viewport, this.rowHeight);
            this.dataSource.offsetChange.subscribe(offset => {
                this.placeholderHeight = offset;
            });
            this.dataSource.allData = this.contactsPlainArray;
        });
    }
    getErrorTooltipMessage(control, col): string {
        if (control.hasError('required')) {
            return `Missing ${col}`;
        } else if (control.hasError('minlength')) {
            return `${col} should be more than ${control.errors.minlength.requiredLength} characters`;
        } else if (control.hasError('maxlength')) {
            return `${col} should be less than ${control.errors.maxlength.requiredLength} characters`;
        } else {
            return `invalid ${col}`;
        }
    }
    public changePagination(event): void {
        const filter = this.sectionService.getFilter();
        filter['listId'] = this.listId;
        filter.pageNo = event.pageIndex + 1;
        filter.pageSize = event.pageSize;
        const model = {
            listId: this.listId,
            eventId: this.eventId,
            listAnalysisStep: 1,
            listAnalysisPageNo: filter.pageNo,
            listAnalysisPageSize: filter.pageSize
        };
        this.dataService.setStep(model).subscribe(res => {
            if (res) {
                this.dataService.getContactFields(filter).subscribe(result => {
                    this.importAnalysisForm.controls.contacts = new FormArray([]);
                    this.contacts = result.items;
                    this.totalCount = result.totalCount;
                    this.pushFields();
                    this.refreshTable();
                });
            }
        });

    }
    refreshTable(): void {
        this.contactsArray.controls.forEach(c => c.updateValueAndValidity());
        this.dataSource = new GridTableDataSource(this.contactsPlainArray, this.viewport, this.rowHeight);
    }

    trackByFn(index, item): any {
        return index;
    }

    next(): void {
        this.formService.linear = false;
        this.formService.setStepperIndex(3);
        setTimeout(() => {
            this.formService.linear = true;
        });
    }

    loadLookups(): void {
        if (this.headers.indexOf('nationalityId') > -1) {
            this.appStateService.GetNationalities().subscribe((result) => {
                this.loading = false;
                this.nationalities = result;
            }, error => {
                this.loading = false;
            });
        }
        if (this.headers.indexOf('documentTypeId') > -1) {
            this.appStateService.GetDocumentTypes().subscribe((result) => {
                this.loading = false;
                this.documentTypes = result;
            }, error => {
                this.loading = false;
            });
        }
        if (this.headers.indexOf('issuingCountryId') > -1) {
            this.appStateService.getCountries().subscribe((result) => {
                this.loading = false;
                this.issuingCountries = result;
            }, error => {
                this.loading = false;
            });
        }
        if (this.headers.indexOf('title') > -1) {
            this.titles = ['Mr.', 'Mrs', 'Ms'];
        }
    }

    remerge(): void {
        const vals = JSON.parse(JSON.stringify(this.importAnalysisForm.controls.contacts.value));
        const custHeaders = this.customFieldsHeaderNames;
        vals.forEach(element => {
            const custArr = [];
            for (let i = 0; (i < custHeaders.length) && !element.customFields; i++) {
                const obj = {
                    id: this.customFieldsinHeader[i].id,
                    name: this.customFieldsinHeader[i].name,
                    value: element[custHeaders[i]]
                };
                custArr.push(obj);
                delete element[custHeaders[i]];

            }
            if (custArr.length > 0 && !element.hasOwnProperty('customFields')) {
                element.customFields = custArr;
            } else {
                element.customFields = null;
            }

        });
        return vals;
    }



    cancel(): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
        dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
        dialogRef.componentInstance.confirmButton = 'Leave page';
        dialogRef.componentInstance.cancelButton = 'Stay';
        dialogRef.componentInstance.confirmMessage = 'Are you sure you want to cancel? Cancel at this step will reset your content.\n All changes will be lost.';
        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.dataService.cancelListAnalysis({ guestListId: this.sectionService.listId, eventId: this.eventId }).subscribe(x => {
                    if (x) {
                        this.formService.finalStep = false;
                        this.router.navigate(['../'], { relativeTo: this.route });
                    }
                });

            }
        });
    }

    goBack(): void {
        this.isBack = true;
        const model = {
            listId: this.listId,
            eventId: this.eventId,
            listAnalysisStep: 0,
            listAnalysisPageNo: null,
            listAnalysisPageSize: null
        };
        this.dataService.setStep(model).subscribe(res => {
            if (res) {
                this.isBack = false;
                this.formService.linear = false;
                this.formService.setStepperIndex(0);
                setTimeout(() => { this.formService.linear = true; });
            }
        });

    }

    // Validators
    computeValidators(element): any {
        const validators = [];
        switch (element.customFieldType) {
            case 0:  // Text
                if (element.minValue) {
                    validators.push(Validators.minLength(element.minValue));
                }
                if (element.maxValue) {
                    validators.push(Validators.maxLength(element.maxValue));
                }
                break;

            case 1: // Number
                if (element.minValue) {
                    validators.push(Validators.pattern(RegExp(RegexpPattern.number)));
                    validators.push(this.minDigitsLength(element.minValue));
                }
                if (element.maxValue) {
                    validators.push(Validators.pattern(RegExp(RegexpPattern.number)));
                    validators.push(this.maxDigitsLength(element.maxValue));
                }
                break;

            case 2: // Date
                element.value = element.value ? new Date(element.value) : '';
                if (element.minValue) {
                    validators.push(this.minDate(element.minValue));
                }
                if (element.maxValue) {
                    validators.push(this.maxDate(element.maxValue));
                }
                validators.push(Validators.pattern(RegExp(RegexpPattern.date)));
                break;
            case 3:
                validators.push(Validators.pattern(RegExp(RegexpPattern.email)));
                break;
            case 4:
                validators.push(Validators.pattern(RegExp(RegexpPattern.intlPhone)));
                break;

            default:
                break;
        }
        return validators;
    }

    private isValidGender(): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } | null => {
            if (control.value !== 0 && control.value !== 1 && control.value !== null) {
                return { genderError: true };
            } else {
                return null;
            }
        };
    }

    private minDigitsLength(size: number): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } | null => {
            if (control.value == null) {
                return null;
            }

            const numberr = control.value;
            if (numberr === '' || !numberr || numberr == null) {
                return null;
            }
            if (numberr < size) {
                return { minDigits: true };
            }
            return null;
        };
    }

    private maxDigitsLength(size: number): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } | null => {
            if (control.value == null) {
                return null;
            }

            const numberr = control.value;
            if (numberr === '' || !numberr || numberr == null) {
                return null;
            }


            if (numberr > size) {
                return { maxDigits: true };
            }
            return null;
        };
    }

    private minDate(date: number): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } | null => {
            if (control.value == null) {
                return null;
            }

            const value = Date.parse(control.value);
            if (date > value) {
                return { minDate: true };
            }

            return null;
        };
    }

    private maxDate(date: number): ValidatorFn {
        return (control: FormControl): { [key: string]: boolean } | null => {
            if (control.value == null) {
                return null;
            }

            const value = Date.parse(control.value);
            if (date < value) {
                return { maxDate: true };
            }

            return null;
        };
    }





    validateContacts(): void {
        const headersToLoop = this.headers;
        let missing = 0; // guests
        let errors = 0; // guests
        let fieldsMissing = 0;
        let fieldsError = 0;
        const contactsList = this.contactsArray.value;
        contactsList.forEach((element, index) => {
            const contactt = this.contactsArray.controls[index] as FormGroup;
            if (contactt.invalid) {
                errors++;
                for (let j = 1; j < headersToLoop.length; j++) {
                    if (this.importAnalysisForm.get(`contacts.${index}.${headersToLoop[j]}`).invalid) { fieldsError++; }
                }
                this.fieldsErrors = fieldsError;
            }
            let isMissing = false;
            let i = 0;
            while (i < headersToLoop.length) {
                if (headersToLoop[i] !== '#' && headersToLoop[i] !== 'fullName') {
                    if (element[headersToLoop[i]] === null || element[headersToLoop[i]] === '') {
                        isMissing = true;
                        fieldsMissing++;
                    }
                }
                i++;
            }
            if (isMissing) { missing++; }
        });
        this.fieldsMissing = fieldsMissing;
        this.fieldsErrors = fieldsError;
    }
}
