import { TestBed } from '@angular/core/testing';

import { ImportAnalysisFormService } from './import-analysis-form.service';

describe('ImportAnalysisFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImportAnalysisFormService = TestBed.get(ImportAnalysisFormService);
    expect(service).toBeTruthy();
  });
});
