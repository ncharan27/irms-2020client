import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfiDataReviewModalComponent } from './rfi-data-review-modal.component';

describe('RfiDataReviewModalComponent', () => {
  let component: RfiDataReviewModalComponent;
  let fixture: ComponentFixture<RfiDataReviewModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfiDataReviewModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfiDataReviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
