import { Component, OnInit, Input } from '@angular/core';
import { EventGuestService } from '../../event-guest.service';

@Component({
  selector: 'irms-event-guest-list-guests-item',
  templateUrl: './event-guest-list-guests-item.component.html',
  styleUrls: ['./event-guest-list-guests-item.component.scss'],
  providers: [EventGuestService]
})
export class EventGuestListGuestsItemComponent implements OnInit {
  @Input() data;
  constructor(protected sectionService: EventGuestService) {
    }

  ngOnInit(): void {
  }


}
