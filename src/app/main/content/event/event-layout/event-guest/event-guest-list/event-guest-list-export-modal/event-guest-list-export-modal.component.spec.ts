import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventGuestListExportModalComponent } from './event-guest-list-export-modal.component';

describe('EventGuestListExportModalComponent', () => {
  let component: EventGuestListExportModalComponent;
  let fixture: ComponentFixture<EventGuestListExportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventGuestListExportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventGuestListExportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
