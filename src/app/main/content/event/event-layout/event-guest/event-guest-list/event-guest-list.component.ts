import { Component, OnInit, OnDestroy } from '@angular/core';
import { fuseAnimations } from 'app/core/animations';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { EventGuestModel } from '../event-guest.model';
import { EventGuestListItemComponent } from './event-guest-list-item/event-guest-list-item.component';
import { EventGuestService } from '../event-guest.service';
import { EventGuestDataService } from '../event-guest-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Section } from 'app/constants/constants';
import { EventGuestListCreateComponent } from '../event-guest-list-create/event-guest-list-create.component';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';
import { appInjector } from 'app/main/app-injector';

@Component({
  selector: 'irms-event-guest-list',
  templateUrl: './event-guest-list.component.html',
  styleUrls: ['./event-guest-list.component.scss'],
  animations: fuseAnimations
})
export class EventGuestListComponent extends SectionListComponent<EventGuestModel> implements OnInit, OnDestroy {
  eventId: string = '';
  public listItemComponent: Object = EventGuestListItemComponent;
  listHeaders = ['Name', 'Count', 'Actions'];
  constructor(public sectionService: EventGuestService,
              protected dataService: EventGuestDataService,
              router: Router, public route: ActivatedRoute, private dialog: MatDialog) {
    super(Section.EventGuests, sectionService, dataService, router);
  }

  ngOnInit(): void {
    this.sectionService.subscriptions = [];
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['id'];
      this.sectionService.setFilter({ eventId: params['id'] });
      super.ngOnInit();
      this.sectionService.doRefreshList();
    });
  }
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  // Handle Search 
  searchQueryChange(event): void{
    this.sectionService.resetQueryString(event);
  }

  /// Handle Create New Contact List 
  createList(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '500px';
    dialogConfig.maxWidth = '740px';
    const dialogRef = this.dialog.open(EventGuestListCreateComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data => {
        data.eventId = this.eventId;
        this.dataService.createList(data).subscribe(result => {
          const toasterService = appInjector().get(ToasterService);
          if (result) {
            this.sectionService.doRefreshList();
            toasterService.pop('success', null, 'Successfully created');
          } else {
            toasterService.pop('error', null, 'List name must be unique.');
          }
        });
      });
  }

  goToImportContacts(): any{
    this.router.navigateByUrl(`/global-contacts/upload-contact-list`, {
      state: {
        data: {
          eventId: this.eventId
        }
      }
    });
  }
  goToAddContact(): any{
    this.router.navigateByUrl(`/global-contacts/add-contact`, {
      state: {
        data: {
          eventId: this.eventId
        }
      }
    });
  }

}
