import { Component, OnInit } from '@angular/core';
import { EventService } from '../../event.service';
import { EventDataService } from '../../event-data.service';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { Section } from 'app/constants/constants';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-event-settings',
  templateUrl: './event-settings.component.html',
  styleUrls: ['./event-settings.component.scss'],
  animations: fuseAnimations
})
export class EventSettingsComponent implements OnInit {
  editFeatureMode: boolean;
  editInfoMode: boolean;
  public eventName: string;
  public event: any;

  constructor(public sectionService: EventService,
              protected dataService: EventDataService,
              private dialog: MatDialog,
              private router: Router) {
  }

  ngOnInit() {
    this.sectionService.onCurrentChanged.subscribe(result => this.event = result);
  }

  /// Method to disable other tab when one's being edited
  public toggleEditMode(event, tab): void {
    if (tab === 'info') {
      this.editInfoMode = event;
    } else {
      this.editFeatureMode = event;
    }
  }

  /// Method to disable other tab when one's being edited
  public setEventName(event): void {
    this.eventName = event;
  }

  @PopUp('Are you sure you want to delete Event?')
  public delete(): void{
    this.dataService.delete({ id: this.event.id }).subscribe(() => {
      this.router.navigateByUrl(Section.Events);
      this.sectionService.doRefreshList();
    });
  }
}
