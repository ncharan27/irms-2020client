import { Component, OnInit, ViewChild, ElementRef, NgZone, EventEmitter, Output } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { RegexpPattern, Section, mapsStyle } from 'app/constants/constants';
import { fuseAnimations } from 'app/core/animations';
import { SectionViewEditComponent } from 'app/main/content/components/shared/section/section-view-edit/section-view-edit.component';
import { EventModel } from '../../../event.model';
import { EventService } from '../../../event.service';
import { EventDataService } from '../../../event-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { MapsAPILoader } from '@agm/core';
// import 'rxjs/add/operator/takeUntil';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'irms-event-info',
  templateUrl: './event-info.component.html',
  styleUrls: ['./event-info.component.scss'],
  animations: fuseAnimations
})
export class EventInfoComponent extends SectionViewEditComponent<EventModel> implements OnInit {
  @ViewChild('locationSearch', { read: ElementRef, static: false }) searchElementRef: ElementRef;
  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;

  public eventTypes: any[];
  public mapStyle = mapsStyle;
  public editMode = false;
  public loading = false;

  // map location google
  public placeService: any;
  public placeServiceIsReady = false;
  autocomplete: any;
  lat = 26.302294;
  lng = 50.206738;

  @Output() public onEditMode: EventEmitter<boolean> = new EventEmitter();
  @Output() public eventName: EventEmitter<string> = new EventEmitter();
  editInfoMode = false;
  marker: google.maps.Marker;
  country: google.maps.GeocoderAddressComponent;

  public infoForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
    eventTypeId: ['', [, Validators.maxLength(500)]],
    isLimitedAttendees: [true, [Validators.required]],
    maximumAttendees: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.digits))]],
    overflowAttendees: ['', [Validators.pattern(RegExp(RegexpPattern.digits))]],
    duration: ['', [Validators.required]],
    startDateTime: ['', [Validators.required]],
    endDateTime: ['', [Validators.required]],
    locationSearch: ['', [Validators.required]],
    location: ['', [Validators.required]],
    city: [''],
    region: [''],
    zipCode: [''],
    country: ['', [Validators.required]],
    image: [''],
    latitude: [''],
    longitude: ['']
  });

  constructor(
    public sectionService: EventService,
    protected dataService: EventDataService,
    protected router: Router,
    protected route: ActivatedRoute,
    private appStateService: AppStateService,
    // // public dialog: MatDialog,
    private ngZone: NgZone,
    private mapsAPILoader: MapsAPILoader,
    protected fb: FormBuilder) {

    super(Section.Events, sectionService, dataService, router, route);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.loadLookups();

    // dynamic validators
    this.infoForm.get('isLimitedAttendees').valueChanges.takeUntil(this.componentDestroyed).subscribe((value: boolean) => {
      if (value) {
        this.setValidators('infoForm', 'maximumAttendees', [Validators.required, Validators.pattern(RegExp(RegexpPattern.digits))]);
      } else {
        this.clearValidators('infoForm', ['maximumAttendees']);
      }
    });

    // load google auto complete
    this.infoForm.get('locationSearch').valueChanges
      .pipe(debounceTime(800),
        distinctUntilChanged())
      .subscribe((value) => {
        if (value !== null && this.placeServiceIsReady) {
          if (!this.autocomplete) {
            this.autocomplete = this.createAutocomplete();
          }
        }
      });
    this.loadGoogle();
  }

  deleteEventImage(): void {
    this.infoForm.controls['image'].reset();
  }

  /// Handle Edit Info Tab 
  public editInfo(): void {
    this.editInfoMode = true;
    this.onEditMode.emit(true);
    this.autocomplete = this.createAutocomplete();
  }

  public viewEventInfo(): void {
    this.editInfoMode = false;
    this.onEditMode.emit(false);
  }

  // get look ups
  loadLookups(): void {
    this.appStateService.getEventTypes().subscribe(result => this.eventTypes = result);
    // this.appStateService.getTimeZones().subscribe(result => this.timeZones = result);
  }

  durationChanged(event): void {
    this.infoForm.controls['startDateTime'].setValue(event.value[0].toISOString());
    this.infoForm.controls['endDateTime'].setValue(event.value[1].toISOString());
  }

  // patch
  patchModelInForms(): void {
    this.infoForm.patchValue(this.model);
    this.infoForm.controls.eventTypeId.setValue(this.model.eventType ? this.model.eventType.id : '');
    this.infoForm.controls.image.setValue(this.model.imagePath);

    // emit event name
    this.eventName.emit(this.model.name);

    // duration
    const dur = [];
    dur.push(this.model.startDateTime);
    dur.push(this.model.endDateTime);
    this.infoForm.controls.duration.setValue(dur);

    // location
    this.infoForm.controls.location.setValue(this.model.locationName);
    // this.infoForm.controls.timeZoneName.setValue({ id: this.model.timeZoneUtcOffset, value: this.model.timeZoneName });
    this.setGeoLocation(this.model.latitude, this.model.longitude);
    this.autocomplete = this.createAutocomplete();
  }

  /////////////// geo auto complete//////////////////////
  public loadGoogle(): void {
    this.mapsAPILoader.load().then(() => {
      this.placeService = new google.maps.places.AutocompleteService();
      this.placeServiceIsReady = true;
    });
  }

  public createAutocomplete(): any {
    // load Places Autocomplete
    if (this.searchElementRef) {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        // types: ['(cities)'],
      });

      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.infoForm.controls.city.setValue(place.formatted_address);
          this.model.latitude = place.geometry.location.lat();
          this.model.longitude = place.geometry.location.lng();
          const rsltAdrComponent = place.address_components;
          const location = place.formatted_address;
          const city = rsltAdrComponent.find(x => x.types.includes('locality')) ? rsltAdrComponent.find(x => x.types.includes('locality')).long_name : (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')) ? (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')).long_name) : undefined);
          const region = rsltAdrComponent.find(x => x.types.includes('administrative_area_level_1')).long_name;
          this.country = rsltAdrComponent.find(x => x.types.includes('country'));
          const postalCode = rsltAdrComponent.find(x => x.types.includes('postal_code')) ? rsltAdrComponent.find(x => x.types.includes('postal_code')).long_name : undefined;
          this.infoForm.controls.location.setValue(location);
          this.infoForm.controls.locationSearch.setValue(location);
          this.infoForm.controls.city.setValue(city);
          this.infoForm.controls.region.setValue(region);
          this.infoForm.controls.country.setValue(this.country.long_name);
          this.infoForm.controls.zipCode.setValue(postalCode);
          this.infoForm.controls.latitude.setValue(this.model.latitude);
          this.infoForm.controls.longitude.setValue(this.model.longitude);
        });
      });

      return autocomplete;
    }
  }

  public getLocation(event): void {
    this.model.latitude = event.coords.lat;
    this.model.longitude = event.coords.lng;
    this.setGeoLocation(this.model.latitude, this.model.longitude);
  }

  public setGeoLocation(latitude: number, longitude: number): void {
    if (navigator.geolocation) {
      const geocoder = new google.maps.Geocoder();
      const latlng = new google.maps.LatLng(latitude, longitude);

      geocoder.geocode({ location: latlng }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
          const result = results[0];
          const rsltAdrComponent = result.address_components;
          if (result != null) {
            const location = result.formatted_address;
            const city = rsltAdrComponent.find(x => x.types.includes('locality')) ? rsltAdrComponent.find(x => x.types.includes('locality')).long_name : (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')) ? (rsltAdrComponent.find(x => x.types.includes('administrative_area_level_2')).long_name) : undefined);
            const region = rsltAdrComponent.find(x => x.types.includes('administrative_area_level_1')).long_name;
            this.country = rsltAdrComponent.find(x => x.types.includes('country'));
            const postalCode = rsltAdrComponent.find(x => x.types.includes('postal_code')) ? rsltAdrComponent.find(x => x.types.includes('postal_code')).long_name : undefined;

            this.infoForm.controls.location.setValue(location);
            this.infoForm.controls.locationSearch.setValue(location);
            this.infoForm.controls.city.setValue(city);
            this.infoForm.controls.region.setValue(region);
            this.infoForm.controls.country.setValue(this.country.long_name);
            this.infoForm.controls.zipCode.setValue(postalCode);
            this.infoForm.controls.latitude.setValue(latitude);
            this.infoForm.controls.longitude.setValue(longitude);

          } else {
            alert('No address available!');
          }
        }
      });
    }
  }

  ////////////////// Save Changes ////////////////////
  public updateEventInfo(): void {
    if (this.infoForm.valid) {
      const model = this.sectionService.trimValues(this.infoForm.value);
      model.id = this.model.id;
      model.country = this.country.short_name;
      model.locationName = model.location;
      delete model.location;
      delete model.locationSearch;
      delete model.duration;
      const fd = new FormData();
      for (const key in model) {
        fd.append(key, model[key]);
      }
      this.loading = true;
      this.dataService.update(fd).subscribe(() => {
        this.loading = false;
        super.ngOnInit();
        this.viewEventInfo();
      }, () => {
        this.loading = false;
      }
      );
    }
  }
}
