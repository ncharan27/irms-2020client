import { Component, OnInit, OnDestroy } from '@angular/core';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

@Component({
  selector: 'irms-event-layout',
  templateUrl: './event-layout.component.html',
  styleUrls: ['./event-layout.component.scss']
})
export class EventLayoutComponent implements OnInit, OnDestroy {

  constructor(private sidebarService: FuseSidebarService) { }

  ngOnInit() {
    this.sidebarService.minimizeSidebar(true);
  }
  ngOnDestroy() {
    this.sidebarService.minimizeSidebar(false);
  }

  

}
