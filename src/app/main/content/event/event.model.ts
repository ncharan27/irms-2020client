import { BaseModel } from '../components/shared/section/base.model';

export class EventModel extends BaseModel {
    name: string;
    imagePath: string;
    eventType: any;
    isLimitedAttendees: boolean;
    maximumAttendees: number;
    overflowAttendees: number;
    startDateTime: string;
    endDateTime: string;
    timeZoneName: string;
    timeZoneUtcOffset: string;
    locationName: string;
    city: string;
    region: string;
    zipCode: string;
    country: string;
    latitude: number;
    longitude: number;
    features: string[];
}
