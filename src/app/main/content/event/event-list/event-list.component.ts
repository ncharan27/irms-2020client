import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { EventModel } from '../event.model';
import { IResponseModel } from 'types';
import { Section } from 'app/constants/constants';
import { EventService } from '../event.service';
import { EventDataService } from '../event-data.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { fuseAnimations } from 'app/core/animations';
import { SectionListComponent } from '../../components/shared/section/section-list/section-list.component';

@Component({
  selector: 'irms-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
  animations: fuseAnimations
})
export class EventListComponent extends SectionListComponent<EventModel> implements OnInit, OnDestroy {

  public timeFilter: any;
  response: IResponseModel;
  loading = true;
  loadingMore = false;
  numberOfEventCards;
  @ViewChild('contentArea', { static: true }) container: ElementRef;
  loadingBtn = true;
  constructor(public sectionService: EventService,
    protected dataService: EventDataService,
    protected router: Router, public dialog: MatDialog) {
    super(Section.Events, sectionService, dataService, router);
  }

  ngOnInit(): void {
    // this.numberOfEventCards = Math.floor((this.container.nativeElement as HTMLElement).offsetWidth / 240);
    // const filter = this.sectionService.getFilter();
    // filter.pageSize = this.numberOfEventCards * 2 - 1;
    // this.sectionService.setFilter(filter);
    this.loading = true;
    this.loadingBtn = true;
    const filter = this.sectionService.getFilter();
    // this.dataService.getList(filter).subscribe(data => {
    //   // this.response.items = this.response.items.concat(data.items);
    //   this.response = data; 
    //   this.loading = false;
    //   setTimeout (() => {
    //     this.loadingBtn = false;
    // }, 700);
    // });

    super.ngOnInit();
    this.data.subscribe(data => {
     
      if(data != null && data){
        this.response = data;
        this.loading = false;
        setTimeout (() => {
           this.loadingBtn = false;
       }, 700);
      }
    });
  }

  ngOnDestroy(): void {
    this.sectionService.resetFilter();
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    this.loading = true;
    super.ngOnDestroy();
  }

  // Handle Search 
  searchQueryChange(event): void {
    this.sectionService.resetQueryString(event);
  }

  /// On Time Filter Checkbox change 
  timeFilterChange(event): void {
    this.timeFilter = event;
    this.sectionService.setFilter({ filter: event });
    this.sectionService.refreshList();
  }

  loadMore(): void {
    this.loadingMore = true;
    const filter = this.sectionService.getFilter();
    if (this.response.items.length < this.response.totalCount) {
      filter.pageNo += 1;
      // filter.pageSize = this.loadMoreCount();
      this.sectionService.setFilter(filter);
      this.dataService.getList(filter).subscribe(data => {
        this.response.items = this.response.items.concat(data.items);
        this.loadingMore = false;
      });
    }
  }

  loadMoreCount(): number {
    const currLength = this.response.items.length + 1;
    if (currLength % this.numberOfEventCards === 0) {
      return this.numberOfEventCards * 2;
    } else {
      return 2 * this.numberOfEventCards + (this.numberOfEventCards - (currLength % this.numberOfEventCards));
    }
  }
}
