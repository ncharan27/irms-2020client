import { Component, OnInit } from '@angular/core';
import { SectionComponent } from '../components/shared/section/section.component';
import { EventModel } from './event.model';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { Section } from 'app/constants/constants';
import { MatDialog } from '@angular/material';
import { IActionButtons } from 'types';
import { EventService } from './event.service';
import { EventDataService } from './event-data.service';
import { Router } from '@angular/router';
import { fuseAnimations } from 'app/core/animations';

@Component({
  selector: 'irms-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent extends SectionComponent<EventModel> implements OnInit {

  constructor(public sectionService: EventService,
    protected dataService: EventDataService,
    protected router: Router, public dialog: MatDialog) {
    super(Section.Events, sectionService, dataService, router);
  }
}
