import {AfterViewChecked, Directive, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[irmsDynamicHeight]'
})
export class DynamicHeightDirective implements OnChanges, AfterViewChecked {
  @Input('irmsDynamicHeight') count: any;
  private frontContainer: HTMLElement;
  private dynamicContainer: HTMLElement;
  private backContainer: HTMLElement;

  constructor(private el: ElementRef) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.frontContainer = this.el.nativeElement.querySelector('.fuse-widget-front');
    this.backContainer = this.el.nativeElement.querySelector('.edit-box');
    this.dynamicContainer = this.el.nativeElement.querySelector('.dynamic-box');
    if (changes) {
      setTimeout(() => {
        this.backContainer = this.el.nativeElement.querySelector('.edit-box');
        this.changeHeight();
      }, 40);
    }
  }

  changeHeight() {
    if (this.frontContainer) {
      this.frontContainer.style.height = this.backContainer.clientHeight + 40 + 'px';
    }
  }

  ngAfterViewChecked() {
    if (this.backContainer.clientHeight !== this.frontContainer.clientHeight - 40) {
      this.changeHeight();
    }


  }


}
