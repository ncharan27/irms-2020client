import { Directive, OnInit, AfterViewInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[irmsTextOverflow]'
})
export class TextOverflowDirective implements OnInit, AfterViewInit {
  // private get hasOverflow(): boolean {
  //   const el: HTMLElement = this.el.nativeElement;
  //   return el.offsetWidth < el.scrollWidth;
  // }
  constructor(private elementRef: ElementRef) {}
  
  ngOnInit() {
    // class overflow: text-overflow: ellipsis; overflow: hidden; white-space: nowrap;
    this.elementRef.nativeElement.classList.add('text-overflow');
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      const element = this.elementRef.nativeElement;
      if (element.offsetWidth < element.scrollWidth){
        element.title = element.innerHTML;
      }
    }, 500);
  }
}
