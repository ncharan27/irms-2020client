import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { WhatsappDesignerNavbarComponent } from './whatsapp-designer-navbar/whatsapp-designer-navbar.component';
import { WhatsappDesignerFormComponent } from './whatsapp-designer-form/whatsapp-designer-form.component';
import { WhatsappDesignerService } from './whatsapp-designer.service';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { FuseNavigationModule } from '@fuse/components';
import { LayoutModule } from '@angular/cdk/layout';
import { EditorModule } from '@tinymce/tinymce-angular';
import { RouterModule } from '@angular/router';
import { RsvpFormEditorModule } from '../components/shared/rsvp-form-editor/rsvp-form-editor.module';
import { CanExitGuard } from '../services/can-exit.guard';
import { WhatsappDesignerComponent } from './whatsapp-designer.component';
import { WhatsappDesignerDesignComponent } from './whatsapp-designer-design/whatsapp-designer-design.component';
import { WhatsappPreviewComponent } from '../components/shared/whatsapp-preview/whatsapp-preview.component';
import { WhatsappDesignerSendTestMessageComponent } from './whatsapp-designer-send-test-message/whatsapp-designer-send-test-message.component';



const routes = [
  {
    path: '',
    redirectTo: `:tempId`,
    pathMatch: 'full'
  },
  {
    path: ':tempId',
    component: WhatsappDesignerComponent, 
    canLoad: [AuthGuard], 
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: 'rsvp-form',
        canActivateChild: [AuthGuard],
        component: WhatsappDesignerFormComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,
        canDeactivate: [CanExitGuard]
      },
      {
        path: 'design',
        canActivateChild: [AuthGuard],
        component: WhatsappDesignerDesignComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,        
        canDeactivate: [CanExitGuard]
      },
    ]
  }
];

@NgModule({
  declarations: [
    WhatsappDesignerComponent,
    WhatsappDesignerFormComponent,
    WhatsappDesignerNavbarComponent,
    WhatsappDesignerDesignComponent,
  ],
  imports: [
    MainModule,
    ContentModule,
    RsvpFormEditorModule,
    CommonModule,
    FuseNavigationModule,
    LayoutModule,
    EditorModule,
    // RouterModule.forChild(routes)
  ],
  entryComponents: [WhatsappDesignerSendTestMessageComponent],
  providers: [WhatsappDesignerService],
  exports: [
    // RouterModule,
    WhatsappDesignerComponent,
    WhatsappDesignerDesignComponent,
    
  ]
})
export class WhatsappDesignerModule { }
