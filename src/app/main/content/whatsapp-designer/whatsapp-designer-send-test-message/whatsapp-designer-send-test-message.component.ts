import { Component, OnInit, Inject, Input } from '@angular/core';
import { ForbiddenPhoneCodes, RegexpPattern } from 'app/constants/constants';
import { ENTER, COMMA, SPACE } from '@angular/cdk/keycodes';
import { FormGroup, FormBuilder } from '@angular/forms';
import { WhatsappDesignerDataService } from '../whatsapp-designer-data.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';
import { CurrentUserService } from '../../services/current-user.service';

@Component({
  selector: 'irms-whatsapp-designer-send-test-message',
  templateUrl: './whatsapp-designer-send-test-message.component.html',
  styleUrls: ['./whatsapp-designer-send-test-message.component.scss']
})
export class WhatsappDesignerSendTestMessageComponent implements OnInit {

  public separatorKeysCodes = [ENTER, COMMA, SPACE];
  public phoneNumbersList = [];
  removable = true;
  testMsgForm: FormGroup;
  @Input() id;

  constructor(
    private fb: FormBuilder,
    private sectionService: WhatsappDesignerDataService,
    private dialogRef: MatDialogRef<WhatsappDesignerSendTestMessageComponent>,
    private currentUserService: CurrentUserService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  forbiddenCodes = ForbiddenPhoneCodes;
  ngOnInit(): void {
    this.testMsgForm = this.fb.group({
      phoneNumbers: this.fb.array([]),
    });

    this.currentUserService.getCurrentUser().subscribe((user) => {
      this.phoneNumbersList.push({ value: user.phoneNo, invalid: false });
    });
  }

  addPhoneNumber(event): void {
    if (event.value) {
      if (this.validatePhoneNumber(event.value)) {
        this.phoneNumbersList.push({ value: event.value, invalid: false });
      } else {
        this.phoneNumbersList.push({ value: event.value, invalid: true });
      }
    }
    if (event.input) {
      event.input.value = '';
    }
    this.checkAllValid();
    if (this.testMsgForm.hasError('noPhoneNumbers')) {
      this.testMsgForm.setErrors({ noPhoneNumbers: null });
      this.testMsgForm.updateValueAndValidity();
    }
  }


  removePhoneNumber(data: any): void {
    if (this.phoneNumbersList.indexOf(data) >= 0) {
      this.phoneNumbersList.splice(this.phoneNumbersList.indexOf(data), 1);
    }
    this.checkAllValid();
    if (this.phoneNumbersList.length === 0) {
      this.testMsgForm.setErrors({ noPhoneNumbers: true });
    }
  }

  sendPhoneNumbers(): void {
    const testPhoneNumbers = this.phoneNumbersList.map(phoneNumber => phoneNumber.value);
    this.sectionService
      .sendTestMsg({
        whatsappList: testPhoneNumbers,
        invitationId: this.id
      }).subscribe(result => {
        const toasterService = appInjector().get(ToasterService);
        toasterService.pop('success', null, 'Successfully sent messages.');
      })

    this.dialogRef.close();
  }

  private checkAllValid(): void {
    let invalidFound = false;
    this.phoneNumbersList.forEach(phoneNumbers => {
      if (phoneNumbers.invalid) {
        invalidFound = true;
        this.testMsgForm.setErrors({ incorrectPhoneNumber: true });
      }
    });
    if (!invalidFound && this.testMsgForm.hasError('incorrectPhoneNumber')) {
      this.testMsgForm.setErrors({ incorrectPhoneNumber: null });
      this.testMsgForm.updateValueAndValidity();
    }

    if (this.phoneNumbersList.length > 10) {
      this.testMsgForm.setErrors({ overflow: true });
    } else if (this.testMsgForm.hasError('overflow')) {
      this.testMsgForm.setErrors({ overflow: null });
      this.testMsgForm.updateValueAndValidity();
    }
  }

  private validatePhoneNumber(phoneNumber: string): any {
    let isForbidden = false;
    this.forbiddenCodes.forEach(code => {
      if (phoneNumber.includes(code)) {
        isForbidden = true;
      }
    });
    if (isForbidden) {
      return false;
    }
    const pattern = new RegExp(RegexpPattern.intlPhone);
    return pattern.test(String(phoneNumber).toLowerCase());
  }

  /// On dialog close
  close(): void {
    this.dialogRef.close();
  }
}
