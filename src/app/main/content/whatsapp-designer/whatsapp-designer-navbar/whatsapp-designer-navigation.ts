import { FuseNavigation } from '@fuse/types';

export const WhatsappDesignerNavigation: FuseNavigation[] = [
    {
        id: 'form',
        title: 'Form',        
        translate: '',
        type: 'item',
        icon: 'ballot',
        url: `rsvp-form`
    },
    {
        id: 'design',
        title: 'Design',        
        translate: '',
        type: 'item',
        icon: 'edit',
        url: `design`
    }
];

export const WhatsappDesignerNavigationWithoutRsvp: FuseNavigation[] = [
    {
        id: 'design',
        title: 'Design',        
        translate: '',
        type: 'item',
        icon: 'edit',
        url: `design`
    }
]
