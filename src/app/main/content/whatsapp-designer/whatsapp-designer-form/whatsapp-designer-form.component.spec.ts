import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappDesignerFormComponent } from './whatsapp-designer-form.component';

describe('WhatsappDesignerFormComponent', () => {
  let component: WhatsappDesignerFormComponent;
  let fixture: ComponentFixture<WhatsappDesignerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappDesignerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappDesignerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
