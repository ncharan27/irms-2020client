import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { WhatsappDesignerService } from '../whatsapp-designer.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { CanExit } from '../../services/can-exit.guard';
import { WhatsappDesignerDataService } from '../whatsapp-designer-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { RsvpFormService } from '../../components/shared/rsvp-form/rsvp-form.service';
import { ChangePasswordComponent } from 'app/account/change-password/change-password.component';

@Component({
  selector: 'irms-whatsapp-designer-form',
  templateUrl: './whatsapp-designer-form.component.html',
  styleUrls: ['./whatsapp-designer-form.component.scss']
})
export class WhatsappDesignerFormComponent implements OnInit, OnDestroy, CanExit {
  invitationId: string = '';
  public rsvpForm = this.fb.group(
    {
      formContent: this.fb.group({
        welcome: this.fb.group({
          content: [`<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
      <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`, [Validators.required]],
          button: ['Proceed', [Validators.required]]
        }),
        rsvp: this.fb.group({
          content: [`<p><span style="font-family: tahoma, arial, helvetica, sans-serif;">Do you accept the Invitation to the&nbsp;</span></p>
      <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p><span style="color: #34495e; font-family: 'arial black', sans-serif;">Select your Response:&nbsp;</span></p>`, [Validators.required]],
          acceptButton: [`Yes I'll be there`, [Validators.required]],
          rejectButton: [` Nope I won't be there`, [Validators.required]]
        }),
        accept: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">&nbsp;</p>`, [Validators.required]]
        }),
        reject: this.fb.group({
          content: [`<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
      <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
      <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
      <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`, [Validators.required]]
        })
      }),
      formTheme: this.fb.group({
        buttons: this.fb.group({
          background: '#576268',
          text: '#fff',
        }),
        background: this.fb.group({
          color: '#fff',
          image: '',
          style: '',
        }),
      }),
      formSettings: this.fb.group({
        title: ['']
      })
    });
  subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private designerService: WhatsappDesignerService,
    private dataService: WhatsappDesignerDataService,
    private route: ActivatedRoute,
    private rsvpServoce: RsvpFormService
  ) {
    this.rsvpServoce.getRsvpForm().subscribe(x => {
        this.rsvpForm.setValue(x)
    })
  }

  ngOnInit() {
    this.designerService.fieldsFilled.next(true);
    this.subscriptions.push(this.designerService.saveObs().subscribe(data => this.saveForm()));
    this.route.params.subscribe((params: Params) => {
      this.invitationId = params['tempId'];
    })
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  saveForm(): void {
    /// Save RSVP response form
    const data = this.rsvpForm.value;

    let backgroundImage = data.formTheme.background.image;
    data.formTheme.background.image = '';

    const payload = {
      campaignInvitationid: this.invitationId,
      rsvpHtml: data.formContent.rsvp.content,
      welcomeHtml: data.formContent.welcome.content,
      acceptHtml: data.formContent.accept.content,
      rejectHtml: data.formContent.reject.content,
      themeJson: JSON.stringify(data.formTheme),
      formSettings: data.formSettings,
      proceedButtonText: data.formContent.welcome.button,
      acceptButtonText: data.formContent.rsvp.acceptButton,
      rejectButtonText: data.formContent.rsvp.rejectButton,
    }

    if (backgroundImage) {
      payload['backgroundImagePath'] = backgroundImage;
    }

    const fd = new FormData();
    for (const key in payload) {
      fd.append(key, payload[key]);
    }

    this.dataService.saveResponse(fd)
      .subscribe(x => { });
  }

  canDeactivate(): any {
    if (this.rsvpForm.dirty) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'You have unsaved data...\nDo you want to leave without saving?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }
}
