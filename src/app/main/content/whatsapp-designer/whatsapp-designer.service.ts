import { Injectable } from '@angular/core';
import { SectionService } from '../components/shared/section/section.service';
import { WhatsappDesignerModel } from './whatsapp-designer.model';
import { WhatsappDesignerDataService } from './whatsapp-designer-data.service';
import { QueryService } from '../services/query.service';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WhatsappDesignerService extends SectionService<WhatsappDesignerModel> {
  public templateId;
  public eventId;
  public campaignId;
  public fieldsFilled = new BehaviorSubject<boolean>(false);
  public unstagedChanges = new BehaviorSubject<boolean>(false);
  public invitationId = new BehaviorSubject<string>('');
  private refreshForm = new Subject<any>();
  private landingPage = new Subject<any>();
  private botPage = new Subject<any>();
  private previewTemplate = new Subject<any>();

  private templateData = new Subject<any>();
  private save = new Subject<any>();
  private sendTest = new Subject<any>();

  public template:any;
  rvspFormValue: any;
  listName = ''
  constructor(
    protected dataService: WhatsappDesignerDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router)
    this.template = {
      senderName: '',
      body: '',
      method: ''
    }
  }

  setTemplate(data: any): void {
    this.templateData.next(data);
    this.template = data;
  }

  getTemplate(): Observable<any> {
    return this.templateData.asObservable();
  }

  triggerSave(): void {
    this.save.next();
  }

  saveObs(): Observable<any> {
    return this.save.asObservable();
  }

  triggerSendTest(data: any): void {
    this.sendTest.next(data);
  }

  sendTestObs(): Observable<any> {
    return this.sendTest.asObservable();
  }

  // Refreshing Form
  triggerRefresh(): void {
    this.refreshForm.next();
  }

  getRefreshTrigger(): Observable<any> {
    return this.refreshForm.asObservable();
  }

  // Landing Page Trigger
  triggerLandingPage(): void {
    this.landingPage.next();
  }

  triggerLandingPageObs(): Observable<any> {
    return this.landingPage.asObservable();
  }

  // Bot Page Trigger
  triggerBotPage(): void {
    this.botPage.next();
  }

  triggerBotPageObs(): Observable<any> {
    return this.botPage.asObservable();
  }

  // Preview Slider Trigger
  triggerPreview(): void {
    this.previewTemplate.next();
  }

  triggerPreviewObs(): Observable<any> {
    return this.previewTemplate.asObservable();
  }

  initDefaultForm(): void {
    const whatsapp = this.template;
    whatsapp.senderName = '',
      whatsapp.body = '';
    this.setTemplate(whatsapp);
    this.rvspFormValue = {
      rsvpHtml: `<p><span style="font-family: tahoma, arial, helvetica, sans-serif;"> Do you accept the Invitation to the& nbsp; </span></p >
        <h2 style="text-align: left;" > <span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;" >& nbsp; The Fanciest Event Ever & nbsp; </span></h2 >
          <p><span style="color: #34495e; font-family: 'arial black', sans-serif;" > Select your Response:& nbsp; </span></p >`,
      welcomeHtml: `<p style="text-align: left;" > <span style="font-family: arial, helvetica, sans-serif;" > You are invited to < /span></p >
              <h2 style="text-align: left;" > <span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;" >& nbsp; The Fanciest Event Ever & nbsp; </span></h2 >
                <p style="text-align: left;" > <span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;" > on 23rd April 2020, 6: 00 PM < /span></p >
                  <h5 style="text-align: left;" > <span style="color: #34495e;" > <em><span style="font-family: tahoma, arial, helvetica, sans-serif;" > Please let us know if you will be joining us < /span></em > </span></h5 >`,
      acceptHtml: `<p style="text-align: center;" > <span style="font-family: tahoma, arial, helvetica, sans-serif;" > Thank you for your response! < /span></p >
                      <p style= "text-align: center;" > <span style= "font-family: tahoma, arial, helvetica, sans-serif;" > We will see you at the & nbsp; </span></p >
                        <h2 style="text-align: center;" > <span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;" >& nbsp; The Fanciest Event Ever & nbsp; </span></h2 >
                          <p style="text-align: center;" >& nbsp; </p>`,
      rejectHtml: `<p style="text-align: center;" > <span style="font-family: tahoma, arial, helvetica, sans-serif;" > Thank you for your response! < /span></p >
      <p style= "text-align: center;" > <span style= "font-family: tahoma, arial, helvetica, sans-serif;" > We were expecting you to see at the & nbsp;& nbsp; </span></p >
        <h2 style="text-align: center;" > <span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;" >& nbsp; The Fanciest Event Ever & nbsp; </span></h2 >
          <p style="text-align: center;" > Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`,
      themeJson: `{ "buttons": { "background": "#576268", "text": "#fff" }, "background": { "color": "#fff", "image": "", "style": "" } }`,
      formSettings: '',
      proceedButtonText: 'Proceed',
      acceptButtonText: `Yes I'll be there`,
      rejectButtonText: `Nope I won't be there`
    }
  }

  }
