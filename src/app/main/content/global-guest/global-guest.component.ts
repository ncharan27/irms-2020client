import { Component, OnInit } from '@angular/core';
import { SectionComponent } from '../components/shared/section/section.component';
import { GlobalGuestService } from './global-guest.service';
import { GlobalGuestDataService } from './global-guest-data.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Section } from 'app/constants/constants';
import { GlobalGuestModel } from './global-guest.model';

@Component({
  selector: 'irms-global-guest',
  templateUrl: './global-guest.component.html',
  styleUrls: ['./global-guest.component.scss']
})
export class GlobalGuestComponent extends SectionComponent<GlobalGuestModel> implements OnInit {

  constructor(public sectionService: GlobalGuestService,
              protected dataService: GlobalGuestDataService,
              protected router: Router, public dialog: MatDialog) {
      super(Section.GlobalGuest, sectionService, dataService, router);

     }

  ngOnInit() {
  }

}
