import { Component, Input } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { EventGuestDataService } from 'app/main/content/event/event-layout/event-guest/event-guest-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';
import { sideDialogConfig } from 'app/constants/constants';
import { DialogAnimationService } from 'app/main/content/services/dialog-animation.service';

@Component({
  selector: 'irms-global-guest-list-item',
  templateUrl: './global-guest-list-item.component.html',
  styleUrls: ['./global-guest-list-item.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestListItemComponent {
  @Input() data: any;
  eventId: string;

  constructor(
    private dialog: MatDialog,
    private dataService: EventGuestDataService,
    public animDialog: DialogAnimationService
  ) { }

  ngOnInit() {
  }

  export(): void {
    const dialogConfig:any = sideDialogConfig;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    // dialogConfig.height = '55vh';
    dialogConfig.data = {
      name: this.data.name
    }
    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);
    dialogRef.afterClosed()
    .subscribe(result => {
      if (result) {
        const columns = result.columns;
        this.dataService
          .exportContacts({
            contactListId: this.data.id,
            filename: this.data.name.listName,
            columns: columns,
            customFieldIds: result.customFieldIds
            //eventId: this.eventId
          }).subscribe(resp => {
            this.saveExportedContacts(resp);
          });
      }
    })
  }

  private saveExportedContacts(resp: any) {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.data.name}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
}
