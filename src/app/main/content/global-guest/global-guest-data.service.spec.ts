import { TestBed } from '@angular/core/testing';

import { GlobalGuestDataService } from './global-guest-data.service';

describe('GlobalGuestDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalGuestDataService = TestBed.get(GlobalGuestDataService);
    expect(service).toBeTruthy();
  });
});
