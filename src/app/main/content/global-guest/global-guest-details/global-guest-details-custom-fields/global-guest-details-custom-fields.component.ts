import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormArray, FormBuilder, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { GlobalGuestService } from '../../global-guest.service';
import { GlobalGuestDataService } from '../../global-guest-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { RegexpPattern } from 'app/constants/constants';
import { EventGuestService } from 'app/main/content/event/event-layout/event-guest/event-guest.service';
import { EventGuestDataService } from 'app/main/content/event/event-layout/event-guest/event-guest-data.service';

@Component({
  selector: 'irms-global-guest-details-custom-fields',
  templateUrl: './global-guest-details-custom-fields.component.html',
  styleUrls: ['./global-guest-details-custom-fields.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestDetailsCustomFieldsComponent implements OnInit {
  public isLocked = true;
  loading
  public customFields = new FormArray([]);
  guestId;
  listId;
  model: {
    fields: []
  };
  filter;

  constructor(protected fb: FormBuilder,
              public sectionService: EventGuestService,
              protected dataService: EventGuestDataService,
              protected route: ActivatedRoute,
              private appStateService: AppStateService) {
    }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.guestId = params['gid'];
      this.listId = params['lid'];
      this.filter = {
        guestId: this.guestId
      };
      this.loadFields();
    });
  }

  updateGuestInfo() {
    // campaign id param
    this.dataService.getEventGuestBasicInfo(this.listId, this.guestId).subscribe(data => {
      this.sectionService.setGuestInfo(data)
      this.sectionService.setSelectedGuestName(data.fullName);
    });    
  }

  loadFields() {
    this.dataService.getGuestCustomFields(this.filter).subscribe(data => {
      this.model = data;
      this.addFields(data);
    });
  }

  addFields(data): void {
    this.customFields.clearValidators();
    data.fields.forEach(element => {
      let validators = [];
      switch(element.customFieldType)
      {
        default:
        case 0:  //Text
          let textFC = new FormControl(element.value);
          if(element.minValue) {
            validators.push(Validators.minLength(element.minValue))
          }
          if(element.maxValue) {
            validators.push(Validators.maxLength(element.maxValue))
          }
          
          break;

        case 1: // Number
          if(element.minValue) {
            validators.push(Validators.pattern(RegExp(RegexpPattern.number)));
            validators.push(this.minDigitsLength(element.minValue))
          }
          if(element.maxValue) {
            validators.push(Validators.pattern(RegExp(RegexpPattern.number)));
            validators.push(this.maxDigitsLength(element.maxValue))
          }
          
          break;

        case 2: // Date
          element.value = element.value ? new Date(element.value) : "";
          if(element.minValue) {
            validators.push(this.minDate(element.minValue));
          }
          if(element.maxValue) {
            validators.push(this.maxDate(element.maxValue));
          }
          validators.push(Validators.pattern(new RegExp(RegexpPattern.date)));

          break;
      }
      let fc = new FormControl(element.value ? element.value : "");
      fc.clearValidators();
      fc.setValidators(validators);
      this.customFields.push(fc);
    });
  }

  toggleLock(): void{
    this.customFields = new FormArray([]);
    this.addFields(this.model)
    this.isLocked = !this.isLocked;
  }

  update(): void {
    this.loading = true;
    const formData = this.model as any;
    formData.fields.forEach((field, i) => {
      field.value = this.customFields.controls[i].value;
    });

    
    // call update API 
    this.dataService.updateGuestCustomFields(this.guestId, formData)
    .subscribe(() => {
      this.loadFields();
      this.toggleLock();
      this.updateGuestInfo();
      this.loading = false;
      
    });
  }

  private minDigitsLength(size: number): ValidatorFn {
    return (control: FormControl) : { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      let number = control.value;
      if(number == '' || !number || number == null){
        return null
      }
      if(number < size)
        return { minDigits: true }
      return null;
    }
  }

  private maxDigitsLength(size: number): ValidatorFn {
    return (control: FormControl) : { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      let number = control.value;
      if(number == '' || !number || number == null){
        return null
      }
      
      if(number > size)
        return { maxDigits: true }
      return null;
    }
  }

  private minDate(date: number): ValidatorFn {
    return (control: FormControl) : { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      let value = Date.parse(control.value);
      if (date > value) {
        return { minDate: true }
      }

      return null;
    }
  }

  private maxDate(date: number): ValidatorFn {
    return (control: FormControl) : { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      let value = Date.parse(control.value);
      if (date < value) {
        return {maxDate: true}
      }
      
      return null;
    }
  }

  private checkValid():void{
    console.log(this.customFields);
  }
}
