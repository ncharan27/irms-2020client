import { Component, OnInit, Input } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { GlobalGuestDetailsEventsHistoryDialogComponent } from '../global-guest-details-events-history-dialog/global-guest-details-events-history-dialog.component';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'irms-global-guest-details-events-history-list-item',
  templateUrl: './global-guest-details-events-history-list-item.component.html',
  styleUrls: ['./global-guest-details-events-history-list-item.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestDetailsEventsHistoryListItemComponent implements OnInit {
 @Input() data: any;
 
  constructor(private dialog: MatDialog, public route: ActivatedRoute) { }

  ngOnInit() {
  }

  openDetails(){
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.minWidth = '40vw';
      this.route.params.subscribe((params: Params) => {
        dialogConfig.data = { 
          guest: this.data,
          gId: params['gid']
        };
      });
      
    
      const dialogRef = this.dialog.open(GlobalGuestDetailsEventsHistoryDialogComponent, dialogConfig);
    
  }



}
