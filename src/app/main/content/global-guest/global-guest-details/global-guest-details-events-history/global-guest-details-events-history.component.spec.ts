import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsEventsHistoryComponent } from './global-guest-details-events-history.component';

describe('GlobalGuestDetailsEventsHistoryComponent', () => {
  let component: GlobalGuestDetailsEventsHistoryComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsEventsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsEventsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsEventsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
