import { Component, OnInit } from '@angular/core';
import { GlobalGuestService } from '../../global-guest.service';
import { GlobalGuestDataService } from '../../global-guest-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Section } from 'app/constants/constants';
import { fuseAnimations } from '@fuse/animations';
import { SectionListComponent } from 'app/main/content/components/shared/section/section-list/section-list.component';
import { GlobalGuestModel } from '../../global-guest.model';
import { GlobalGuestDetailsEventsHistoryListItemComponent } from './global-guest-details-events-history-list-item/global-guest-details-events-history-list-item.component';
import { of } from 'rxjs';
import { GlobalGuestDetailsEventHistoryDataService } from './global-guest-details-event-history-data.service';
import { GlobalGuestDetailsEventHistoryService } from './global-guest-details-event-history.service';

@Component({
  selector: 'irms-global-guest-details-events-history',
  templateUrl: './global-guest-details-events-history.component.html',
  styleUrls: ['./global-guest-details-events-history.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestDetailsEventsHistoryComponent extends SectionListComponent<GlobalGuestModel> implements OnInit {
  childComponent: Object = GlobalGuestDetailsEventsHistoryListItemComponent;
  public selectable = false;
  contactPage
  id = 1;
  constructor(
    public sectionService: GlobalGuestDetailsEventHistoryService,
    protected dataService: GlobalGuestDetailsEventHistoryDataService,
    router: Router, public route: ActivatedRoute, private dialog: MatDialog
  ) { 
    super(Section.GlobalGuest, sectionService, dataService, router);
  }

  ngOnInit() {
    this.sectionService.subscriptions = [];
    this.route.params.subscribe(params => {
      this.id = params['gid'];

      this.contactPage = {
        guestId : this.id
      }
    })
    this.sectionService.setFilter(this.contactPage);
    super.ngOnInit();
  }


  ngOnDestroy(){
    super.ngOnDestroy();
  }
  

}
