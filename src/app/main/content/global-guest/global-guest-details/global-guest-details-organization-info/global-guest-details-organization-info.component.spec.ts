import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsOrganizationInfoComponent } from './global-guest-details-organization-info.component';

describe('GlobalGuestDetailsOrganizationInfoComponent', () => {
  let component: GlobalGuestDetailsOrganizationInfoComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsOrganizationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsOrganizationInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsOrganizationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
