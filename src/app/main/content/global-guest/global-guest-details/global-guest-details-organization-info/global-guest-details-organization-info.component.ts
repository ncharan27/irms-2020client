import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { GlobalGuestDataService } from '../../global-guest-data.service';
import { ActivatedRoute, Params } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { EventGuestService } from 'app/main/content/event/event-layout/event-guest/event-guest.service';
import { EventGuestDataService } from 'app/main/content/event/event-layout/event-guest/event-guest-data.service';

@Component({
  selector: 'irms-global-guest-details-organization-info',
  templateUrl: './global-guest-details-organization-info.component.html',
  styleUrls: ['./global-guest-details-organization-info.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestDetailsOrganizationInfoComponent implements OnInit {
  public organizationForm = this.fb.group({
    organization: ['', [Validators.minLength(2), Validators.maxLength(50)]],
    position: ['', [Validators.minLength(2), Validators.maxLength(50)]],
  });
  guestId: any;
  guestListId:any;
  model;
  editInfoMode = false;
  constructor(protected fb: FormBuilder, 
    protected dataService: EventGuestDataService, 
    protected route: ActivatedRoute,
    protected service: EventGuestService
    ) { }

  ngOnInit(): void {
    // campaign id param
    this.route.params.subscribe((params: Params) => {
      this.guestId = params['gid'];     
      this.guestListId = params['lid'];
      this.loadUserInfo(); 
    });
  }
  updateGuestInfo() {
    // campaign id param
    this.dataService.getEventGuestBasicInfo(this.guestListId, this.guestId).subscribe(data => {
      this.service.setGuestInfo(data)
      this.service.setSelectedGuestName(data.fullName);
    });    
  }

  private loadUserInfo(): void {
    this.dataService.getEventGuestOrganizationInfo(this.guestId).subscribe(data => {
      this.model = data;
      this.organizationForm.patchValue(data);
    });
  }

  /// Handle Edit Info Tab 
  public editInfo(): void {
    this.organizationForm.patchValue(this.model);
    this.editInfoMode = true;
  }

  public viewGuestInfo(): void {
    this.editInfoMode = false;
  }

  // Update Guest organization
  updateGuest(): void {
    const formData = this.organizationForm.value;
    // Call API to update guest organization

    this.dataService.updateEventGuestOrganizationInfo({
      ...formData,
      id: this.guestId
    })
    .subscribe(_ => {
      this.viewGuestInfo();
      this.loadUserInfo();
      this.updateGuestInfo();
    });

    // after call
  }

}
