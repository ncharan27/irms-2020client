import { Component, OnInit } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { GlobalContactService } from '../../global-contact/global-contact.service';
import { EventGuestService } from '../../event/event-layout/event-guest/event-guest.service';
import { GlobalContactDataService } from '../../global-contact/global-contact-data.service';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'irms-global-guest-details',
  templateUrl: './global-guest-details.component.html',
  styleUrls: ['./global-guest-details.component.scss'],
  animations: fuseAnimations
})
export class GlobalGuestDetailsComponent implements OnInit {
  contactName = 'Guest Details';
  contact;
  contactId;
  guest;
  public tabGroup = [
    {
      label: 'Personal Information',
      link: './personal-info'
    },
    {
      label: 'Organization Information',
      link: './organization-info'
    },
    {
      label: 'Event History',
      link: './event-history'
    },
    {
      label: 'Custom Fields',
      link: './custom-fields'
    },
    {
      label: 'Associated Lists',
      link: './associated-lists'
    }
  ];
  subscription: Subscription;
  private unsubscribeAll: Subject<any>;
  constructor(protected service: GlobalContactService,
    protected headerService: EventGuestService,
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    protected dataService: GlobalContactDataService) {
    this.unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.headerService.getGuestInfo()
      .subscribe(guest => {
        this.guest = guest;
      })
    this.service.getSelectedContactName()
      .subscribe(x => {
        this.contactName = x;

      })
    this.loadBasicInfo();
  }

  private loadBasicInfo(): void {
    this.route.params.subscribe((params: Params) => {
      this.contactId = params['gid'];
      this.dataService.getGlobalContactBasicInfo(params['gid'], params['lid']).subscribe(data => {
        this.contact = data;
        this.headerService.setGuestInfo(data)
        this.service.setSelectedContactName(data.fullName);
      });
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
    this.service.resetFilter();
  }

  @PopUp('Are you sure want to delete this contact from the system?')
  public delete(): void {
    var ids = [];
    ids.push(this.contactId);
    this.dataService.deleteContacts({
      ids: ids
    }).subscribe(() => {
      this.router.navigate(['../../'], { relativeTo: this.route });
    });
  }
}
