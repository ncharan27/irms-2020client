import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestDetailsPersonalInfoComponent } from './global-guest-details-personal-info.component';

describe('GlobalGuestDetailsPersonalInfoComponent', () => {
  let component: GlobalGuestDetailsPersonalInfoComponent;
  let fixture: ComponentFixture<GlobalGuestDetailsPersonalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestDetailsPersonalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestDetailsPersonalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
