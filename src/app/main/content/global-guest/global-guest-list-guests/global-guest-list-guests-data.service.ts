import { SectionDataService } from '../../components/shared/section/section-data.service';
import { Injectable } from '@angular/core';
import { GlobalGuestModel } from '../global-guest.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/constants/constants';
import { IResponseModel } from 'types';

@Injectable({
    providedIn: 'root'
})
export class GlobalGuestListGuestsDataService implements SectionDataService<GlobalGuestModel> {
    private url = `${BASE_URL}api/contact`;
    private dataUrl = `${BASE_URL}api/dataModule`;

    constructor(private http: HttpClient) { }

    getList(filterParam: any): Observable<any> {
        return this.http.post<any>(`${this.dataUrl}/global-guest-list`, filterParam);
    }

    getListName(data): Observable<any> {
        return this.http.post(`${this.url}/list-name`, data);
    }

    get(id: any): Observable<GlobalGuestModel> {
        throw new Error("Method not implemented.");
    }
    create(model: any): Observable<string> {
        throw new Error("Method not implemented.");
    }
    update(model: any): Observable<any> {
        throw new Error("Method not implemented.");
    }
    delete(model: any): Observable<any> {
        throw new Error("Method not implemented.");
    }
}