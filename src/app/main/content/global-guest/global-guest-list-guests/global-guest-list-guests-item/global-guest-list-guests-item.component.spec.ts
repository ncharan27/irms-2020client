import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalGuestListGuestsItemComponent } from './global-guest-list-guests-item.component';

describe('GlobalGuestListGuestsItemComponent', () => {
  let component: GlobalGuestListGuestsItemComponent;
  let fixture: ComponentFixture<GlobalGuestListGuestsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalGuestListGuestsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalGuestListGuestsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
