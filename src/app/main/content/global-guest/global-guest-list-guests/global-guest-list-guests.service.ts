import { Injectable } from "@angular/core";
import { SectionService } from '../../components/shared/section/section.service';
import { GlobalGuestModel } from '../global-guest.model';
import { Router } from '@angular/router';
import { QueryService } from '../../services/query.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { GlobalGuestListGuestsDataService } from './global-guest-list-guests-data.service';

@Injectable({
    providedIn: 'root'
})
export class GlobalGuestListGuestsService extends SectionService<GlobalGuestModel>{
    setSelectedGuestName(name: string): void {
        this.selectedGuestName.next(name);
    }
    private selectedGuestName = new BehaviorSubject<any>('');

    constructor(
        protected dataService:GlobalGuestListGuestsDataService,
        queryService:QueryService,
        protected router: Router
    ) {
        super(dataService, queryService, router);
    }

    public getSelectedGuestName(): Observable<any>{
        return this.selectedGuestName.asObservable();
    }
    public setSelectedContactName(name: string):void{
        return this.selectedGuestName.next(name);
    }
}
