import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormDesignerComponent } from './form-designer.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { LayoutModule } from '@angular/cdk/layout';
import { EditorModule } from '@tinymce/tinymce-angular';
import { RouterModule } from '@angular/router';
import { FormDesignerNavbarComponent } from './form-designer-navbar/form-designer-navbar.component';
import { FormBuilderModule } from '../components/shared/form-builder/form-builder.module';

const routes = [
  {
    path: '',
    redirectTo: `:iid`,
    pathMatch: 'full'
  },
  {
    path: ':iid',
    component: FormDesignerComponent, 
    canLoad: [AuthGuard], 
    data: { expectedRoles: [RoleType.TenantAdmin] }
  }];

@NgModule({
  declarations: [FormDesignerComponent, FormDesignerNavbarComponent],
  imports: [
    MainModule,
    ContentModule,
    CommonModule,
    LayoutModule,
    FormBuilderModule,
    EditorModule,
    RouterModule.forChild(routes)
  ],
  // providers: [SmsDesignerService],
  exports: [RouterModule],
})
export class FormDesignerModule { }
