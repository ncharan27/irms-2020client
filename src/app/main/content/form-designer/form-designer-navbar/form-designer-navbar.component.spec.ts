import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDesignerNavbarComponent } from './form-designer-navbar.component';

describe('FormDesignerNavbarComponent', () => {
  let component: FormDesignerNavbarComponent;
  let fixture: ComponentFixture<FormDesignerNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDesignerNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDesignerNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
