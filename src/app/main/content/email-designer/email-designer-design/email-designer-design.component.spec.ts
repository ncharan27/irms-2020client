import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailDesignerDesignComponent } from './email-designer-design.component';

describe('EmailDesignerDesignComponent', () => {
  let component: EmailDesignerDesignComponent;
  let fixture: ComponentFixture<EmailDesignerDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailDesignerDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailDesignerDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
