import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';
import { SectionDataService } from 'app/main/content/components/shared/section/section-data.service';
import { IResponseModel } from 'types';
import { EmailDesignerModel } from './email-designer.model';

@Injectable({
  providedIn: 'root'
})
export class EmailDesignerDataService implements SectionDataService<EmailDesignerModel> {

  private url = `${BASE_URL}api/campaign`;
  private campaignInvitationUrl = `${BASE_URL}api/campaignInvitation`;

  constructor(private http: HttpClient) { }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/${id}/email`);
  }

  getDataReview(id): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/${id}/email/data-review`);
  }

  @Toast('Successfully updated')
  update(model) {
    return this.http.put(`${this.url}`, model);
  }

  getList(filterParam): Observable<IResponseModel> {
    return of();
  }

  getPlaceholders(filterParam, invitationId, tempId, response): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.campaignInvitationUrl}/placeholder`, {
      ...filterParam,
      invitationId,
      templateTypeId: tempId,
      response: response
    });
  }

  getPlaceholdersDataReview(filterParam, invitationId): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.campaignInvitationUrl}/placeholder/data-review`, {
      ...filterParam,
      invitationId,
      templateTypeId: 0
    });
  }

  @Toast('Successfully updated')
  createOrUpdate(model: any) {
    return this.http.post<IResponseModel>(`${this.campaignInvitationUrl}/email-template`, model);
  }

  @Toast('Successfully sent')
  sendTestEmail(model: any) {
    return this.http.post<IResponseModel>(`${this.campaignInvitationUrl}/test-email`, model);
  }

  @Toast('Successfully updated')
  updateResponseForm(model: any): Observable<IResponseModel> {
    return this.http.put<IResponseModel>(`${this.campaignInvitationUrl}/email-response-form`, model);
  }

  create(model: any): Observable<string> {
    throw new Error('Method not implemented.');
  }

  delete(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }

  invitationInfo(id: any): Observable<any> {
    return this.http.get<any>(`${this.campaignInvitationUrl}/invitation-info/${id}`);
  }

  getDefaultTemplate(): Observable<any> {
    return of ({
    id : '93b0287d-ebed-41af-8791-158c74bfec93',
    campaignInvitationId : 'bd40682d-a1b0-4a20-ba64-631a6c559acc',
    subject : 'fsdfds',
    senderEmail : 'ee@ee.eee',
    body : '<p>wqeweqewqewq</p>',
    plainBody : 'wqeweqewqewq',
    acceptHtml : `<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">Thank you for your response!</span></p><p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">We will see you at the&nbsp;</span></p><h2 style=\"text-align: center;\"><span style=\"background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2><p style=\"text-align: center;\">&nbsp;</p>`,
    rejectHtml : `<p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">Thank you for your response!</span></p><p style=\"text-align: center;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">We were expecting you to see at the&nbsp;&nbsp;</span></p><h2 style=\"text-align: center;\"><span style=\"background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>         <p style=\"text-align: center;\">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`,
     themeJson : `{\"buttons\":{\"background\":\"#576268\",\"text\":\"#fff\"},\"background\":{\"color\":\"#fff\",\"image\":\"",\"style\":\"\"}}`,
    backgroundImagePath: ''
  });
  }
}
