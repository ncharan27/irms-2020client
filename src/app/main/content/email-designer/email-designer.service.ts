import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { SectionService } from '../components/shared/section/section.service';
import { EmailDesignerModel } from './email-designer.model';
import { EventDataService } from '../event/event-data.service';
import { QueryService } from '../services/query.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EmailDesignerService extends SectionService<EmailDesignerModel>{
  public invitationId = new BehaviorSubject<string>('');
  private templateData = new Subject<any>();
  private save = new Subject<any>();
  private sendTest = new Subject<any>();
  private refreshForm = new Subject<any>();
  private landingPage = new Subject<any>();
  private previewTemplate = new Subject<any>();

  public template;
  private editingComponentState = 'component'; // Can take value 'component' or 'dialog' to indicate which component is currently active

  constructor(
    protected dataService: EventDataService,
    queryService: QueryService,
    protected router: Router) {
    super(dataService, queryService, router);
  }

  flushTemplate(): void {
    const template = {
      subject: '',
      senderEmail: '',
      preheader: '',
      body: '',
      plainBody: '',
    };
    this.setTemplate(template);
  }
  
  setCurrentComponentState( state ): void {
    this.editingComponentState = state;
  }

  getCurrentComponentState(): any {
    return this.editingComponentState;
  }

  setTemplate(data: any): void {
    this.templateData.next(data);
    this.template = data;
  }

  getTemplate(): Observable<any> {
    return this.templateData.asObservable();
  }

  triggerSave(): void {
    this.save.next();
  }

  saveObs(): Observable<any> {
    return this.save.asObservable();
  }

  // Send test
  triggerSendTest(data: any): void {
    this.sendTest.next(data);
  }

  sendTestObs(): Observable<any> {
    return this.sendTest.asObservable();
  }

  // Refreshing Form
  triggerRefresh(): void {
    this.refreshForm.next();
  }

  getRefreshTrigger(): Observable<any> {
    return this.refreshForm.asObservable();
  }

  // Landing Page Trigger
  triggerLandingPage(): void {
    this.landingPage.next();
  }

  triggerLandingPageObs(): Observable<any> {
    return this.landingPage.asObservable();
  }

  // Preview Slider Trigger
  triggerPreview(): void {
    this.previewTemplate.next();
  }

  triggerPreviewObs(): Observable<any> {
    return this.previewTemplate.asObservable();
  }
}
