import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { EmailDesignerService } from '../email-designer.service';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EmailDesignerDataService } from '../email-designer-data.service';
import { Params, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'irms-email-designer-preview',
  templateUrl: './email-designer-preview.component.html',
  styleUrls: ['./email-designer-preview.component.scss']
})
export class EmailDesignerPreviewComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  saveLoading = false;
  private unsubscribeAll: Subject<any>;
  templateData: any;
  invitationId: any;
  @Input() id: any;

  constructor(public templateService: EmailDesignerService,
    public dataService: EmailDesignerDataService,
    protected route: ActivatedRoute) {
    this.unsubscribeAll = new Subject();
    // this.subscription = this.templateService.getTemplate().subscribe(x => {
    //   this.templateData = x;
    // });
  }

  ngOnInit(): void {
    // this.templateData = this.templateService.template;

    // this.route.params.subscribe((params: Params) => {
      this.invitationId = this.id;
      this.dataService.get(this.invitationId).subscribe(m => {
        if (m) {
          this.templateData = m;
        }
      });
    // });
  }
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

}
