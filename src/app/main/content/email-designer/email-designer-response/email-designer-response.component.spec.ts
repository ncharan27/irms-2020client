import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailDesignerResponseComponent } from './email-designer-response.component';

describe('EmailDesignerResponseComponent', () => {
  let component: EmailDesignerResponseComponent;
  let fixture: ComponentFixture<EmailDesignerResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailDesignerResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailDesignerResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
