import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailDesignerComponent } from './email-designer.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { RouterModule } from '@angular/router';
import { LayoutModule } from 'app/layout/layout.module';
import { EmailDesignerNavbarComponent } from './email-designer-navbar/email-designer-navbar.component';
import { FuseNavigationModule } from '@fuse/components';
import { EmailDesignerDesignComponent } from './email-designer-design/email-designer-design.component';
import { EmailDesignerPreviewComponent } from './email-designer-preview/email-designer-preview.component';
import { EmailDesignerSendTestEmailComponent } from './email-designer-send-test-email/email-designer-send-test-email.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { EmailDesignerService } from './email-designer.service';
import { EmailDesignerResponseComponent } from './email-designer-response/email-designer-response.component';
import { RsvpFormEditorModule } from '../components/shared/rsvp-form-editor/rsvp-form-editor.module';
import { CanExitGuard } from '../services/can-exit.guard';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { GenericColorPickerModule } from '../components/shared/generic-color-picker/generic-color-picker.module';

const routes = [
  {
    path: '',    
    redirectTo: ':tempId',
    pathMatch: 'full'
  },
  {
    path: ':tempId',
    component: EmailDesignerComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: 'design',
        canActivateChild: [AuthGuard],
        component: EmailDesignerDesignComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,
        canDeactivate: [CanExitGuard]
      },
      {
        path: 'preview',
        canActivateChild: [AuthGuard],
        component: EmailDesignerPreviewComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true
      },
      {
        path: 'response',
        canActivateChild: [AuthGuard],
        component: EmailDesignerResponseComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,
        canDeactivate: [CanExitGuard]
      },
    ]
  }];

@NgModule({
  declarations: [
    EmailDesignerComponent,
    EmailDesignerNavbarComponent,
    EmailDesignerDesignComponent,
    // EmailDesignerPreviewComponent,
    // EmailDesignerSendTestEmailComponent,
    EmailDesignerResponseComponent
  ],
  imports: [
    MainModule,
    ContentModule,
    CommonModule,
    // FuseNavigationModule,
    RsvpFormEditorModule,
    EditorModule,    
    GenericColorPickerModule
    // RouterModule.forChild(routes)
  ],
  entryComponents: [EmailDesignerSendTestEmailComponent, FuseConfirmDialogComponent],
  providers: [EmailDesignerService],
  exports: [EmailDesignerComponent, EmailDesignerDesignComponent, EmailDesignerResponseComponent],
})
export class EmailDesignerModule { }
