import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsvpFormEditorThemePaneComponent } from './rsvp-form-editor-theme-pane.component';

describe('RsvpFormEditorThemePaneComponent', () => {
  let component: RsvpFormEditorThemePaneComponent;
  let fixture: ComponentFixture<RsvpFormEditorThemePaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsvpFormEditorThemePaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsvpFormEditorThemePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
