import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { EmailDesignerService } from 'app/main/content/email-designer/email-designer.service';

@Component({
  selector: 'irms-rsvp-form-editor-theme-pane',
  templateUrl: './rsvp-form-editor-theme-pane.component.html',
  styleUrls: ['./rsvp-form-editor-theme-pane.component.scss']
})
export class RsvpFormEditorThemePaneComponent implements OnInit {
  @Input() theme: FormGroup;
  @Input() responseOnlyMode;
  croppieOptions = {
    viewport: { width: 100, height: 100 },
    boundary: { width: 300, height: 300 },
    showZoomer: false,
    enableResize: true,
    enableOrientation: true,
    mouseWheelZoom: 'ctrl'
  };
  constructor(
    public sectionService: EmailDesignerService
  ) { }

  ngOnInit(): void{
    this.theme && this.theme.valueChanges.subscribe(x => {
      let template = this.sectionService.template;
      this.sectionService.setTemplate({
        ...template,
        formTheme: x
      })
    });
  }

  deleteBackground(): void{
    this.theme.get('background.image').reset();
    // this.theme.get('background.image').setValue('');
  }

}
