import { OnInit, Component, Input, Output, EventEmitter } from '@angular/core';
import { fuseAnimations } from '../../../../../../../core/animations';
import { RsvpFormService } from '../../../rsvp-form/rsvp-form.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'irms-rsvp-form-editor-tags',
    templateUrl: './rsvp-form-editor-tags.component.html',
    styleUrls: ['./rsvp-form-editor-tags.component.scss'],
    animations: fuseAnimations
})
export class RsvpFormEditorTagsComponent implements OnInit {
    @Input() showDescription: boolean = true;

    @Output() onTagAdded: EventEmitter<any> = new EventEmitter();

    tags: any;
    selectedTags: any;

    constructor(private previewService: RsvpFormService,
        private route: ActivatedRoute) { }
    
    ngOnInit() {
        this.previewService.getTags(this.route.snapshot.params.tempId).subscribe(tags => {
            this.tags = tags;
            this.selectedTags = tags;
        })
    }

    addTag(event) {
        this.onTagAdded.emit(event)
    }

    searchTags(event) {
        const searchValue = event.toLowerCase();
        this.selectedTags = this.tags.filter(x => x.placeholder.toLowerCase().includes(searchValue));
    }
}