import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { tinyMceKey } from 'app/constants/constants';
import { EmailDesignerDataService } from 'app/main/content/email-designer/email-designer-data.service';
import { SmsDesignerService } from 'app/main/content/sms-designer/sms-designer.service';
import { RsvpFormService } from '../../rsvp-form/rsvp-form.service';

@Component({
  selector: 'irms-rsvp-form-editor-content-pane-stepper',
  templateUrl: './rsvp-form-editor-content-pane-stepper.component.html',
  styleUrls: ['./rsvp-form-editor-content-pane-stepper.component.scss'],
  animations: [fuseAnimations]
})
export class RsvpFormEditorContentPaneStepperComponent implements OnInit, AfterViewInit {
  currentEditor;
  tinyMceKey = tinyMceKey;
  tinyMceConfig = {
    menubar: false,
    statusbar: false,
    branding: false,
    height: window.innerHeight*.405,
    inline: false,
    plugins: [
      'advlist charmap lists link image imagetools directionality',
      'searchreplace visualblocks visualchars media table paste pagebreak code emoticons'
    ],
    toolbar_mode: 'floating',
    toolbar: 'insertComponent | styleselect fontselect fontsizeselect |bold italic underline | superscript subscript | forecolor backcolor | alignleft aligncenter alignright alignjustify | numlist bullist | image link emoticons | table hr pagebreak code',
    image_advtab: true,
    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
    paste_data_images: !0,
    importcss_append: !0,
    images_upload_handler(e, t) {
      t('data:' + e.blob().type + ';base64,' + e.base64());
    },
  };

  tags: any;

  editorRefs = {};

  @Input() welcome: FormGroup;
  @Input() rsvp: FormGroup;
  @Input() accept: FormGroup;
  @Input() reject: FormGroup;
  @Input() responseOnlyMode: boolean;
  @Input() canCopyTemplate: boolean;
  @Input() tempId = 0;
  @Input() stepper = 0;
  @Input() media;

  @Output() public copyMediaTemplate: EventEmitter<any> = new EventEmitter();

  invitationId: string;
  templateId: string;

  constructor(protected previewService: RsvpFormService,
              private sectionService: SmsDesignerService,
              private dataService: EmailDesignerDataService,
              protected route: ActivatedRoute,
  ) {
    this.previewService.refreshFormTrigger();
    const body = sectionService.template.body;
    if (body) {
      this.responseOnlyMode = body.includes('{{rsvp_from_link}}');
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.invitationId = params['tempId'];
      this.loadTags();
    });

    this.setCurrentEditor('welcome');
    this.previewService.refreshFormTrigger();
  }

  edit(event, type): void {
    this.editorRefs[type] = event.editor;
  }
  ngAfterViewInit(): void {
    this.previewService.refreshFormTrigger();
  }


  loadTags(): void {
    this.previewService.tempId = this.tempId;
    this.dataService.getPlaceholders({ pageNo: 1, pageSize: 100 }, this.invitationId, this.tempId, true).subscribe(result => {
      this.previewService.setTags(result.items);
    });
  }

  /// sets current editor flag
  setCurrentEditor(event): void {
    this.currentEditor = event.id;
    this.previewService.setCurrentSection(event.id);
  }

  addTag(event, type): void {
    if (!this.editorRefs[type]) {
      return;
    }

    this.editorRefs[type].insertContent(event);
  }

  copyTemplate(event, media): void {
    const eventToEmit = {
      media: media,
      copy: event,
      templateId: this.tempId
    };
    this.copyMediaTemplate.emit(eventToEmit);
  }
}
