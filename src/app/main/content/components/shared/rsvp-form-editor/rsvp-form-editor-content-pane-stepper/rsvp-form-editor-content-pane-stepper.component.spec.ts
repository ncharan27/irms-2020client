import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsvpFormEditorContentPaneStepperComponent } from './rsvp-form-editor-content-pane-stepper.component';

describe('RsvpFormEditorContentPaneStepperComponent', () => {
  let component: RsvpFormEditorContentPaneStepperComponent;
  let fixture: ComponentFixture<RsvpFormEditorContentPaneStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsvpFormEditorContentPaneStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsvpFormEditorContentPaneStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
