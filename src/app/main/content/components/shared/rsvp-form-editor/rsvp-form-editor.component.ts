import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RsvpFormService } from '../rsvp-form/rsvp-form.service';
import { SmsDesignerDataService } from 'app/main/content/sms-designer/sms-designer-data.service';
import { MatDialogRef } from '@angular/material';
import { SmsLandingDesignComponent } from '../media-template-group/sms-preview/sms-landing-design/sms-landing-design.component';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist';
import { WhatsappDesignerDataService } from 'app/main/content/whatsapp-designer/whatsapp-designer-data.service';
import { RsvpFormEditorDataService } from './rsvp-form-editor-data.service';
import { copyTemplateType } from 'app/constants/constants';

@Component({
  selector: 'irms-rsvp-form-editor',
  templateUrl: './rsvp-form-editor.component.html',
  styleUrls: ['./rsvp-form-editor.component.scss']
})
export class RsvpFormEditorComponent implements OnInit {
  selectedTab = 0;
  @Input() responseOnlyMode;
  @Input() rsvpForm: FormGroup;
  @Input() tempId = 0;
  @Input() isStepper = false;
  @Input() tempInfo;
  stepIndex = 0;
  emptyTemplate: FormGroup;
  sections = ['welcome', 'rsvp', 'accepted', 'rejected'];

  constructor(private fb: FormBuilder, protected preview: RsvpFormService,
              public smsDataService: SmsDesignerDataService,
              public rsvpDataService: RsvpFormEditorDataService,
              public whatappDataService: WhatsappDesignerDataService,
              public dialogRef: MatDialogRef<SmsLandingDesignComponent>) {
  }

  isUpdating = false;
  isUpdatingTheme = false;

  ngOnInit(): void {
    this.emptyTemplate = this.fb.group(
      {
        formContent: this.fb.group({
          welcome: this.fb.group({
            content: ['', [Validators.required]],
            button: ['', [Validators.required]]
          }),
          rsvp: this.fb.group({
            content: ['', [Validators.required]],
            acceptButton: ['', [Validators.required]],
            rejectButton: ['', [Validators.required]]
          }),
          accept: this.fb.group({
            content: ['', [Validators.required]]
          }),
          reject: this.fb.group({
            content: ['', [Validators.required]]
          })
        }),
        formTheme: this.fb.group({
          buttons: this.fb.group({
            background: '',
            text: '',
          }),
          background: this.fb.group({
            color: '',
            image: '',
            style: '',
          }),
        }),
        formSettings: this.fb.group({
          title: ['']
        })
      });

    // if (!this.responseOnlyMode) {
    //   this.preview.getRsvpForm()
    //   .subscribe(x => {
    //     if (!this.isUpdating) {
    //     this.isUpdating = true;
    //     this.rsvpForm = this.emptyTemplate;
    //     this.rsvpForm.patchValue(x);
    //     this.isUpdating = false;
    //   }
    //   });
    // }
    if (!this.rsvpForm && this.responseOnlyMode) {
      this.rsvpForm = this.emptyTemplate;
    } else {
      this.preview.setRsvpForm(this.rsvpForm.value);
      this.preview.rvspFormValue = this.rsvpForm.value;
    }

    this.rsvpForm.valueChanges.subscribe(data => {
      if (!this.isUpdating) {
        this.isUpdating = true;
        this.preview.rvspFormValue = data;
        this.preview.setRsvpForm(data);
        this.isUpdating = false;
      }
    });
  }

  refreshFormPreview(): void {
    this.preview.refreshFormTrigger();
  }

  stepChange(event): void {
    this.stepIndex = event.selectedIndex;
    this.preview.setCurrentSection(this.sections[this.stepIndex]);
  }

  close(): void {
    this.dialogRef.close();
  }

  saveForm(): void {
    /// Save RSVP response form
    const background = this.rsvpForm.get('formTheme') && this.rsvpForm.get('formTheme.background');
    const theme = this.rsvpForm.get('formTheme');

    let img = '';
    img = background && background.get('image').value;
    if (background && !background.get('image').value) {
      img = theme.get('background.image').value;
    }
    if (!img) {
      img = '';
    }

    // theme.background.image = ''
    if (this.tempInfo.id) {
      const model = {
        id: this.tempInfo.id,
        campaignInvitationId: this.tempInfo.campaignInvitationId,
        senderName: this.tempInfo.senderName,
        body: this.tempInfo.body,
        welcomeHTML: this.rsvpForm.controls.formContent['controls'].welcome.controls.content.value,
        proceedButtonText: this.rsvpForm.controls.formContent['controls'].welcome.controls.button.value,
        rsvpHTML: this.rsvpForm.controls.formContent['controls'].rsvp.controls.content.value,
        acceptButtonText: this.rsvpForm.controls.formContent['controls'].rsvp.controls.acceptButton.value,
        rejectButtonText: this.rsvpForm.controls.formContent['controls'].rsvp.controls.rejectButton.value,
        acceptHtml: this.rsvpForm.controls.formContent['controls'].accept.controls.content.value,
        rejectHtml: this.rsvpForm.controls.formContent['controls'].reject.controls.content.value,
        themeJson: JSON.stringify(theme.value),
        // formSettings: JSON.stringify(this.rsvpForm.controls.formSettings.value),
        backgroundImagePath: img
      };

      if (this.rsvpForm.value.formTheme && this.rsvpForm.value.formTheme.background) {
        model['backgroundImage'] = this.rsvpForm.value.formTheme.background.value;
      }

      const fd = new FormData();
      // tslint:disable-next-line:forin
      for (const key in model) {
        fd.append(key, model[key]);
      }
      if (this.tempInfo.type === 'SMS') {
        this.smsDataService.createOrUpdate(fd).subscribe(() => {
          this.rsvpForm.markAsPristine();
          const toasterService = appInjector().get(ToasterService);
          toasterService.pop('success', null, 'Successfully Saved');
        });
      } else if (this.tempInfo.type === 'WhatsApp') {
        this.whatappDataService.saveResponse(fd).subscribe(() => {
          this.rsvpForm.markAsPristine();
          const toasterService = appInjector().get(ToasterService);
          toasterService.pop('success', null, 'Successfully Saved');
        });
      }
    }
  }

  save(): void {
    /// Save RSVP response form
    if (this.stepIndex < 3) {
      this.saveForm();
      this.stepIndex++;
      return;
    }
    this.saveForm();
  }

  copyMediaTemplate(event): void {
    if (event.copy) {
      const reqData = {
        campaignInvitationId: this.tempInfo.campaignInvitationId,
        copyTemplateType: event.media === 'SMS' ? copyTemplateType.WhatsappToSms : copyTemplateType.SmsToWhatsapp
      };
      this.rsvpDataService.copyTemplate(reqData).subscribe(resp => {
        if (resp) {
          if (this.tempInfo.type === 'SMS') {
            this.refreshSMSFormContent();
          } else if (this.tempInfo.type === 'WhatsApp') {
            this.refreshSMSFormContent();
          }
        }
      });
    }
  }

  refreshSMSFormContent(): void {
    this.smsDataService.get(this.tempInfo.campaignInvitationId).subscribe(data => {
      this.rsvpForm.get('formContent.rsvp.content').setValue(data.rsvpHtml);
      this.rsvpForm.get('formContent.rsvp.acceptButton').setValue(data.acceptButtonText);
      this.rsvpForm.get('formContent.rsvp.rejectButton').setValue(data.rejectButtonText);
      this.rsvpForm.get('formContent.accept.content').setValue(data.acceptHtml);
      this.rsvpForm.get('formContent.reject.content').setValue(data.rejectHtml);
      this.rsvpForm.get('formContent.welcome.content').setValue(data.welcomeHtml);
      this.rsvpForm.get('formContent.welcome.button').setValue(data.proceedButtonText);
      this.rsvpForm.get('formTheme').patchValue(JSON.parse(data.themeJson));
      // if (data.backgroundImage && data.backgroundImagePath != null && data.backgroundImagePath != "") {
      this.rsvpForm.get('formTheme.background.image').setValue(data.backgroundImagePath);
      // }
      // console.log(data)
      // console.log(this.rsvpForm)
      // const toasterService = appInjector().get(ToasterService);
      // toasterService.pop('success', null, 'Successfully Copied Template');
    });
  }

  refreshWhatsappFormContent(): void {
    this.whatappDataService.get(this.tempInfo.campaignInvitationId).subscribe(data => {
      this.rsvpForm.get('formContent.rsvp.content').setValue(data.rsvpHtml);
      this.rsvpForm.get('formContent.rsvp.acceptButton').setValue(data.rejectButtonText);
      this.rsvpForm.get('formContent.rsvp.rejectButton').setValue(data.acceptButtonText);
      this.rsvpForm.get('formContent.accept.content').setValue(data.acceptHtml);
      this.rsvpForm.get('formContent.reject.content').setValue(data.rejectHtml);
      this.rsvpForm.get('formContent.welcome.content').setValue(data.welcomeHtml);
      this.rsvpForm.get('formContent.welcome.button').setValue(data.proceedButtonText);
      this.rsvpForm.get('formTheme').patchValue(JSON.parse(data.themeJson));
      // const toasterService = appInjector().get(ToasterService);
      // toasterService.pop('success', null, 'Successfully Copied Template');
    });
  }
}
