import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'irms-rsvp-form-editor-settings-pane',
  templateUrl: './rsvp-form-editor-settings-pane.component.html',
  styleUrls: ['./rsvp-form-editor-settings-pane.component.scss']
})
export class RsvpFormEditorSettingsPaneComponent implements OnInit {
  @Input() settings: FormGroup;
  constructor() { }

  ngOnInit(): void {
  }

}
