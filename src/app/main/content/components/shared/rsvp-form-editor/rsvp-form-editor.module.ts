import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RsvpFormEditorComponent } from './rsvp-form-editor.component';
import { RsvpFormEditorSettingsPaneComponent } from './rsvp-form-editor-settings-pane/rsvp-form-editor-settings-pane.component';
import { RsvpFormEditorThemePaneComponent } from './rsvp-form-editor-theme-pane/rsvp-form-editor-theme-pane.component';
import { RsvpFormEditorContentPaneComponent } from './rsvp-form-editor-content-pane/rsvp-form-editor-content-pane.component';
import { MainModule } from 'app/main/main.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { RsvpFormModule } from '../rsvp-form/rsvp-form.module';
import { GenericColorPickerModule } from '../generic-color-picker/generic-color-picker.module';
import { RsvpFormEditorContentPaneStepperComponent } from './rsvp-form-editor-content-pane-stepper/rsvp-form-editor-content-pane-stepper.component';



@NgModule({
  declarations: [RsvpFormEditorComponent,
    RsvpFormEditorSettingsPaneComponent,
    RsvpFormEditorThemePaneComponent,
    RsvpFormEditorContentPaneComponent,
    RsvpFormEditorContentPaneStepperComponent],
  imports: [
    CommonModule,
    MainModule,
    EditorModule,
    RsvpFormModule,
    GenericColorPickerModule,
  ],
  exports: [
    RsvpFormEditorComponent,
    RsvpFormEditorSettingsPaneComponent,
    RsvpFormEditorThemePaneComponent,
    RsvpFormEditorContentPaneComponent,
    RsvpFormEditorContentPaneStepperComponent
  ]
})
export class RsvpFormEditorModule { }
