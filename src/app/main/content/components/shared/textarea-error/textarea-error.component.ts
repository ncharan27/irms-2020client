import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'irms-textarea-error',
  templateUrl: './textarea-error.component.html',
  styleUrls: ['./textarea-error.component.scss']
})
export class TextareaErrorComponent{
  @Input() public inputControl: FormControl;
  @Input() public placeholder: string;
  @Input() public fxFlex = '100';
  @Input() public required: boolean;
  @Input() public fieldLabel: string;
  @Input() public charactersCount: number;
  @Input() public errorPatternText: string;
  @Input() public errorRangeText: string;
  @Input() public maxRows = 5;
  @Input() public minRows = 2;
  @Output() onBlur: EventEmitter<Event> = new EventEmitter();
  @Output() curPos: EventEmitter<Event> = new EventEmitter();


  handleBlur(): void {
    this.onBlur.emit(event);
  }
  handleCursor(event): void {
    if (event.target.selectionStart || event.target.selectionStart === 0) {
      this.curPos.emit(event.target.selectionStart);
   }
  }

}
