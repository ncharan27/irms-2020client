import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegexpPattern, BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MatDialogRef } from '@angular/material';
import { ToasterService } from 'angular5-toaster/dist';
import { appInjector } from 'app/main/app-injector';


@Component({
  selector: 'irms-credential-confirm',
  templateUrl: './credential-confirm.component.html',
  styleUrls: ['./credential-confirm.component.scss']
})
export class CredentialConfirmComponent implements OnInit {
  public passConfirmForm = this.fb.group({
    password: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.password))]],
  });
  private url = `${BASE_URL}api/confirm`;
  constructor(
    public fb: FormBuilder,
    public http: HttpClient,
    public matDialogRef: MatDialogRef<CredentialConfirmComponent>) { }

  ngOnInit() {
  }
  
  submit(){
    this.confirmPassword().subscribe(result =>{
      const toasterService = appInjector().get(ToasterService);
      if(result){
        this.matDialogRef.close();
      } else {
        toasterService.pop('error', 'Invalid Password', 'You have entered an incorrect.');
      }
    });
  }

  confirmPassword(): Observable <any> {
    return this.http.post(`${this.url}/confirmPassword`, this.passConfirmForm.controls.password.value);
  }
}
