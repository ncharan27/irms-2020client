import { Component, OnInit, Input, HostBinding, OnDestroy, HostListener, ElementRef, AfterViewInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormService } from './form.service';
import { Subscription, Subject } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { SwiperComponent, SwiperConfigInterface, SwiperDirective } from 'ngx-swiper-wrapper';
import { FormBuilder, FormControl, ValidationErrors } from '@angular/forms';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'irms-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  animations: fuseAnimations
})
export class FormComponent implements OnInit, OnDestroy, AfterViewInit {
  section = 'welcome';
  @Input() mockup;
  @Input() allowReview;
  @Input() formObj;
  @Input() themeObj;
  @Input() settings;
  @Input() saveLoading;
  @Output() formSubmit: EventEmitter<any> = new EventEmitter();

  // Host binding for background styling of the form host component
  @HostBinding('style.background') background: any;
  @HostBinding('style.background-color') backgroundColor: any;
  @HostBinding('class.cover') isBackgroundCover = true;
  @HostBinding('class.repeat') isBackgroundRepeat = false;
  @HostBinding('class.no-repeat') isBackgroundNoRepeat = false;
  @HostBinding('style.height') hostHeight: any;
  @HostBinding('style.width') hostWidth: any;
  @HostBinding('style.position') hostPosition = 'relative';
  progressSubject: Subject<any> = new Subject();
  qIndex = 0; // Index of slide/ question
  isArabic: any;
  formGroup = this.fb.group({});
  subscriptions: Subscription[] = [];

  public innerHeight: any;
  screenHeight: number;
  screenWidth: number;
  @ViewChild(SwiperDirective, { static: false }) swipperRef?: SwiperDirective;
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'vertical',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: true,
    scrollbar: true,
    navigation: true,
    pagination: true,
    height: this.el.nativeElement.offsetHeight,
    fadeEffect: {
      crossFade: true
    }
  };
  reviewMode: boolean;
  reviewQuestions = [];
  reviewQuestionsCurrentIndex: any;
  reachedEnd: boolean;
  orientation: string;

  constructor(protected formService: FormService, private fb: FormBuilder, private el: ElementRef, private titleService: Title) {
    this.onResize();
  }

  ngOnInit(): void {
    this.subscriptions.push(this.formService.getForm().subscribe(data => {
      this.formObj = data.formContent;
      this.themeObj = data.formTheme;
      this.settings = data.formSettings;
      this.setBackgroundStyle();
      this.createGroup();
    }));
    this.subscriptions.push(this.formService.getRefreshFormTrigger().subscribe(data => this.refreshForm()));
    this.subscriptions.push(this.formService.getCurrentSection().subscribe(data => this.setCurrentSection(data)));
    this.subscriptions.push(this.formService.getCurrentQuestion().subscribe(data => this.setCurrentQuestion(data)));
    // set height to 100vh if not mockup
    if (!this.mockup) {
      this.hostHeight = this.screenHeight + 'px';
      this.hostPosition = 'fixed';
      this.config.height = this.screenHeight;   
    } else {
      this.config.height = this.el.nativeElement.offsetHeight;  
      this.hostWidth = this.el.nativeElement.offsetWidth;  
    } 
    this.innerHeight = this.config.height + 'px';
    if (!this.mockup) { 
      this.setBackgroundStyle();
      this.createGroup();  
    }
    // console.log(`INIT \n hostH: ${this.hostHeight} \n hostW: ${this.hostWidth}  \n natvH: ${this.el.nativeElement.offsetHeight} \n natvW: ${this.el.nativeElement.offsetWidth} `);
  }
  ngAfterViewInit(): void { 
    this.formService.getForm().subscribe(data => {
      this.formObj = data.formContent;
      this.themeObj = data.formTheme;
      this.settings = data.formSettings;
      this.setBackgroundStyle();
      this.createGroup();
    }); 
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?): void {
    if (this.mockup) {
      this.config.height = this.el.nativeElement.offsetHeight;
    } else {
      this.config.height = window.innerHeight;
    }
    this.hostHeight = window.innerHeight + 'px';
    this.hostWidth = window.innerWidth + 'px';
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    // console.log(`Resize \n hostH: ${window.innerHeight} \n hostW: ${window.innerWidth}  \n natvH: ${this.el.nativeElement.offsetHeight} \n natvW: ${this.el.nativeElement.offsetWidth} `);
  }

  @HostListener('window:orientationchange', ['$event'])
  onOrientationChange(event): void {
    window.setTimeout(() => {
      if (window.innerHeight > window.innerWidth) {
        this.orientation = 'portrait';
      } else {
        this.orientation = 'landscape';
      }
      if (this.mockup) {
        this.config.height = this.el.nativeElement.offsetHeight;
      } else {
        this.config.height = window.innerHeight;
      }
      this.hostHeight = window.innerHeight + 'px';
      this.hostWidth = window.innerWidth + 'px';
      this.screenHeight = window.innerHeight;
      this.screenWidth = window.innerWidth;
      // console.log(`Orientation \n ${this.orientation} \n hostH: ${window.innerHeight} \n hostW: ${window.innerWidth}  \n natvH: ${this.el.nativeElement.offsetHeight} \n natvW: ${this.el.nativeElement.offsetWidth} `);
      if (this.swipperRef) {
        this.swipperRef.update();
      }
    }, 500);
  }

  setBackgroundStyle(): void {
    if (!this.themeObj) { return; }
    this.backgroundColor = this.themeObj.background.color;
    if (this.themeObj.background.image) {
      this.background = `url(${this.themeObj.background.image})`;
    } else {
      this.background = null;
    }
    this.isBackgroundCover = this.isBackgroundNoRepeat = this.isBackgroundRepeat = false;
    if (this.themeObj.background.style === 'repeat') {
      this.isBackgroundRepeat = true;
    } else if (this.themeObj.background.style === 'no-repeat') {
      this.isBackgroundNoRepeat = true;
    } else { this.isBackgroundCover = true; }
  }

  gotoForm(): void {
    this.section = 'form';
  }

  createGroup(): void {   
    if (this.settings.title != null || this.settings.title !== '') {
      this.titleService.setTitle(this.settings.title);
    }
    const group = this.fb.group({});
    this.formObj.questions.forEach(control => group.addControl(control.id, 
      new FormControl((control.hasOwnProperty('response') && 
      (control.response !== undefined || control.response !== null)) ? control.response : '')));
    this.formGroup = group;
    if (this.swipperRef) {
      this.swipperRef.update();
    }
  }

  setCurrentSection(section): void {
    if (this.swipperRef) {
      this.swipperRef.update();
    }
    if (section === 'submit') {
      if (section !== 'form') {
        this.gotoForm();
      }
      this.qIndex = this.formObj.questions.length;
    }
    else { 
      this.section = section;
    }
  }
  
  setCurrentQuestion(questionIndex: any): void {
    if (this.swipperRef) {
      this.swipperRef.update();
    }
    if (this.section !== 'form') {
      this.gotoForm();
    }
    this.qIndex = questionIndex;
    this.nextTrig(null);

  }

  refreshForm(): void {
    this.formGroup.reset();
    this.section = 'welcome';
    if (this.settings.defaultLanguage === 'ar') {
      this.isArabic = true;
    } else { this.isArabic = false; }
    this.qIndex = 0;
    this.notifyProgress();
    if (this.swipperRef) {
      this.swipperRef.update();
    }
  }

  submitForm(): void {
    if (this.mockup) {
      this.showThanks();
    } else if (this.allowReview) {
      this.showReview();
    }else {
      this.formSubmit.emit(this.formGroup.value);
    }
  }

  submitReviewedForm(): void {
    this.formSubmit.emit(this.formGroup.value);
  }
  
  showReview(): void {
    Object.keys(this.formGroup.controls).forEach((key, qIndex) => {
      const controlErrors: ValidationErrors = this.formGroup.get(key).errors;
      if (controlErrors != null) {
            this.formGroup.get(key).markAsDirty();
          }
        });
    this.section = 'review';
  }
  showThanks(): void {
    this.section = 'thanks';
  }

  /// Go to next question
  nextTrig(event): void {
    let node: any;
    if (this.qIndex >= 0 && this.qIndex < this.formObj.questions.length) {
      node = document.getElementById(this.formObj.questions[this.qIndex].id);
      const input = node.querySelector('input, textarea');
      if (input !== null && !this.mockup) {
        input.focus();
      }
    } else {
      return;
    }
  }

  gotoNext(event): void {
    if (event && event.destination >= 0 && this.reviewMode) {
      this.qIndex = this.reviewQuestions[this.reviewQuestionsCurrentIndex];
      return;
    } else if (this.reviewMode) {
      if (this.reviewQuestionsCurrentIndex !== this.reviewQuestions.length - 1 &&
        this.reviewQuestionsCurrentIndex < this.formObj.questions.length) {
        this.reviewQuestionsCurrentIndex += 1;
        this.qIndex = this.reviewQuestions[this.reviewQuestionsCurrentIndex];
      } else {
        this.qIndex = this.formObj.questions.length;
        this.reachedEnd = true;
        this.reviewMode = false;
        return;
      }
    } else {
      if (this.qIndex < this.formObj.questions.length) {
        this.qIndex += 1;
      } else {
        this.qIndex = this.formObj.questions.length;
        this.reachedEnd = true;
      }
    }
  }

  gotoPrevious(event): void {
    let node: any;
    if (this.reviewMode) {
      if (this.reviewQuestionsCurrentIndex > 0) {
        this.reviewQuestionsCurrentIndex -= 1;
        this.qIndex = this.reviewQuestions[this.reviewQuestionsCurrentIndex];
        node = document.getElementById(this.formObj.questions[this.qIndex].identifier);
      } else {
        return;
      }
    } else if (!this.reviewMode) {
      if (this.qIndex > 0) {
        this.qIndex -= 1;
        node = document.getElementById(this.formObj.questions[this.qIndex].identifier);
      } else {
        return;
      }
    }
  }
  getFormValidationErrors(): void {
    this.reviewMode = true;
    const errorFields = [];
    Object.keys(this.formGroup.controls).forEach((key, qIndex) => {
    const controlErrors: ValidationErrors = this.formGroup.get(key).errors;
    if (controlErrors != null) {
          errorFields.push(qIndex);
          this.formGroup.get(key).markAsDirty();
        }
      });
    this.reviewQuestions = errorFields;
    this.reviewQuestionsCurrentIndex = 0;
    this.gotoNext({destination: this.reviewQuestions[0]});
  }
  notifyProgress(): void {
    let questionCount = 0;
    // tslint:disable-next-line:forin
    for (const field in this.formGroup.controls) {
      const formField = this.formGroup.controls[field];
      if (formField.valid && formField.dirty) {
        questionCount++;
      }
    }
    const progress = {
      total: this.formObj.questions.length,
      done: questionCount
    };
    this.progressSubject.next(progress);
  }

}
