import { Injectable } from '@angular/core';
import { SectionDataService } from '../section/section-data.service';
import { BaseModel } from '../section/base.model';
import { BASE_URL } from 'app/constants/constants';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { Toast } from 'app/core/decorators/toast.decorator';

@Injectable({
  providedIn: 'root'
})
export class DataReviewFormBuilderDataService implements SectionDataService<BaseModel> {
  

  private url = `${BASE_URL}api/rfiForm`;

  constructor(private http: HttpClient) { }

  getImportantFields(filterParam): Observable<any>{
    return of({
      id: 'e66480ea-2e2a-4d7e-8bf9-5039b87d7742',
      importantFields: ['Email', 'Gender'],
      campaignInvitationId: '00000000-0000-0000-0000-000000000000',
      welcomeHtml: '{"content":"<p style=\\"text-align: left;\\"><span style=\\"font-family: arial, helvetica, sans-serif;\\">You are invited to</span></p>\\n      <h2 style=\\"text-align: left;\\"><span style=\\"background-color: #236fa1; color: #ecf0f1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: left;\\"><span style=\\"color: #000000; font-family: tahoma, arial, helvetica, sans-serif;\\">on 23rd April 2020, 6:00 PM</span></p>\\n      <h5 style=\\"text-align: left;\\"><span style=\\"color: #34495e;\\"><em><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">\\n      Please let us know if you will be joining us</span></em></span></h5>","contentArabic":"<p style=\\"text-align: left;\\"><span style=\\"font-family: arial, helvetica, sans-serif;\\">You are invited to</span></p>\\n      <h2 style=\\"text-align: left;\\"><span style=\\"background-color: #236fa1; color: #ecf0f1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: left;\\"><span style=\\"color: #000000; font-family: tahoma, arial, helvetica, sans-serif;\\">on 23rd April 2020, 6:00 PM</span></p>\\n      <h5 style=\\"text-align: left;\\"><span style=\\"color: #34495e;\\"><em>\\n      <span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">Please let us know if you will be joining us</span></em></span></h5>","button":"Proceed","buttonArabic":"تقدم"}',
      submitHtml: '{"submitMessage":"Everything seems good,\\nYou can submit now!","submitMessageArabic":"جميع البيانات صحيحة،\\n قم بتقديم الطلب الآن!","submitButton":"Submit","submitButtonArabic":"تقدم"}',
      thanksHtml: '{"content":"<p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">Thank you for your response!</span></p>\\n      <p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">We will see you at the&nbsp;</span></p>\\n      <h2 style=\\"text-align: center;\\"><span style=\\"background-color: #236fa1; color: #ecf0f1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: center;\\">&nbsp;</p>","contentArabic":"<p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">Thank you for your response!</span></p>\\n      <p style=\\"text-align: center;\\"><span style=\\"font-family: tahoma, arial, helvetica, sans-serif;\\">We will see you at the&nbsp;</span></p>\\n      <h2 style=\\"text-align: center;\\"><span style=\\"background-color: #236fa1; color: #ecf0f1; font-family: \'arial black\', sans-serif;\\">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>\\n      <p style=\\"text-align: center;\\">&nbsp;</p>","button":null,"buttonArabic":null}',
      formTheme: '{"buttons":{"background":"#576268","text":"#fff"},"questions":{"color":"#576268"},"answers":{"color":"#576268"},"background":{"color":"#fff","style":""}}',
      formSettings: '{"title":"Personal Information","defaultLanguage":"en","languageOption":false}',
      themeBackgroundImagePath: null,
      rfiFormQuestions: [
        {
          id: '59cea93b-30fa-47de-b7d2-3aec73bc35e5',
          question: '{"type":"email","id":"59cea93b-30fa-47de-b7d2-3aec73bc35e5","headlineArabic":"","headline":"","isDescription":false,"description":null,"descriptionArabic":null,"required":false,"multiline":false,"minLength":3,"maxLength":20,"pattern":"","mapping":true,"mappingField":"Email","jumps":null}',
          questionMappingType: 0,
          mappedField: 'Email',
          sortOrder: 1
        },
        {
          id: '8d653930-c7fb-4668-8def-efdeb3f556ca',
          question: '{"type":"singleLineText","id":"8d653930-c7fb-4668-8def-efdeb3f556ca","headlineArabic":"","headline":"","isDescription":false,"description":null,"descriptionArabic":null,"required":false,"multiline":false,"minLength":3,"maxLength":20,"pattern":"","mapping":true,"mappingField":"Gender","jumps":null}',
          questionMappingType: 0,
          mappedField: 'Gender',
          sortOrder: 2
        }
      ]
    });
  }

  getList(filter): any {
    return of();
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}`);
  }

  @Toast('Successfully updated')
  upsert(model) {
    return this.http.post(`${this.url}`, model);
  }

  create(model: any): Observable<string> {
    throw new Error('Method not implemented.');
  }

  update(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
  
  delete(model: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
}
