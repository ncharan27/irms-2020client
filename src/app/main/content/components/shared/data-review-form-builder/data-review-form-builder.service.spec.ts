import { TestBed } from '@angular/core/testing';

import { DataReviewFormBuilderService } from './data-review-form-builder.service';

describe('DataReviewFormBuilderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataReviewFormBuilderService = TestBed.get(DataReviewFormBuilderService);
    expect(service).toBeTruthy();
  });
});
