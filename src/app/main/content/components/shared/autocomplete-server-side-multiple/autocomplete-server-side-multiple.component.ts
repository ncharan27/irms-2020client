import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, ViewChild, Input, Output, EventEmitter, OnInit, AfterViewChecked} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';
import {map, startWith, debounceTime} from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'irms-autocomplete-server-side-multiple',
  templateUrl: './autocomplete-server-side-multiple.component.html',
  styleUrls: ['./autocomplete-server-side-multiple.component.scss']
})
export class AutocompleteServerSideMultipleComponent implements OnInit, AfterViewChecked {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  items: any[] = [];
  allItems: any[] = [];
  separatorKeysCodes = [ENTER, COMMA];

  @Input() public data: Observable<any[]>;
  @Input() public selectedValues: any[];
  @Input() public inputControl: FormControl;
  @Input() public placeholder: string;
  @Input() public empty: boolean;
  @Input() public resetEvent: EventEmitter<any>;
  @Output() public onChangeObject: EventEmitter<any[]> = new EventEmitter();
  @Output() public onChangeString: EventEmitter<string> = new EventEmitter();

  @ViewChild('itemInput', {static: false}) itemInput: ElementRef;

  constructor() {
  }

  ngOnInit() {
    this.inputControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(300)
      )
      .subscribe((value: any) => {
        if (typeof value === 'string') {
          this.onChangeString.emit(value);
        }
      }
    );

    if (this.resetEvent)
    {
      this.resetEvent.subscribe(data => {
        this.items = [];
        this.onChangeObject.emit(this.items);
      });
    }

    this.items = this.selectedValues;
  }

  ngAfterViewChecked() {
    // this.data.subscribe(data => {
    //   this.filteredItems = this.inputControl.valueChanges.pipe(
    //     startWith(null),
    //     map((item: any | null) => item ? this._filter(item, data) : data.slice()));
    // });

    if (this.data)
    {
      this.data.subscribe(data => {
        this.allItems = data;
      });
    }
  }

  add(event: MatChipInputEvent): void {
    if (this.items.indexOf(event.value) === -1 &&
        this.allItems.indexOf(event.value) !== -1)
    {
      const input = event.input;
      const value = event.value;

      // Add our item
      if (value) {
        this.items.push(value);
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.inputControl.setValue(null);
    }

    this.itemInput.nativeElement.blur();
  }

  remove(item: any): void {
    const index = this.items.indexOf(item);

    if (index >= 0) {
      this.items.splice(index, 1);
      this.onChangeObject.emit(this.items);
    }

    this.itemInput.nativeElement.blur();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.items.indexOf(event.option.value) === -1 &&
        this.allItems.indexOf(event.option.value) !== -1)
    {
      this.items.push(event.option.value);
      this.itemInput.nativeElement.value = '';
      this.inputControl.setValue(null);

      this.onChangeObject.emit(this.items);
    }

    this.itemInput.nativeElement.blur();
  }

  private _filter(value: any, data: any[]): any[] {
    return data.filter(item => item.id === value.id);
  }

  public displayName(val): string {
    if (val) {
      if (typeof val === 'string') {
        return val;
      }
      if (val.hasOwnProperty('value')) {
        return val.value;
      }
      if (val.hasOwnProperty('name')) {
        return val.name;
      }
    }
    return undefined;
  }
}
