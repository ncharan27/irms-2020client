import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'irms-form-builder-settings-pane',
  templateUrl: './form-builder-settings-pane.component.html',
  styleUrls: ['./form-builder-settings-pane.component.scss']
})
export class FormBuilderSettingsPaneComponent implements OnInit {
  @Input() settings: FormGroup;
  constructor() { }

  ngOnInit() {
  }

}
