import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBuilderContentPaneComponent } from './form-builder-content-pane.component';

describe('FormBuilderContentPaneComponent', () => {
  let component: FormBuilderContentPaneComponent;
  let fixture: ComponentFixture<FormBuilderContentPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormBuilderContentPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBuilderContentPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
