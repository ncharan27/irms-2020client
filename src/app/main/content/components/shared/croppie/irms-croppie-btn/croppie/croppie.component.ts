import { Component, Input, OnInit, ViewChild, OnChanges, EventEmitter, Output, ElementRef } from '@angular/core';

import { CroppieOptions } from 'croppie/croppie';
import { MatDialogRef } from '@angular/material';
import { IrmsNgxCroppieComponent } from '../irms-ngx-croppie/irms-ngx-croppie.component';
import { Toast } from 'angular5-toaster/dist/src/toast';
import { ErrorToastComponent } from 'app/auth/services/error-toast.component';
import { BodyOutputType, ToasterService } from 'angular5-toaster/dist/angular5-toaster';

/**
 * Component containing Coppie instance and material design rotation buttons and slider.
 */
@Component({
  selector: 'angular-croppie',
  templateUrl: './croppie.component.html',
  styleUrls: ['./croppie.component.scss']
})

export class CroppieComponent implements OnInit {

  @Input()
  public options: CroppieOptions;
  @Input()
  public outputSize: any ;
  @ViewChild('ngxCroppie', { static: true }) ngxCroppie: IrmsNgxCroppieComponent;
  @Output() public handleUploadFile: EventEmitter<Object> = new EventEmitter();

  widthPx = '400';
  heightPx = '400';
  imageUrl = '';
  currentImage: string;
  croppieImage: any;
  public isVisibleUploadMessage = true;
  public file: any;
  public outputFormatOptions;

  public get imageToDisplay() {
    if (this.currentImage) { return this.currentImage; }
    if (this.imageUrl) { return this.imageUrl; }
    return `http://placehold.it/${this.widthPx}x${this.heightPx}`;
  }

  constructor(public dialogRef: MatDialogRef<CroppieComponent>,
              public toasterService: ToasterService) { }


  ngOnInit(): void{
    this.outputFormatOptions = { type: 'Blob', size: this.outputSize };
    this.currentImage = this.imageUrl;
    this.croppieImage = this.imageUrl;
  }

  newImageResultFromCroppie(img: any) {

    const fr = new FileReader();
    fr.readAsDataURL(img);
    fr.onloadend = () => {
      this.croppieImage = fr.result;
    };
  }

  saveImageFromCroppie() {
    this.currentImage = this.croppieImage;
    this.uploadImage();
  }

  cancelCroppieEdit() {
    this.croppieImage = this.currentImage;
    this.isVisibleUploadMessage = true;
    this.dialogRef.close(true);
  }

  imageUploadEvent(evt: any) {
    if (!evt.target) { return; }
    if (!evt.target.files) {
      return;
    } else {
      this.isVisibleUploadMessage = false;
    }
    if (evt.target.files.length !== 1) { return; }
    this.file = evt.target.files[0];
    if (this.file.type !== 'image/jpeg' && this.file.type !== 'image/png' && this.file.type !== 'image/gif' && this.file.type !== 'image/jpg') {
      const toast: Toast =
      {
        type: 'error',
        title: 'Only jpg, jpeg, png types are allowed',
        body: ErrorToastComponent,
        bodyOutputType: BodyOutputType.Component,
        data: ''
      };
      this.toasterService.pop(toast);
      evt.target.value = '';
      return;
    }

    const fr = new FileReader();
    fr.readAsDataURL(this.file);
    fr.onloadend = () => {
      this.croppieImage = fr.result;
      this.ngxCroppie.updateImg(this.croppieImage);
    };
  }

  uploadImage() {
    this.handleUploadFile.emit(this.croppieImage);
  }

}
