import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

import { CroppieComponent } from './croppie/croppie.component';
import { CroppieOptions } from 'croppie';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-croppie-btn',
  templateUrl: './irms-croppie-btn.component.html',
  styleUrls: ['./irms-croppie-btn.component.scss']
})
export class IrmsCroppieBtnComponent implements OnInit {
  @Input() public title = 'Upload';
  @Input() public required = false;
  @Output() public handleUploadFile: EventEmitter<Object> = new EventEmitter();
  @Input() public inputControl: FormControl;
  @Input()
  public outputSize: any = 'original';

  @Input() public options: CroppieOptions = {
    viewport: {
      width: 385,
      height: 250,
    },
    boundary: {
      width: 400,
      height: 400
    },
    enableExif: true
  };

  public file: any;
  public validStatus: any;
  constructor(public dialog: MatDialog) { }
  ngOnInit() { }

  uploadCroppImg() {
    const confirmDialogRef: MatDialogRef<CroppieComponent> = this.dialog.open(CroppieComponent, {
      disableClose: false
    });
    confirmDialogRef.componentInstance.options = this.options;
    confirmDialogRef.componentInstance.outputSize = this.outputSize;
    const sub = confirmDialogRef.componentInstance.handleUploadFile.subscribe((file) => {
      this.file = file;
      this.inputControl.setValue(file);
      if (confirmDialogRef) {
        confirmDialogRef.close();
      }
    });
    confirmDialogRef.afterClosed().subscribe(() => {
      sub.unsubscribe();
    });
  }

  checkValidate(status) {
    this.validStatus = status;
  }
}
