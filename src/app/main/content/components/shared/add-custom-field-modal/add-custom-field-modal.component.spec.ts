import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCustomFieldModalComponent } from './add-custom-field-modal.component';

describe('AddCustomFieldModalComponent', () => {
  let component: AddCustomFieldModalComponent;
  let fixture: ComponentFixture<AddCustomFieldModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCustomFieldModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCustomFieldModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
