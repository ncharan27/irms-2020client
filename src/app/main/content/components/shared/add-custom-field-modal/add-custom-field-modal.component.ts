import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { NavbarHorizontalStyle1Module } from 'app/layout/components/navbar/horizontal/style-1/style-1.module';
import { GlobalContactDataService } from 'app/main/content/global-contact/global-contact-data.service';

@Component({
  selector: 'irms-add-custom-field-modal',
  templateUrl: './add-custom-field-modal.component.html',
  styleUrls: ['./add-custom-field-modal.component.scss']
})
export class AddCustomFieldModalComponent implements OnInit {
  form: FormGroup;
  public loader = false;
  disabled = false;
  required = false;
  type: any;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddCustomFieldModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private customFieldDataService: GlobalContactDataService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      type: ['text', [Validators.required]],
      text: this.formBuilder.group({
        min: [''],
        max: ['']
      }),
      date: this.formBuilder.group({
        min: [''],
        max: ['']
      }),
      number: this.formBuilder.group({
        min: [''],
        max: ['']
      })
    });

    this.form.get('type').valueChanges.subscribe(x => {
      this.invalidate(x);
    });

    this.form.get(['text', 'min']).valueChanges.subscribe(value => {
      const maxValue = this.form.get(['text', 'max']).value;

      if (maxValue && maxValue < value) {
        this.form.get(['text', 'min']).setErrors({ rangeError: true });
      }
      else {
        this.form.get(['text', 'max']).setErrors(null);
        this.form.get(['text', 'min']).setErrors(null);
      }
    });

    this.form.get(['text', 'max']).valueChanges.subscribe(value => {
      const minValue = this.form.get(['text', 'min']).value;
      if (minValue && minValue > value) {
        this.form.get(['text', 'max']).setErrors({ rangeError: true });
      }
      else {
        this.form.get(['text', 'min']).setErrors(null);
        this.form.get(['text', 'max']).setErrors(null);
      }
    });
    this.form.get(['number', 'min']).valueChanges.subscribe(value => {
      const maxValue = this.form.get(['number', 'max']).value;
      if (maxValue && maxValue < value) {
        this.form.get(['number', 'min']).setErrors({ rangeError: true });
      }
      else {
        this.form.get(['number', 'max']).setErrors(null);
        this.form.get(['number', 'min']).setErrors(null);
      }
    });
    this.form.get(['number', 'max']).valueChanges.subscribe(value => {
      const minValue = this.form.get(['number', 'min']).value;
      if (minValue && minValue > value) {
        this.form.get(['number', 'max']).setErrors({ rangeError: true });
      }
      else {
        this.form.get(['number', 'min']).setErrors(null);
        this.form.get(['number', 'min']).setErrors(null);
      }
    });

    this.form.get(['date', 'min']).valueChanges.subscribe(value => {
      const minDate = new Date(value);
      const maxDate = new Date(this.form.get(['date', 'max']).value);
      if (maxDate && (maxDate.getTime() - minDate.getTime() < 0)) {
        this.form.get(['date', 'min']).setErrors({ rangeError: true });
      }
      else {
        this.form.get(['date', 'max']).setErrors(null);
        this.form.get(['date', 'min']).setErrors(null);
      }
    });

    this.form.get(['date', 'max']).valueChanges.subscribe(value => {
      const minDate = new Date(this.form.get(['date', 'min']).value);
      const maxDate = new Date(value);
      if (minDate && (maxDate.getTime() - minDate.getTime() < 0)) {
        this.form.get(['date', 'max']).setErrors({ rangeError: true });
      }
      else {
        this.form.get(['date', 'min']).setErrors(null);
        this.form.get(['date', 'min']).setErrors(null);
      }
    });

    if (this.data.type) {
      this.type = this.data.type;
      if (this.type !== 'singleLineText') {
        this.form.controls.type.setValue(this.type);
      }
    }
  }

  invalidate(type: string): void {
    switch (type) {
      case 'text':

        this.form.get(['number', 'min']).setValue('');
        this.form.get(['number', 'max']).setValue('');
        this.form.get(['date', 'min']).setValue('');
        this.form.get(['date', 'max']).setValue('');
        break;
      case 'number':

        this.form.get(['text', 'min']).setValue('');
        this.form.get(['text', 'max']).setValue('');
        this.form.get(['date', 'min']).setValue('');
        this.form.get(['date', 'max']).setValue('');
        break;

      case 'date':
        this.form.get(['number', 'min']).setValue('');
        this.form.get(['number', 'max']).setValue('');
        this.form.get(['text', 'min']).setValue('');
        this.form.get(['text', 'max']).setValue('');
        break;

      default:
        break;
    }
  }

  close(): void {
    this.loader = true;
    this.dialogRef.close();
  }

  confirm(): void {
    this.loader = true;
    const value = this.form.value;
    let min = null;
    let max = null;
    if (value.type === 'text') {
      min = value.text.min;
      max = value.text.max;
    }
    if (value.type === 'number') {
      min = value.number.min;
      max = value.number.max;
    }

    if (value.type === 'date') {
      if (value.date.min) {
        const minDate = new Date(value.date.min);
        min = minDate.getTime();
      }
      if (value.date.max) {
        const maxDate = new Date(value.date.max);
        max = maxDate.getTime();
      }
    }

    const newField: any = {
      name: value.name,
      customFieldType: value.type,
      minValue: min,
      maxValue: max
    };
    if (this.data != null) {
      this.dialogRef.close({
        name: value.name,
        customFieldType: value.type,
        minValue: min,
        maxValue: max
      });
    } else {
      this.customFieldDataService.createCustomField(newField).subscribe(result => {
        if (result) {
          this.loader = false;
          this.dialogRef.close({name: newField.name});
        }
      }, error => {
        this.loader = false;
      });

    }


  }
}
