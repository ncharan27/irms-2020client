import {BaseModel} from '../base.model';
import {Router} from '@angular/router';
import {SectionDataService} from '../section-data.service';
import {SectionService} from '../section.service';
import {Section} from '../../../../../../constants/constants';
import {FormArray, Validators, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {OnDestroy} from '@angular/core';


export class SectionCreateComponent<T extends BaseModel> implements OnDestroy {
  constructor(private sectionName: Section,
              public sectionService: SectionService<T>,
              protected dataService: SectionDataService<T>,
              protected router: Router,
              ) {
  }

  public minStartDate = new Date();
  public minEndDate = new Date();
  public createForm: FormGroup;
  protected componentDestroyed: Subject<any> = new Subject();

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  setValidators(form, field, validators) {
    this[form].get(field).setValidators(validators);
    this[form].get(field).updateValueAndValidity();
  }

  setRequiredValidator(form, fieldsArr) {
    for (let i = 0; i < fieldsArr.length; i++) {
      this.setValidators(form, fieldsArr[i], [Validators.required]);
    }
  }

  setRequiredValidators(form , fieldsArr, validatorsArr) {
    for (let i = 0; i < fieldsArr.length; i++) {
      this[form].get(fieldsArr[i]).setValidators(validatorsArr);
      this[form].get(fieldsArr[i]).updateValueAndValidity();
    }
  }

  clearValidators(form, fieldsArr) {
    for (let i = 0; i < fieldsArr.length; i++) {
      this[form].get(fieldsArr[i]).clearValidators();
      this[form].get(fieldsArr[i]).updateValueAndValidity();
    }
  }

  changeFieldRequiredValidator(form, setValidators, clearValidators) {
    this.setRequiredValidator(form, setValidators);
    this.clearValidators(form, clearValidators);
  }
  public setFormArrayValidators(form, fieldName, validators) {
    const formArray = <FormArray>this[form]['controls']['items'];
    for (let i = 0; i < formArray.controls.length; i++) {
      formArray.at(i).get(fieldName).setValidators(validators);
      formArray.at(i).get(fieldName).updateValueAndValidity();
    }
  }


  toggleValidators(form, fieldsArr, condition, validators) {
    if (condition) {
      for (let i = 0; i < fieldsArr.length; i++) {
        this.setValidators(form, fieldsArr[i], validators);
      }
    }
    else {
      this.clearValidators(form, fieldsArr);
    }
  }

  touchedFormFields(form) {
    for (const inner in form.controls) {
      form.get(inner).markAsTouched({onlySelf: true});
      form.get(inner).updateValueAndValidity();
    }
  }

  public create() {
    if (this.createForm.dirty && this.createForm.valid) {
      const model = this.getModel();
      this.dataService.create(model).subscribe((id: string) => {
        this.router.navigate([`/${this.sectionName}/${id}`]);
        this.sectionService.refreshList();
      });
    }
  }

  public resetFormControlValue(form, controlName) {
    if (this[form].controls.hasOwnProperty(controlName)) {
      this[form].controls[controlName].setValue('');
      this[form].controls[controlName].setErrors(null);
    }
  }

  protected getModel() {
    return this.createForm.value;
  }

  public close() {
    this.router.navigate([`/${this.sectionName}`]);
  }

}
