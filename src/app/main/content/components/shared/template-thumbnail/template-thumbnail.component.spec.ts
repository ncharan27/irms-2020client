import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateThumbnailComponent } from './template-thumbnail.component';

describe('TemplateThumbnailComponent', () => {
  let component: TemplateThumbnailComponent;
  let fixture: ComponentFixture<TemplateThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateThumbnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
