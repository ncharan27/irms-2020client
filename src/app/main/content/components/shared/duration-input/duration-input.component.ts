import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { timePeriodUnits } from 'app/constants/constants';

@Component({
  selector: 'irms-duration-input',
  templateUrl: './duration-input.component.html',
  styleUrls: ['./duration-input.component.scss']
})
export class DurationInputComponent implements OnInit {
  @Input() public inputControl: FormGroup;
  @Input() public placeholder: string;
  @Input() public required: boolean;
  @Input() public flexWidth: string;
  @Input() public errorRangeText: string;
  @Input() public fieldLabel: string;
  @Input() public disabled: boolean;
  @Input() afterText: string;
  
  Validators = Validators;
  timePeriodUnits = timePeriodUnits;
  @Output() onBlur: EventEmitter<Event> = new EventEmitter();

  handleBlur(): void {
    this.onBlur.emit(event);
  }
  ngOnInit(): void { }
  keydown(event): void {
    // prevent: "e", "=", ",", "-", "."
    if ([69, 187, 188, 189, 190].includes(event.keyCode)) {
      event.preventDefault();
    }
 }
  public isRequired(): boolean { 
    const validator: any = this.inputControl.validator && this.inputControl.validator(this.inputControl);
    return validator && validator.required;
  }
}
