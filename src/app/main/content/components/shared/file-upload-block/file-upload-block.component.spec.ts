import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadBlockComponent } from './file-upload-block.component';

describe('FileUploadBlockComponent', () => {
  let component: FileUploadBlockComponent;
  let fixture: ComponentFixture<FileUploadBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
