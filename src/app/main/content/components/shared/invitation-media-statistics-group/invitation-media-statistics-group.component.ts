import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { timePeriodUnits } from 'app/constants/constants';
@Component({
  selector: 'irms-invitation-media-statistics-group',
  templateUrl: './invitation-media-statistics-group.component.html',
  styleUrls: ['./invitation-media-statistics-group.component.scss']
})
export class InvitationMediaStatisticsGroupComponent implements OnInit {
  @Input() statistics: FormGroup;
  @Input() emailTemplate;
  @Input() smsTemplate;
  @Input() whatsappTemplate;
  @Input() dateInfo: FormControl;
  @Input() fullWidth = false;
  @Input() isAcceptedRejected = true;
  timePeriodUnits = timePeriodUnits
  date: any = {isInstant: true};

  constructor() { }

  ngOnInit() {
    this.dateInfo.valueChanges.subscribe(x => {
      this.date = x;
    });
    this.date = this.dateInfo.value;
  }
}
