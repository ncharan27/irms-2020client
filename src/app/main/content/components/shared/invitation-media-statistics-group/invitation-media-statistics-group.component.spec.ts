import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitationMediaStatisticsGroupComponent } from './invitation-media-statistics-group.component';

describe('InvitationMediaStatisticsGroupComponent', () => {
  let component: InvitationMediaStatisticsGroupComponent;
  let fixture: ComponentFixture<InvitationMediaStatisticsGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationMediaStatisticsGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationMediaStatisticsGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
