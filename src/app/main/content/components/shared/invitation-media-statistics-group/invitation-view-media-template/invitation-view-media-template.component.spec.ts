import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitationViewMediaTemplateComponent } from './invitation-view-media-template.component';

describe('InvitationViewMediaTemplateComponent', () => {
  let component: InvitationViewMediaTemplateComponent;
  let fixture: ComponentFixture<InvitationViewMediaTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationViewMediaTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationViewMediaTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
