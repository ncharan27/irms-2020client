import { Component, OnInit, Input, OnDestroy, HostBinding } from '@angular/core';
import { RsvpFormService } from './rsvp-form.service';
import { Subject, Subscription } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-rsvp-form',
  templateUrl: './rsvp-form.component.html',
  styleUrls: ['./rsvp-form.component.scss'],
  animations: fuseAnimations
})
export class RsvpFormComponent implements OnInit, OnDestroy {
  section = 'welcome';
  @Input() mockup = false;
  @Input() responseOnlyMode;
  @Input() formObj;
  @Input() themeObj;
  @Input() stepper = -1;

  

  // Host binding for background styling of the form host component
  @HostBinding('style.background') background: any;
  @HostBinding('style.background-color') backgroundColor: any;
  @HostBinding('class.cover') isBackgroundCover = true;
  @HostBinding('class.repeat') isBackgroundRepeat = false;
  @HostBinding('class.no-repeat') isBackgroundNoRepeat = false;

  subscriptions: Subscription[] = [];

  constructor(protected formService: RsvpFormService) {
    const a = this.formService.rvspFormValue;
    this.formObj = a.formContent;
    this.themeObj = a.formTheme;
    this.setBackgroundStyle();
  }

  ngOnInit(): void {
    this.subscriptions.push(this.formService.getRsvpForm().subscribe(data => {
      this.formObj = data.formContent;
      this.themeObj = data.formTheme;
      this.setBackgroundStyle();
    }));
    this.subscriptions.push(this.formService.getRefreshFormTrigger().subscribe(data => this.refreshForm()));
    this.subscriptions.push(this.formService.getCurrentSection().subscribe(data => this.setCurrentSection(data)));
    
    // if(this.stepper>-1){
      
      
    // }
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  setBackgroundStyle(): void{
    this.backgroundColor = this.themeObj.background.color;
    if (this.themeObj.background.image) {
      this.background = `url(${this.themeObj.background.image})`;
    } else {
      this.background = null;
    }
    this.isBackgroundCover = this.isBackgroundNoRepeat = this.isBackgroundRepeat = false;
    if (this.themeObj.background.style === 'repeat') {
      this.isBackgroundRepeat = true;
    } else if (this.themeObj.background.style === 'no-repeat') {
      this.isBackgroundNoRepeat = true;
    } else { this.isBackgroundCover = true; }
  }

  gotoForm(): void {
    this.section = 'rsvp';
  }


  setCurrentSection(section): void {
    this.section = section;
  }

  refreshForm(): void {
    this.section = 'welcome';
  }

  submitForm(): void {

  }
  showThanks(): void {
    this.section = 'thanks';
  }

  acceptInvite(): void {
    this.section = 'accepted';
  }
  rejectInvite(): void {
    this.section = 'rejected';
  }
}
