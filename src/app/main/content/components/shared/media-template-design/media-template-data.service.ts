import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BASE_URL } from 'app/constants/constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MediaTemplateDataService {


  private url = `${BASE_URL}api/campaignInvitation`;

  constructor(private http: HttpClient) { }

  getWhatsAppBotTemplate(id): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}/whatsapp/bot`);
  }

  updateWhatsAppBotTemplate(model): Observable<any> {
    return this.http.post<any>(`${this.url}/whatsapp-bot-template`, model);
  }

}
