import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'irms-rfi-media-group',
  templateUrl: './rfi-media-group.component.html',
  styleUrls: ['./rfi-media-group.component.scss']
})
export class RfiMediaGroupComponent implements OnInit {
  @Input() form;
  @Input() isLoading;
  @Output() edit: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}
