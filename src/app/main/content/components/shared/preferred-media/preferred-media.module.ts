import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignPreferredMediaDialogComponent } from './campaign-preferred-media-dialog/campaign-preferred-media-dialog.component';
import { PreferredMediaListFilterPipe } from './campaign-preferred-media-dialog/preferred-media-list-filter.pipe';
import { PreferredMediaListItemComponent } from './campaign-preferred-media-dialog/preferred-media-list-item/preferred-media-list-item.component';
import { MainModule } from 'app/main/main.module';



@NgModule({
  declarations: [
    CampaignPreferredMediaDialogComponent,
    PreferredMediaListItemComponent,
    PreferredMediaListFilterPipe],
  imports: [
    MainModule,
    CommonModule
  ],
  exports: [
    PreferredMediaListItemComponent,
    PreferredMediaListFilterPipe,
    CampaignPreferredMediaDialogComponent,
  ],
  entryComponents: [
    CampaignPreferredMediaDialogComponent,
    PreferredMediaListItemComponent,
  ]
})
export class PreferredMediaModule { }
