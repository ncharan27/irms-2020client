import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MediaType } from 'app/constants/constants';
import { fuseAnimations } from 'app/core/animations';
import { WhatsappDesignerService } from 'app/main/content/whatsapp-designer/whatsapp-designer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'irms-whatsapp-template-card',
  templateUrl: './whatsapp-template-card.component.html',
  styleUrls: ['./whatsapp-template-card.component.scss'],
  animations: fuseAnimations
})
export class WhatsappTemplateCardComponent implements OnInit {
  @Input() editWhatsApp
  @Input() whatsAppTemplate;
  @Input() showRsvp;
  @Input() disabled;
  @Input() isDialog;
  @Input() isLanding;
  showWhatsApp = true;

  @Output() editWhatsAppEvent: EventEmitter<any> = new EventEmitter();
  @Output() whatsAppPreview: EventEmitter<any> = new EventEmitter();
  @Output() landingWhatsApp: EventEmitter<any> = new EventEmitter();
  @Output() botWhatsApp: EventEmitter<any> = new EventEmitter();

  subscriptions: Subscription[] = [];
  mediaType = MediaType;
  private dialogRef = null;
  private dialogData;
  constructor(
    private dialog: MatDialog,
    private injector: Injector,
    private whatsappDesignerService: WhatsappDesignerService
    ) {
      this.dialogRef = this.injector.get(MatDialogRef, null);
      this.dialogData = this.injector.get(MAT_DIALOG_DATA, null);
  }

  ngOnInit() {
    this.subscriptions.push(this.whatsappDesignerService.triggerPreviewObs().subscribe(data => {
      this.whatsAppPreview.emit();
      this.closeDialogView();
    }));
    this.subscriptions.push(this.whatsappDesignerService.triggerLandingPageObs().subscribe(data => {
      this.landingWhatsApp.emit();
      this.closeDialogView();
    }));
    this.subscriptions.push(this.whatsappDesignerService.triggerBotPageObs().subscribe(data => {
      this.botWhatsApp.emit();
      this.closeDialogView();
    }));
    
    
    if (this.dialogData) {
      this.isDialog = this.dialogData.isDialog;
    }
  }

  expandForEdit(): void {
    this.editWhatsAppEvent.emit();
  }

  openDialogView(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.minWidth = '70vw';
    dialogConfig.data = {
      isDialog: true
    };
    const dialogRef = this.dialog.open(WhatsappTemplateCardComponent, dialogConfig);
    dialogRef.componentInstance.editWhatsApp = this.editWhatsApp;
    dialogRef.componentInstance.whatsAppTemplate = this.whatsAppTemplate;
    dialogRef.componentInstance.showRsvp = this.showRsvp;
    dialogRef.componentInstance.disabled = this.disabled;
    dialogRef.componentInstance.isDialog = true;
    dialogRef.componentInstance.isLanding = this.isLanding;

    dialogRef.afterClosed().subscribe( ex => {
      this.isDialog = false;
      this.whatsappDesignerService.triggerRefresh();
    });

  }

  closeDialogView(): void {
    if (this.dialogRef) {
      this.dialogRef.close(false);
      
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

  whatsAppSave(): void {
    this.whatsappDesignerService.triggerSave();
  }
  whatsAppPreviewMethod(): void {
    this.whatsappDesignerService.triggerPreview();
  }


}
