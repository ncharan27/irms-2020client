import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'irms-whatsapp-option-config-modal',
  templateUrl: './whatsapp-option-config-modal.component.html',
  styleUrls: ['./whatsapp-option-config-modal.component.scss']
})
export class WhatsappOptionConfigModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<WhatsappOptionConfigModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    
  }

  closeDialog(exitData = null): void {
    this.dialogRef.close(exitData);
  }

}
