import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { IActionButtons } from 'app/core/types/types';


@Component({
  selector: 'irms-actions-button-group',
  templateUrl: './actions-button-group.component.html',
  styleUrls: ['./actions-button-group.component.scss']
})
export class ActionsButtonGroupComponent implements OnInit {
  @Input() public actionButtons: IActionButtons;
  @Input() public disabled;
  @Input() public selectedItems: any[];
  @Output() public onClick: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  handleClick(action: () => void): void {
    action();
  }
}
