import {
  AfterViewInit, Component, ComponentFactoryResolver, Input,  Type, ViewChild,
} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { ListDirectiveDirective } from 'app/main/content/directives/list-directive.directive';

@Component({
  selector: 'irms-registration-list',
  templateUrl: './generic-list-container.component.html',
  styleUrls: ['./generic-list-container.component.scss']
})
export class GenericListContainerComponent implements AfterViewInit {
  @Input() public dataListItemBody: Type<any>;
  @Input() public item: Observable<any>;
  @ViewChild(ListDirectiveDirective, {static: false}) dynamicContainer: ListDirectiveDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngAfterViewInit(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.dataListItemBody);
    const viewContainerRef = this.dynamicContainer.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    componentRef.instance.data = this.item;
    componentRef.changeDetectorRef.detectChanges();
  }
}


