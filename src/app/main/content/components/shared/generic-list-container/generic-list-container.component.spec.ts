import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericListContainerComponent } from './generic-list-container.component';

describe('GenericListContainerComponent', () => {
  let component: GenericListContainerComponent;
  let fixture: ComponentFixture<GenericListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
