import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericSearchBarComponent } from './generic-search-bar.component';

describe('GenericSearchBarComponent', () => {
  let component: GenericSearchBarComponent;
  let fixture: ComponentFixture<GenericSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
