import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { QueryService } from 'app/main/content/services/query.service';
import { Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'irms-generic-search-bar',
  templateUrl: './generic-search-bar.component.html',
  styleUrls: ['./generic-search-bar.component.scss']
})
export class GenericSearchBarComponent implements OnInit, OnDestroy {

  @Output() searchText: EventEmitter<any> = new EventEmitter();
  @Input() searchPlaceholder = 'Search';
  @Input() emitQuery = false;
  @Input() debounceTime = 250;
  @ViewChild('search_input', { static: true }) searchInput: ElementRef;
  queryString: string;
  // Observable for debouncing input changes
  private searchDecouncer: Subject<string> = new Subject();
  private unsubscribeAll: Subject<any>;

  constructor(protected queryService: QueryService) {
    this.unsubscribeAll = new Subject();
  }

  ngOnInit(): void {    
    this.setupSearchDebouncer();
    this.queryString = '';
    if (!this.emitQuery) {
      this.queryService.queryString.pipe(takeUntil(this.unsubscribeAll)).subscribe(result => {
        this.queryString = result;
        if (result) {
          this.searchInput.nativeElement.focus();
        }
      });
    }
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
    // this.queryService.resetAllFilters();
  }

  public onSearchInputChange(term: string): void {
    // `onSearchInputChange` is called whenever the input is changed.
    // We have to send the value to debouncing observable
    this.searchDecouncer.next(term);
  }

  private setupSearchDebouncer(): void {
    // Subscribe to `searchDecouncer` values,
    // but pipe through `debounceTime` and `distinctUntilChanged`
    this.searchDecouncer.pipe(
      debounceTime(this.debounceTime),
      distinctUntilChanged(),
    ).subscribe((term: string) => {
      // Remember value after debouncing
      this.queryString = term;
      // Do the actual search
      this.search(term);
    });
  }

  search(term): void{
    if (this.emitQuery) {
      this.searchText.emit(term);
    } else {
      this.queryService.queryString.next(term);
    }
  }
}
