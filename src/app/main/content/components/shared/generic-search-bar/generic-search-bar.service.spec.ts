import { TestBed } from '@angular/core/testing';

import { GenericSearchBarService } from './generic-search-bar.service';

describe('GenericSearchBarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenericSearchBarService = TestBed.get(GenericSearchBarService);
    expect(service).toBeTruthy();
  });
});
