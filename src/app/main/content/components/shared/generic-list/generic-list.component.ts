import {
  Component, EventEmitter, Input, OnInit, Output, ViewChild,
  Type, IterableDiffers, OnDestroy, DoCheck
} from '@angular/core';
import { MatSelectionList } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BASE_PAGE_SIZE } from '../../../../../constants/constants';
import { GenericListService } from './generic-list.service';
import { fuseAnimations } from '@fuse/animations';
import { Subscription } from 'rxjs/Subscription';
import { IResponseModel } from 'app/core/types/types';

@Component({
  selector: 'irms-generic-list',
  templateUrl: './generic-list.component.html',
  styleUrls: ['./generic-list.component.scss'],
  providers: [GenericListService],
  animations: fuseAnimations
})

export class GenericListComponent implements OnInit, OnDestroy, DoCheck {
  public pageSize = BASE_PAGE_SIZE;
  public pageIndex = 0;
  public currentItem: string;
  public hasAllSelected = false;
  public isIndeterminate = false;
  @Input() selectedIds: string[] = [];
  @Input() selectedItems: any[] = [];
  @Input() currentId: string;
  @Input() selectable: boolean;
  @Input() actionMenu: boolean;
  @Input() static = false;
  @Input() listHeaders: string[];
  @Input() loaderType: string;
  @Input() public dataListObservable: Observable<IResponseModel>;
  @Input() public staticList: any[];
  @Input() public dataListItemBody: Type<any>;
  @Input() public selectedIdsList: any[];
  @Input() emptyHeading: string;
  @Input() emptyMessage: string;
  @Input() returnSelectedItemsList = false;
  @Input() svgPositionRelative = false;
  @Input() hideEmptyIllustration = false;
  @Input() NumOfMaxSelectables: number;
  @Input() checked: boolean;
  @Input() indeterminate: boolean;

  @Output() public onCheckChange: EventEmitter<any> = new EventEmitter();
  @Output() public onReadItem: EventEmitter<string> = new EventEmitter();
  @Output() public onSelectAll: EventEmitter<any> = new EventEmitter();
  @Output() public onDeSelectAll: EventEmitter<any> = new EventEmitter();
  @Output() public onChangePagination: EventEmitter<Event> = new EventEmitter();

  @ViewChild('list', { read: MatSelectionList, static: false }) matSelectionList: MatSelectionList;
  onSelectedCurrentItemChanged: Subscription;
  onSelectAllSubscription: Subscription;
  differ: any;

  constructor(private genericListService: GenericListService, differs: IterableDiffers) {
    this.differ = differs.find([]).create(null);
  }

  ngOnInit() {
    this.onSelectedCurrentItemChanged = this.genericListService.onCurrentItemChanged.subscribe(currentId => this.currentItem = currentId);
  }

  ngDoCheck() {
    const change = this.differ.diff(this.selectedIds);
    if (change) {
      this.genericListService.setSelectAllItems(change.collection);
    }

    this.currentItem = this.currentId;
  }

  ngOnDestroy() {
    if (this.onSelectedCurrentItemChanged) {
      this.onSelectedCurrentItemChanged.unsubscribe();
    }
  }


  toggleSelectedId(newSelectedItem) {
    // First, check if we already have that todo as selected...
    const item = newSelectedItem.item ? newSelectedItem.item : newSelectedItem;
    const index = this.selectedIds.indexOf(item.id);
    if (index > -1) {
      this.selectedIds.splice(index, 1);
      this.selectedItems.splice(index, 1);
    }
    else {
      this.selectedIds.push(item.id);
      this.selectedItems.push(item);
    }
    if (this.returnSelectedItemsList) {
      this.onCheckChange.emit({ selectedIds: this.selectedIds, selectedItems: this.selectedItems});
    } else {
      this.onCheckChange.emit(this.selectedIds);
    }
  }
  
  toggleSelectAll(event): void {
    this.onSelectAll.emit(event);
  }

  readItem(item) {
    this.onReadItem.emit(item);
    this.currentItem = item.id;
  }
}
