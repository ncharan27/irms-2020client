import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FeatureListService {
  selectedIds: any[] = [];
  onSelectedTodosChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  onCurrentItemChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  onCurrentIdsSelectesChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  constructor() { }

  toggleSelectedId(newSelectedId) {
    const index = this.selectedIds.indexOf(newSelectedId);
    index > -1 ? this.selectedIds.splice(index, 1) : this.selectedIds.push(newSelectedId);
    this.onSelectedTodosChanged.next(this.selectedIds);
    }


  setCurrentItem(id) {
    this.onCurrentItemChanged.next(id);
  }

  setSelectAllItems(allIds){
    this.onCurrentIdsSelectesChanged.next(allIds);
  }

}
