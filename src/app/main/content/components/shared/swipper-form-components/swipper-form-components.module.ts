import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContinueButtonComponent } from './continue-button/continue-button.component';
import { MainModule } from 'app/main/main.module';
import { FormNavigatorComponent } from './form-navigator/form-navigator.component';
import { TextAnswerComponent } from './text-answer/text-answer.component';
import { ProgressBarColor } from './form-navigator/progress-bar-color';
import { SingleOptionComponent } from './single-option/single-option.component';
import { MultiOptionComponent } from './multi-option/multi-option.component';
import { FilterPipe } from './multi-option/filter.pipe';
import { OnlyNumberDirective } from './phone-number/onlynumber.directive';
import { PhoneNumberComponent } from './phone-number/phone-number.component';
import { IconRatingComponent } from './icon-rating/icon-rating.component';
import { TextMaskModule } from 'angular2-text-mask';
import { RatingModule } from 'ng-starrating';
import { DateFieldComponent } from './date-field/date-field.component';
import { FormReviewComponent } from './form-review/form-review.component';
import { SwipperMatSelectSearchExtComponent } from './swipper-mat-select-search-ext/swipper-mat-select-search-ext.component';

@NgModule({
  declarations: [
    ContinueButtonComponent,
    FormNavigatorComponent,
    TextAnswerComponent,
    ProgressBarColor,
    SingleOptionComponent,
    MultiOptionComponent,
    IconRatingComponent,
    PhoneNumberComponent,
    DateFieldComponent,
    FilterPipe, 
    OnlyNumberDirective, 
    FormReviewComponent, 
    SwipperMatSelectSearchExtComponent,
  ],
  imports: [
    CommonModule,
    MainModule,
    TextMaskModule,
    RatingModule
  ],
  exports: [ 
    ContinueButtonComponent,
    FormNavigatorComponent,
    TextAnswerComponent,    
    PhoneNumberComponent,
    ProgressBarColor,
    IconRatingComponent,
    SingleOptionComponent,
    DateFieldComponent,
    MultiOptionComponent, 
    OnlyNumberDirective, 
    FormReviewComponent,
    SwipperMatSelectSearchExtComponent
  ]
})
export class SwipperFormComponentsModule { }
