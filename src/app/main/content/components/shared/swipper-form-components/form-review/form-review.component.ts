import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'irms-form-review',
  templateUrl: './form-review.component.html',
  styleUrls: ['./form-review.component.scss']
})
export class FormReviewComponent implements OnInit {

  @Input() questions;
  @Input() responseFormGroup: FormGroup;
  @Input() theme: any;
  @Input() isArabic: boolean;
  @Input() saveLoading: boolean;

  @Output() save = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    this.responseFormGroup.valueChanges.subscribe(value => {
    });
  }

  submitForm(): void {
    this.save.emit();
  }

}
