import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'irms-phone-number',
  templateUrl: './phone-number.component.html',
  styleUrls: ['./phone-number.component.scss']
})
export class PhoneNumberComponent implements OnInit {
  @Input() theme: any;
  @Input() isArabic: boolean;
  @Input() question;
  @Input() reviewMode = false;
  @Input() public inputControl: FormControl;
  @Output() scrollNext = new EventEmitter<any>();
  @Output() fieldUpdate = new EventEmitter<string>();
  mobilePhoneObject: any;
  mobilePhoneCountryCode: string;
  public phoneConfig = {
    initialCountry: 'sa',
    preferredCountries: ['sa', 'ae', 'bh', 'kw', 'om', 'ps', 'lb', 'jo', 'sy', 'eg', 'us'],
    excludeCountries: ['il', 'ir', 'kp']
  };

  setAsCurrent(): void{
    
  }

  constructor() { }

  ngOnInit(): void{
  }

/// On Enter key press
  onPressEnter(event): void{
    if (event.keyCode === 13 || event.keyCode === 9) {
      event.preventDefault();
      this.gotoNext();
    }
  }

  /**
   * Go to next
   */
  gotoNext(): void{
    if (this.reviewMode) {
      return;
    }
    this.fieldUpdate.next();
    this.scrollNext.emit();
  }

  getNumber(e): void{
    this.inputControl.setValue(e);
  }

  hasError(notError): void{
    if (!notError && this.inputControl.value) {
      this.inputControl.setErrors({ phoneError: 'Please enter a valid phone number' });
    }
  }


}
