import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'irms-refresh-form-button',
  templateUrl: './refresh-form-button.component.html',
  styleUrls: ['./refresh-form-button.component.scss']
})
export class RefreshFormButtonComponent implements OnInit {

  @Output()
  refreshClick = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void{
  }

  refresh(): void{
    this.refreshClick.emit();
  }

}
