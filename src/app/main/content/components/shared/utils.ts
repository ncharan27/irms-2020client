function ResetFormFields(form) {
  for (const name in this[form].controls) {
    if (this[form].controls.hasOwnProperty(name)) {
      this[form].controls[name].setValue('');
      this[form].controls[name].setErrors(null);
    }
  }
}



export {ResetFormFields};
