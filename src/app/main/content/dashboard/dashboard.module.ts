import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';

const routes = [
  {
    path: '',
    component: DashboardComponent, 
    canLoad: [AuthGuard], 
    data: { expectedRoles: [RoleType.TenantAdmin] },
  }];

@NgModule({
  declarations: [
  ],
  imports: [
    MainModule,
    ContentModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
