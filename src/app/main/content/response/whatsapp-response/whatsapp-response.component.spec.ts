import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WhatsappResponseComponent } from './whatsapp-response.component';

describe('WhatsappResponseComponent', () => {
  let component: WhatsappResponseComponent;
  let fixture: ComponentFixture<WhatsappResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
