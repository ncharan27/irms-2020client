import { TestBed } from '@angular/core/testing';

import { GlobalContactDataService } from './global-contact-data.service';

describe('GlobalContactDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalContactDataService = TestBed.get(GlobalContactDataService);
    expect(service).toBeTruthy();
  });
});
