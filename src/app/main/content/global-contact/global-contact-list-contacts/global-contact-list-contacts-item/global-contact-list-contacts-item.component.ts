import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-global-contact-list-contacts-item',
  templateUrl: './global-contact-list-contacts-item.component.html',
  styleUrls: ['./global-contact-list-contacts-item.component.scss']
})
export class GlobalContactListContactsItemComponent implements OnInit {
@Input() data;
  constructor() { }

  ngOnInit() {
  }

}
