import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IActionButtons } from 'types';
import { fuseAnimations } from '@fuse/animations';
import { SectionListComponent } from '../../components/shared/section/section-list/section-list.component';
import { GlobalContactModel } from '../global-contact.model';
import { GlobalContactService } from '../global-contact.service';
import { GlobalContactDataService } from '../global-contact-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Section, sideDialogConfig } from 'app/constants/constants';
import { of, Subscription } from 'rxjs';
import { GlobalContactListContactsItemComponent } from './global-contact-list-contacts-item/global-contact-list-contacts-item.component';
import { ToasterService } from 'angular5-toaster/dist';
import { PopUp } from 'app/core/decorators/PopUp.decorator';
import { appInjector } from 'app/main/app-injector';
import { Location } from '@angular/common';
import { GlobalContactExportToGuestModalComponent } from '../global-contact-export-to-guest-modal/global-contact-export-to-guest-modal.component';
import { GlobalContactListExportModalComponent } from 'app/main/global-contact-list-export-modal/global-contact-list-export-modal.component';
import { DialogAnimationService } from '../../services/dialog-animation.service';


@Component({
  selector: 'irms-global-contact-list-contacts',
  templateUrl: './global-contact-list-contacts.component.html',
  styleUrls: ['./global-contact-list-contacts.component.scss'],
  animations: fuseAnimations
})
export class GlobalContactListContactsComponent extends SectionListComponent<GlobalContactModel> implements OnInit {
  public listItemComponent: Object = GlobalContactListContactsItemComponent;
  listHeaders = ['Contact Name', 'Date Added', 'Date Updated'];
  isLocked = true;
  selectedContacts = [];
  tempSelected = [];
  public contactsListContact;

  public actionButtons: IActionButtons = {
    leftSide: [
      {
        title: 'Add to Guest List',
        svgIcon: 'add_contacts',
        handler: this.addToList.bind(this),
      },
      {
        title: 'Remove from the List',
        svgIcon: 'remove_contacts',
        handler: this.removeContacts.bind(this),
      },
      {
        title: 'Export Selected Contacts',
        icon: 'launch',
        handler: this.exportSelectedContacts.bind(this),
      }
    ],
  };
  listName: any;
  contactListPage;
  listId: any;
  isLoading: boolean;
  allCount: number;
  NumOfMaxSelectables: number;
  getListSubscription: Subscription;
  constructor(public sectionService: GlobalContactService,
              protected dataService: GlobalContactDataService,
              private dialog: MatDialog,
              protected location: Location,
              router: Router, public route: ActivatedRoute,
              public animDialog: DialogAnimationService) {
    super(Section.EventGuests, sectionService, dataService, router);
  }

  ngOnInit(): void {
    this.sectionService.subscriptions = [];
    this.route.params.subscribe((params: Params) => {
      this.listId = params.lid;
      this.isLoading = true;
      this.contactListPage = {
        listId: params.lid,
        pageSize: 5,
        pageNo: 1
      };
      
      this.dataService.getListName({
        id: params.lid
      })
        .subscribe(result => {
          this.listName = result;
        });

      this.sectionService.setFilter({});
      // super.ngOnInit();
    });
    this.reloadContacts(true);
  }

  private reloadContacts(firstInit = false): void {
    // this.isLocked = true;
    if ( this.getListSubscription ) {
      this.getListSubscription.unsubscribe();
    }
    this.getListSubscription = this.dataService.getGlobalContactListByListId(this.contactListPage)
      .subscribe(response => {
        if (firstInit) {
          this.allCount = response.totalCount;
        }
        this.data = of(response);
        this.sectionService.data.next(response);
        this.totalCount = response.totalCount;
        this.isLoading = false;
      });
  }

  public search(text): void {
    if (!text) {
      this.contactListPage.searchText = null;
    } else {
      this.contactListPage.searchText = text;
    }
    this.contactListPage.pageNo = 1;
    this.sectionService.pageIndex = 0;
    this.sectionService.pageSize = this.contactListPage.pageSize;
    this.reloadContacts();
  }

  public changePagination(event): void {
    this.contactListPage.pageNo = event.pageIndex + 1;
    this.contactListPage.pageSize = event.pageSize;
    this.reloadContacts();
    if(this.contactListPage.pageSize > this.totalCount){
      this.NumOfMaxSelectables = this.totalCount;
    } else {
      
      //this.NumOfMaxSelectables = this.contactListPage.pageSize;
    }
  }

  exportAllGuests(): void {
  }

  goToImportContacts(): any {
    this.router.navigateByUrl(`/global-contacts/upload-contact-list`, {
      state: {
        data: {
          listId: this.listId
        }
      }
    });
  }
  
  goToCreateContact(): any {
    this.router.navigateByUrl(`/global-contacts/add-contact`, {
      state: {
        data: {
          listId: this.listId
        }
      }
    });
  }

  exportSelectedContacts(): void {
    const dialogConfig:any = sideDialogConfig;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    // dialogConfig.height = '55vh';
    dialogConfig.data = {
      isFromContactModule : true
    };

    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const columns = result.columns;
          const customFieldIds = result.customFieldIds;

          this.dataService
            .exportContacts({
              contactIds: this.selectedContacts,
              contactListId: this.listId,
              filename: this.listName,
              columns: columns,
              customFieldIds: customFieldIds
            }).subscribe(resp => {
              this.saveExportedContacts(resp);
            });
        }
      });
  }

  exportContacts(): void {
    const dialogConfig:any = sideDialogConfig;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '40vw';
    // dialogConfig.height = '55vh';
    dialogConfig.data = {
      isFromContactModule : true
    };
    
    const dialogRef = this.animDialog.open(GlobalContactListExportModalComponent, dialogConfig);

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          const columns = result.columns;
          const customFieldIds = result.customFieldIds;

          this.dataService
            .exportContacts({
              contactListId: this.listId,
              filename: this.listName.listName,
              columns: columns,
              customFieldIds: customFieldIds
            }).subscribe(resp => {
              this.saveExportedContacts(resp);
            });
        }
      });
  }

  private saveExportedContacts(resp: any): void {
    const element = document.createElement('a');
    element.href = URL.createObjectURL(resp.body);
    element.download = `${this.listName.listName}.xlsx`;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  @PopUp('Are you sure want to remove selected contacts from the list?')
  public removeContacts(): void {
    this.dataService.removeContactsFromContactList({
      contactIds: this.selectedContacts,
      contactListId: this.listId
    })
      .subscribe(result => {
        this.selectedContacts = [];
        this.reloadContacts(true);
      });
  }

  @PopUp(`Are you sure want to delete the entire contact list?`)
  public deleteList(): void {
    const toasterService = appInjector().get(ToasterService);


    this.dataService.deleteContactList({
      id: this.listId
    })
      .subscribe(result => {
        if (result) {
          toasterService.pop('success', null, 'Successfully deleted.');
          this.location.back();
        } else {
          toasterService.pop('error', 'This list can not be deleted!', 'This list can’t be deleted because it is in use with a campaign.\n'
            + 'Please disassociate this list from this association before attempting to delete.');
        }
        this.sectionService.doRefreshList();
      });
  }

  public selectionChange(event): void {
    if(this.maxSelectable === 0){
      this.setMaxSelectable();
    }
    this.selectedContacts = event;
    if(this.selectedContacts.length != this.maxSelectable && this.selectedContacts.length > 0){
      this.indeterminate = true;
      this.checked = false;
    } else if(this.selectedContacts.length == this.maxSelectable){
      this.indeterminate = false;
      this.checked = true;
    } else {
      this.checked = false;
      this.indeterminate = false;
    }
  }

  /// handle select all
  public selectAll(event): void {
    const data = this.toggleSelectAll();
    if (event) {
      this.selectedContacts = this.contactsListContact;
      this.selectionChange(data);
    } else { 
      this.selectionChange([]);

    }
    
  }

  public addToList(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50vw';
    const dialogRef = this.dialog.open(GlobalContactExportToGuestModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        const request = {
          listId: data.guestListId,
          contactIds: this.selectedContacts
        };
        this.dataService.appendGuestsToList(request)
        .subscribe(x => {
        });
      }
    });
  }
}
