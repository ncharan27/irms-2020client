import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactListItemRenameModalComponent } from './global-contact-list-item-rename-modal.component';

describe('GlobalContactListItemRenameModalComponent', () => {
  let component: GlobalContactListItemRenameModalComponent;
  let fixture: ComponentFixture<GlobalContactListItemRenameModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListItemRenameModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListItemRenameModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
