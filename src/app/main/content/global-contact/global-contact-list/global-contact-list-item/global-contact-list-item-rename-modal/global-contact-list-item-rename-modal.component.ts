import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'irms-global-contact-list-item-rename-modal',
  templateUrl: './global-contact-list-item-rename-modal.component.html',
  styleUrls: ['./global-contact-list-item-rename-modal.component.scss']
})
export class GlobalContactListItemRenameModalComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<GlobalContactListItemRenameModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.form = this.formBuilder.group({
      name: [data.name, [Validators.required, Validators.required, Validators.minLength(2), Validators.maxLength(100)]]
    });
  }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

  confirm() {
    this.dialogRef.close({
      name: this.form.controls['name'].value
    });
  }
}
