import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactListUploadComponent } from './global-contact-list-upload.component';

describe('GlobalContactListUploadComponent', () => {
  let component: GlobalContactListUploadComponent;
  let fixture: ComponentFixture<GlobalContactListUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
