import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateDataStepGlobalContactComponent } from './validate-data-step-global-contact.component';

describe('ValidateDataStepGlobalContactComponent', () => {
  let component: ValidateDataStepGlobalContactComponent;
  let fixture: ComponentFixture<ValidateDataStepGlobalContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateDataStepGlobalContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateDataStepGlobalContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
