import { Component, OnInit, Input } from '@angular/core';
import { GlobalContactListUploadFormService } from '../global-contact-list-upload-form.service';
import { FormGroup, FormBuilder, FormArray, Validators, ValidatorFn, FormControl } from '@angular/forms';
import { MatTableDataSource, MatDialog, MatDialogConfig, MatStepper } from '@angular/material';
import { RegexpPattern } from 'app/constants/constants';
import { CanExit } from 'app/main/content/services/can-exit.guard';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { GlobalContactDataService } from '../../global-contact-data.service';
import { forEach } from 'lodash';
import { KeysPipe } from '@fuse/pipes/keys.pipe';
import { group } from 'console';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { appInjector } from 'app/main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/src/toaster.service';

@Component({
  selector: 'irms-validate-data-step-global-contact',
  templateUrl: './validate-data-step-global-contact.component.html',
  styleUrls: ['./validate-data-step-global-contact.component.scss']
})
export class ValidateDataStepGlobalContactComponent implements OnInit, CanExit  {
  validateStep: FormGroup;
  @Input() stepperRef: MatStepper;
  submit = false;
  customFields: any[];

  constructor(private fb: FormBuilder, 
              private formService: GlobalContactListUploadFormService, 
              private dialog: MatDialog,
              private dataService: GlobalContactDataService
  ) {
    this.validateStep = this.fb.group({
      headers: [],
      contacts: new FormArray([])
    });
    this.formService.stepReady(this.validateStep, 'validate');
  }
  displayedColumns: string[] = [];
  dataSource;
  mandatory = ['Title', 'FullName', 'PrefferedName', 'Gender', 'Email', 'AlternateEmail', 'MobileNumber', 'SecondaryMobileNumber', 'Nationality', 'Position', 'ExpirationDate', 'IssuingCountry'];
  ngOnInit(): void {
    this.loadCustomFields()
      .subscribe(() => {
        const contactsData = this.formService.getContactsData();
        this.validateStep.controls.headers.patchValue(contactsData.headers);
        contactsData.contacts.controls.forEach((element: FormGroup) => {
          const formValidator = [];
          if (contactsData.headers.indexOf('Title') > -1) {
            element.get('Title').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
          }
          if (contactsData.headers.indexOf('FullName') > -1) {
            element.get('FullName').setValidators([Validators.required, Validators.minLength(2), Validators.maxLength(100)]);
          }
          if (contactsData.headers.indexOf('PrefferedName') > -1) {
            element.get('PrefferedName').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
          }
          if (contactsData.headers.indexOf('Gender') > -1) {
            element.get('Gender').setValidators(this.isValidGender());
          }
          if (contactsData.headers.indexOf('Email') > -1) {
            element.get('Email').setValidators([Validators.pattern(RegExp(RegexpPattern.email))]);
            formValidator.push(this.isMatchEmail());
          }
          if (contactsData.headers.indexOf('AlternateEmail') > -1) {
            element.get('AlternateEmail').setValidators(Validators.pattern(RegExp(RegexpPattern.email)));
          }
          if (contactsData.headers.indexOf('MobileNumber') > -1) {
            element.get('MobileNumber').setValidators([Validators.pattern(RegExp(RegexpPattern.intlPhone))]);
            formValidator.push(this.isMatchPhone());
          }
          if (contactsData.headers.indexOf('SecondaryMobileNumber') > -1) {
            element.get('SecondaryMobileNumber').setValidators([Validators.pattern(RegExp(RegexpPattern.intlPhone))]);
          }
          if (contactsData.headers.indexOf('Nationality') > -1) {
            element.get('Nationality').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
          }
          if (contactsData.headers.indexOf('Position') > -1) {
            element.get('Position').setValidators([Validators.minLength(2), Validators.maxLength(100)]);
          }
          if (contactsData.headers.indexOf('ExpirationDate') > -1) {
            element.get('ExpirationDate').setValidators([Validators.pattern(RegExp(`[0-9]{2}\/[0-9]{2}\/[0-9]{4}`))]);
          }
          if (contactsData.headers.indexOf('Email') > -1 || contactsData.headers.indexOf('MobileNumber') > -1){
            formValidator.push(this.isEmailOrMobile());
          }

          // For custom fields
          this.customFields.forEach(customField => {
            if (contactsData.headers.indexOf(customField.name) > -1) {
              const validators = [];
              switch (customField.customFieldType) {
                  case 0: // text
                    if (customField.minValue) {
                      validators.push(Validators.minLength(customField.minValue));
                    }
                    if (customField.maxValue) {
                      validators.push(Validators.maxLength(customField.maxValue));
                    }
                    break;
                
                  case 1: // number
                    if (customField.minValue) {
                      validators.push(Validators.pattern(RegExp(RegexpPattern.digits)), this.minDigitsLength(customField.minValue));
                    }
                    if (customField.maxValue) {
                      validators.push(Validators.pattern(RegExp(RegexpPattern.digits)), this.maxDigitsLength(customField.maxValue));
                    }
                    
                    break;
                  case 2: // date
                    if (customField.minValue) {
                      validators.push(this.minDate(customField.minValue));
                    }
                    if (customField.maxValue) {
                      validators.push(this.maxDate(customField.maxValue));
                    }
                    
                    break;

                  default:
                    break;

                }
              element.get(customField.name).setValidators(validators);
              // element.get(customField.name).setValidators([Validators.minLength(2), Validators.maxLength(100)]);
            }
          });
          
          
          element.setValidators(formValidator);
          this.contacts.push(element);
        });
        const temp = ['#'];  
        this.displayedColumns = temp.concat(this.validateStep.controls.headers.value);
        this.displayedColumns.push('actions');
        this.refreshTable();
        // this.checkIfDuplicateContacts();
      });
      // this.validateStep.valueChanges.subscribe(()=>this.validateStep.updateValueAndValidity())
  }

  loadCustomFields(): Observable<any> {
    return this.dataService.getAllCustomFields()
    .pipe(
      map(x => {
        this.customFields = x;
      })
    );
  }

  get contacts(): FormArray {
    return this.validateStep.get('contacts') as FormArray;
  }

  checkIfDuplicateContacts(): void {
    // API call for check if any of the parsed contacts already exist in db
    const contacts = this.contacts.controls.map((contact: FormGroup) => {
      return {
        email : contact.controls.Email.value !== '' && contact.controls.Email.value ? contact.controls.Email.value.trim() : '',
        mobileNumber : contact.controls.MobileNumber.value !== '' &&  contact.controls.MobileNumber.value ?  contact.controls.MobileNumber.value.trim() : ''
      };
    });
    // call API
    // set error { contactExists: true } to the contact group that exists
  }

  checkIfDuplicateContact(contact: FormGroup): void {
    // API call for check if a given contact already exist in db
    const contactInfo = {
      email : contact.controls.Email.value !== '' && contact.controls.Email.value ? contact.controls.Email.value.trim() : '',
      mobileNumber : contact.controls.MobileNumber.value !== '' &&  contact.controls.MobileNumber.value ?  contact.controls.MobileNumber.value.trim() : ''
    };
    // call API
    // set error { contactExists: true } to the contact group that exists
  }

  removeContact(index): void {
    // remove a contact form list
    this.contacts.removeAt(index);
    this.refreshTable();
  }
  keepContact(index): void {
    const duplicatePhoneIndexes = [];
    const duplicateEmailIndexes = [];
    // keep a duplicate/invalid contact in list...and remove conflicting ones

    // get indexes of conflict phone
    if (this.contacts.controls[index].hasError('duplicatePhone')) {
      const phones = this.contacts.controls.map((contact: FormGroup) => contact.controls.MobileNumber.value !== '' &&  contact.controls.MobileNumber.value ?  contact.controls.MobileNumber.value.trim() : '');
      phones.forEach((mobileNumber, i) => {
        if (mobileNumber === this.contacts.controls[index].get('MobileNumber').value) {
          duplicatePhoneIndexes.push(i);
        }
      });
    }
    // get indexes of conflict email
    if (this.contacts.controls[index].hasError('duplicateEmail')) {
      const emails = this.contacts.controls.map((contact: FormGroup) => contact.controls.Email.value !== '' && contact.controls.Email.value ? contact.controls.Email.value.trim() : '');
      emails.forEach((email, i) => {
        if (email === this.contacts.controls[index].get('Email').value) {
          duplicateEmailIndexes.push(i);
        }
      });
    }
    const conflicts = Array.from(new Set(duplicatePhoneIndexes.concat(duplicateEmailIndexes)));
    conflicts.splice(conflicts.indexOf(index), 1); /// from conflicts array remove the index to keep
    for (const conflict of conflicts) {
      this.contacts.removeAt(conflict);
    }
    this.refreshTable();
  }

  refreshTable(): void {
    this.contacts.controls.forEach(c => c.updateValueAndValidity());
    this.dataSource = new MatTableDataSource(this.contacts.controls);
  }

  private isMatchPhone(): ValidatorFn {
    // To validate duplicate phone number in list
    return (group: FormGroup): { [key: string]: boolean } | null => {
      const phones = this.contacts.controls.map((contact: FormGroup) => contact.controls.MobileNumber.value !== '' &&  contact.controls.MobileNumber.value ?  contact.controls.MobileNumber.value.trim() : '');
      const duplicate = phones.filter(item => item === group.controls.MobileNumber.value).length;
      // console.log(group.controls.MobileNumber.value)
      if (duplicate > 1 && group.controls.MobileNumber.value) {
        return { duplicatePhone: true };
      } else {
        return null;
      }
    };
  }
  private isMatchEmail(): ValidatorFn {
    // To validate duplicate phone number in list
    // To validate duplicate email in list
    return (group: FormGroup): { [key: string]: boolean } | null => {
      const emails = this.contacts.controls.map((contact: FormGroup) => contact.controls.Email.value !== '' && contact.controls.Email.value ? contact.controls.Email.value.trim() : '');
      const duplicate = emails.filter(item => item === group.controls.Email.value).length;
      // console.log("@@@@",group.errors)
      if (duplicate > 1 && group.controls.Email.value) {
        return { duplicateEmail: true };
      } else {
        return null;
      }
    };
  }

  private isEmailOrMobile(): ValidatorFn {
    return (group: FormGroup): { [key: string]: boolean } | null => {
      const phone = this.formService.getContactsData().headers.indexOf('MobileNumber') > -1;
      const email = this.formService.getContactsData().headers.indexOf('Email') > -1;
      if (email && group.controls.Email.value == null){
        group.controls.Email.setValue('');
      }
      if (phone && group.controls.MobileNumber.value == null){
        group.controls.MobileNumber.setValue('');
      }
      if (phone && !email) {
        if (group.controls.MobileNumber.value !== '') { 
          return null;
        } else {
          return { emailOrMobile : true};
        }
      } else if (!phone && email) {
        if ( group.controls.Email.value !== '') { 
          return null;
        } else {
          return { emailOrMobile : true};
        }
      } else if (phone && email) {
        // console.log(group.controls.Email.value, '@@' , group.controls.MobileNumber.value)
        if ((group.controls.Email.value == null || group.controls.Email.value.length === 0) &&
        (group.controls.MobileNumber.value == null || group.controls.MobileNumber.value.length === 0)) { 
          return { emailOrMobile : true};
        } else {
          return null;
        }
      } else {
        return null;
      }
      
      
      // if ((this.formService.getContactsData().headers.indexOf('MobileNumber') && !phone && this.formService.getContactsData().headers.indexOf('Email') && !email)) {
      //   return { emailOrMobile: true };
      // } else {
      //   return null;
      // }
    };
  }

  private isValidGender(): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean} | null => {
      if (control.value !== 'Male' && control.value !== 'Female' && control.value !== '' && control.value !== null) {
        return { genderError: true };
      } else {
        return null;
      }
    };
  }

  private minDigitsLength(size: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      const numberr = control.value.split('.').join('');
      if (numberr.length < size) {
        return { minDigits: true };
      }
      return null;
    };
  }

  private maxDigitsLength(size: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      const numberr = control.value.split('.').join('');

      
      if (numberr.length > size) {
        return { maxDigits: true };
      }
      return null;
    };
  }

  private minDate(date: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      const value = Date.parse(control.value);
      if (date > value) {
        return { minDate: true };
      }

      return null;
    };
  }

  private maxDate(date: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean} | null => {
      if (control.value == null) {
        return null;
      }

      const value = Date.parse(control.value);
      if (date < value) {
        return {maxDate: true};
      }
      
      return null;
    };
  }

  uploadList(): void {
    // API call for submit form
    const list = this.formService.contactListUploadForm.controls.listForm.value;

    let listValue;
    switch (list.list)
    {
      case 'add':
        listValue = {
          list: 'add'
        };
        break;

      case 'existing':
        listValue = {
          list: 'existing',
          listName: list.listId.id
        };
        break;

      case 'new':
        listValue = {
          list: 'new',
          listName: list.listName
        };
        break;
      
      case 'eventGuest':
        listValue = {
          list: 'eventGuest',
          listName: list.eventGuestId.id
        };
        break;
    }

    const data = {
      list : listValue,
      contacts: this.formService.contactListUploadForm.controls.validateForm.get('contacts').value
    };
    // delete data.list.csvFile;
    this.dataService.importCsv(data)
    .subscribe(x => {
      // if success
      this.stepperRef.next();
    }, error => {
      
      if (error.error && error.error.code === 503) {
        const toasterService = appInjector().get(ToasterService);
        if (error.error && error.error.message) {
          toasterService.pop('error', null, error.error.message);
        } else {
          toasterService.pop('success', null, 'Upload contacts are queued by background process');
        }
      }
    });    
  }

  canDeactivate(): any {
    if (this.validateStep.dirty || ! this.submit) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      const dialogRef = this.dialog.open(FuseConfirmDialogComponent, dialogConfig);
      dialogRef.componentInstance.confirmHeading = 'Are you sure you want to leave this page?';
      dialogRef.componentInstance.confirmButton = 'Leave page';
      dialogRef.componentInstance.cancelButton = 'Stay';
      dialogRef.componentInstance.confirmMessage = 'This action will take you out of this page...\nDo you want to proceed?';
      return dialogRef.afterClosed();
    }
    else {
      return true;
    }
  }

}
