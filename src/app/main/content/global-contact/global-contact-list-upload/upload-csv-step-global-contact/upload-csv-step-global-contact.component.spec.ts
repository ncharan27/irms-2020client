import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadCsvStepGlobalContactComponent } from './upload-csv-step-global-contact.component';

describe('UploadCsvStepGlobalContactComponent', () => {
  let component: UploadCsvStepGlobalContactComponent;
  let fixture: ComponentFixture<UploadCsvStepGlobalContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadCsvStepGlobalContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadCsvStepGlobalContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
