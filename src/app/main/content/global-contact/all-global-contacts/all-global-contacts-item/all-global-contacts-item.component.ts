import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'irms-all-global-contacts-item',
  templateUrl: './all-global-contacts-item.component.html',
  styleUrls: ['./all-global-contacts-item.component.scss']
})
export class AllGlobalContactsItemComponent implements OnInit {
@Input() data;
  constructor() { }

  ngOnInit() {
  }

}
