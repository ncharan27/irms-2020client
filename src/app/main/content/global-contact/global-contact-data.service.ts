import { Injectable } from '@angular/core';
import { SectionDataService } from '../components/shared/section/section-data.service';
import { GlobalContactModel } from './global-contact.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Loader } from 'app/core/decorators/loader.decorator';
import { Observable, of } from 'rxjs';
import { IResponseModel, IPage } from 'types';
import { Toast } from 'app/core/decorators/toast.decorator';
import { BASE_URL } from 'app/constants/constants';
// import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class GlobalContactDataService implements SectionDataService<GlobalContactModel> {



  private url = `${BASE_URL}api/contact`;
  private eventUrl = `${BASE_URL}api/event`;
  private customFieldUrl = `${BASE_URL}api/customField`;
  private contactDetailUrl = `${BASE_URL}api/contactDetail`;
  private dataModuleUrl = `${BASE_URL}api/dataModule`

  constructor(private http: HttpClient) { }

  deleteContacts(model): Observable<any> {
    return this.http.request('delete', `${this.url}/delete-contacts`, { body: model });
  }

  transferContactToGuestList(filterParam): Observable<any> {
    return this.http.post(`${this.url}/callhere`, filterParam);
  }
  getEventGuestList(id): Observable<any> {
    return this.http.get(`${BASE_URL}api/dictionary/${id}/event-guest-list`)
  }

  @Toast('Successfully removed')
  removeContactsFromContactList(filterParam): Observable<any> {
    return this.http.post(`${this.url}/remove-contact-from-list`, filterParam);
  }
  @Toast('Successfully exported')
  exportContacts(data): Observable<any> {
    return this.http.post(`${this.url}/export-users`, data, {
      responseType: 'blob' as 'json',
      observe: 'response' as 'body',
    });
  }

  renameList(filterParam): Observable<any> {
    return this.http.post(`${this.url}/rename-list`, filterParam);
  }

  deleteContactList(data): Observable<any> {
    return this.http.post(`${this.url}/delete-contact-list`, data);
  }

  deleteGlobalList(id): Observable<any> {
    return this.http.get(`${this.dataModuleUrl}/remove-contact-list/${id}`);
  }

  updateReservedFields(filterParam: any, id: number): Observable<any> {
    return this.http.put<any>(`${this.url}/id`, filterParam);
  }

  getUnlockedLists(id): Observable<any> {
    return this.http.get<any>(`${this.url}/unlocked-lists/${id}`);
  }

  @Toast('Contacts were successfully added')
  appendGuestsToList(data): Observable<any> {
    return this.http.post<any>(`${this.url}/append-contacts-to-list`, data);
  }

  @Toast('Successfully added to event guest list')
  appendContactListToGuestsList(data): Observable<any> {
    return this.http.post<any>(`${this.url}/append-list-to-list`, data);
  }

  getGlobalContactBasicInfo(id, listId): Observable<any> {
    return this.http.post<any>(`${this.contactDetailUrl}/personal-details`, {
      guestListId: listId,
      contactId: id
    });
  }

  getContactAssociatedLists(id: number): Observable<any> {
    return this.http.get<any>(`${this.contactDetailUrl}/associated-lists/${id}`);
  }

  updateCustomFields(id: any, data: any): Observable<any> {
    return this.http.post<any>(`${this.contactDetailUrl}/custom-fields/${id}`, data);
  }

  getContactCustomFields(guestId: any): Observable<any> {
    return this.http.get<any>(`${this.contactDetailUrl}/custom-fields/${guestId}`)
  }

  @Loader()
  getList(filterParam: any): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/contact-lists`, filterParam);
  }

  getGuestList(filterParam: any): Observable<IResponseModel> {
    return this.http.post<IResponseModel>(`${this.url}/global-guest-lists`, filterParam);
  }

  getContactReservedFields(id: any): Observable<any> {
    return this.http.get<any>(`${this.contactDetailUrl}/personal-info/${id}`);
  }

  get(id): Observable<GlobalContactModel> {
    return of();//this.http.get<GlobalContactModel>(`${this.url}/${id}`);
  }

  getGlobalContactList(): Observable<GlobalContactModel[]> {
    return this.http.get<GlobalContactModel[]>(`${this.url}/contact-list`);
  }

  isListNameUnique(name): Observable<boolean> {
    return this.http.post<boolean>(`${this.url}/list-name-uniqueness`, {
      name
    });
  }

  getGlobalContactListByListId(filterParam: any): Observable<any> {
    return this.http.post<IResponseModel>(`${this.url}/contact-list-by-list-id`, filterParam);
  }

  getAllGlobalContacts(filterParam: any): Observable<any> {
    return this.http.post<IResponseModel>(`${this.url}/all-contacts`, filterParam);
  }

  getListName(data): Observable<any> {
    return this.http.post(`${this.url}/list-name`, data);
  }

  getGlobalContactsCount(): Observable<any> {
    return this.http.get<any>(`${this.url}/contacts-count`);
  }

  checkEmailUniqueness(email): Observable<any> {
    return this.http.post(`${this.url}/CheckEmailUniqueness`, email);
  }

  checkPhoneUniqueness(phone): Observable<any> {
    return this.http.post(`${this.url}/CheckPhoneUniqueness`, phone);
  }

  addContact(model: any): Observable<string> {
    const header = new HttpHeaders();
    return this.http.post<string>(`${this.url}/create-contact`, model, { headers: header });
  }


  @Toast('Successfully created')
  create(model: any): Observable<string> {
    const header = new HttpHeaders();
    return this.http.post<string>(`${this.url}`, model, { headers: header });
  }

  createList(model: any): Observable<string> {
    const header = new HttpHeaders();
    return this.http.post<string>(`${this.url}/contact-list`, model, { headers: header });
  }


  @Toast('Successfully updated')
  update(model): Observable<any> {
    return this.http.post<any>(`${this.contactDetailUrl}/global-contact-personal-info`, model);
  }

  @Toast('Successfully deleted')
  delete(model): Observable<any> {
    return this.http.request('delete', `${this.url}`, { body: model });
  }

  getEvents(): Observable<any> {
    return this.http.post(`${this.eventUrl}/list`, { pageNo: 1, pageSize: 100 });
  }

  importCsv(data): Observable<any> {
    return this.http.post(`${this.url}/import-csv`, data);
  }

  @Toast('Successfully created')
  createCustomField(data): Observable<any> {
    return this.http.post(`${this.customFieldUrl}/create`, data);
  }

  getAllCustomFields(): Observable<any> {
    return this.http.post(`${this.customFieldUrl}/get-all`, { fields: []});
  }

  loadImportTemplate(): Observable<any> {
    return this.http.get(`${this.url}/template-csv-file`, {
      responseType: 'blob' as 'json',
      observe: 'response' as 'body',
    });
  }
}
