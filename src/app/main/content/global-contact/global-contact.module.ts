import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalContactComponent } from './global-contact.component';
import { RoleType } from 'app/constants/constants';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { GlobalContactListComponent } from './global-contact-list/global-contact-list.component';
import { RouterModule } from '@angular/router';
import { GlobalContactCreateComponent } from './global-contact-list-create/global-contact-list-create.component';
import { GlobalContactListItemComponent } from './global-contact-list/global-contact-list-item/global-contact-list-item.component';
import { MainModule } from 'app/main/main.module';
import { GlobalContactAddComponent } from './global-contact-add/global-contact-add.component';
import { GlobalContactListUploadComponent } from './global-contact-list-upload/global-contact-list-upload.component';
import { UploadCsvStepGlobalContactComponent } from './global-contact-list-upload/upload-csv-step-global-contact/upload-csv-step-global-contact.component';
import { MapFieldStepGlobalContactComponent } from './global-contact-list-upload/map-field-step-global-contact/map-field-step-global-contact.component';
import { ValidateDataStepGlobalContactComponent } from './global-contact-list-upload/validate-data-step-global-contact/validate-data-step-global-contact.component';
import { MatTableModule } from '@angular/material';
import { CanExitGuard } from '../services/can-exit.guard';
import { GlobalContactListContactsComponent } from './global-contact-list-contacts/global-contact-list-contacts.component';
import { GlobalContactListContactsItemComponent } from './global-contact-list-contacts/global-contact-list-contacts-item/global-contact-list-contacts-item.component';
import { GlobalContactDetailsComponent } from './global-contact-details/global-contact-details.component';
import { GlobalContactDetailsReservedFieldsComponent } from './global-contact-details/global-contact-details-reserved-fields/global-contact-details-reserved-fields.component';
import { GlobalContactDetailsCustomFieldsComponent } from './global-contact-details/global-contact-details-custom-fields/global-contact-details-custom-fields.component';
import { GlobalContactDetailsAssociatedListsComponent } from './global-contact-details/global-contact-details-associated-lists/global-contact-details-associated-lists.component';
import { GlobalContactListItemRenameModalComponent } from './global-contact-list/global-contact-list-item/global-contact-list-item-rename-modal/global-contact-list-item-rename-modal.component';
import { GlobalContactDetailsOrganizationInfoComponent } from './global-contact-details/global-contact-details-organization-info/global-contact-details-organization-info.component';
import { AllGlobalContactsItemComponent } from './all-global-contacts/all-global-contacts-item/all-global-contacts-item.component';
import { AllGlobalContactsComponent } from './all-global-contacts/all-global-contacts.component';
import { GlobalContactExportToGuestModalComponent } from './global-contact-export-to-guest-modal/global-contact-export-to-guest-modal.component';
import { DialogAnimationService } from '../services/dialog-animation.service';

const routes = [
  {
    path: '',
    component: GlobalContactComponent,
    canLoad: [AuthGuard],
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        component: GlobalContactListComponent,
      },
      {
        path: 'all-contacts',
        canActivateChild: [AuthGuard],
        component: AllGlobalContactsComponent,
      },
      {
        path: 'add-contact',
        canActivateChild: [AuthGuard],
        component: GlobalContactAddComponent,
      },
      {
        path: 'upload-contact-list',
        canActivateChild: [AuthGuard],
        component: GlobalContactListUploadComponent,
        canDeactivate: [CanExitGuard]
      },
      {
        path: ':lid',
        canActivateChild: [AuthGuard],
        component: GlobalContactListContactsComponent,
      },
      {
        path: ':lid/contact/:id',
        canActivateChild: [AuthGuard],
        component: GlobalContactDetailsComponent,
        children: [{
          path: 'personal-info',
          canActivateChild: [AuthGuard],
          component: GlobalContactDetailsReservedFieldsComponent,
          data: { expectedRoles: [RoleType.TenantAdmin]},
        },
        {
          path: 'organization-info',
          canActivateChild: [AuthGuard],
          component: GlobalContactDetailsOrganizationInfoComponent,
          data: { expectedRoles: [RoleType.TenantAdmin]},
        },
        {
          path: 'custom-fields',
          canActivateChild: [AuthGuard],
          component: GlobalContactDetailsCustomFieldsComponent,
          data: { expectedRoles: [RoleType.TenantAdmin]},
        },
        {
          path: 'associated-lists',
          canActivateChild: [AuthGuard],
          component: GlobalContactDetailsAssociatedListsComponent,
          data: { expectedRoles: [RoleType.TenantAdmin]},
        }]
      }
    ]
  }];


@NgModule({
  declarations: [GlobalContactComponent,
     GlobalContactListComponent,
     AllGlobalContactsComponent,
     GlobalContactCreateComponent,
     GlobalContactListItemComponent,
     AllGlobalContactsItemComponent,
     GlobalContactAddComponent,
     GlobalContactListUploadComponent,
     UploadCsvStepGlobalContactComponent,
     MapFieldStepGlobalContactComponent,
     ValidateDataStepGlobalContactComponent,
     GlobalContactListContactsComponent,
     GlobalContactListContactsItemComponent,
     GlobalContactDetailsComponent,
     GlobalContactDetailsReservedFieldsComponent,
     GlobalContactDetailsCustomFieldsComponent,
     GlobalContactDetailsAssociatedListsComponent,
     GlobalContactListItemRenameModalComponent,
     GlobalContactDetailsOrganizationInfoComponent,
     GlobalContactExportToGuestModalComponent],
     
  imports: [
    CommonModule,
    MainModule,
    MatTableModule,
    RouterModule.forChild(routes)
  ],
  providers: [DialogAnimationService],
  entryComponents : [
    GlobalContactExportToGuestModalComponent,
    GlobalContactListItemRenameModalComponent,
    GlobalContactListItemComponent,
    AllGlobalContactsItemComponent,
    GlobalContactCreateComponent,
    GlobalContactListContactsItemComponent]
})
export class GlobalContactModule { }
