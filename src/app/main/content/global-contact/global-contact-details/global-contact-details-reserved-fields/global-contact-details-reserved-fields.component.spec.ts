import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactDetailsReservedFieldsComponent } from './global-contact-details-reserved-fields.component';

describe('GlobalContactDetailsReservedFieldsComponent', () => {
  let component: GlobalContactDetailsReservedFieldsComponent;
  let fixture: ComponentFixture<GlobalContactDetailsReservedFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactDetailsReservedFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactDetailsReservedFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
