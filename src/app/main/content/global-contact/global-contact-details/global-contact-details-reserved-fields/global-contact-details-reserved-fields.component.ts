import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { RegexpPattern, Section } from 'app/constants/constants';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { fuseAnimations } from '@fuse/animations';
import { EventGuestService } from 'app/main/content/event/event-layout/event-guest/event-guest.service';
import { EventGuestDataService } from 'app/main/content/event/event-layout/event-guest/event-guest-data.service';
import { DateTimeAdapter } from 'ng-pick-datetime';



@Component({
  selector: 'irms-global-contact-details-reserved-fields',
  templateUrl: './global-contact-details-reserved-fields.component.html',
  styleUrls: ['./global-contact-details-reserved-fields.component.scss'],
  animations: fuseAnimations
})
export class GlobalContactDetailsReservedFieldsComponent implements OnInit {
  // lookups
  public titles = [];
  public nationalities = [];
  public documentTypes = [];
  public issuingCountries = [];
  loading: boolean;
  public guestInfoForm = this.fb.group({
    id: [''],
    title: [''],
    fullName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    preferredName: ['', [Validators.minLength(2), Validators.maxLength(100)]],
    gender: [''],
    documentNumber: [''],
    expirationDate: [''],
    email: ['', [Validators.required, Validators.pattern(RegExp(RegexpPattern.email))]],
    alternativeEmail: ['', [Validators.pattern(RegExp(RegexpPattern.email))]],
    mobileNumber: ['', [Validators.required]],
    secondaryMobileNumber: [''],
    workNumber: [''],
    nationalityId: [''],
    documentTypeId: [''],
    issuingCountryId: ['']
  });
  editInfoMode: boolean = false;
  contactId; 
  guestListId;
  model;
  min = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate(),23,59);//minimum date that can be used for expiration date

  constructor(
    public sectionService: EventGuestService,
    protected dataService: EventGuestDataService,
    protected router: Router,
    protected route: ActivatedRoute,
    private appStateService: AppStateService,
    protected fb: FormBuilder,
    dateTimeAdapter: DateTimeAdapter<any>) {
      dateTimeAdapter.setLocale('en-GB')
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.contactId = params['id'];
      this.guestListId = params['lid'];
      this.loadContact();
    })
    this.loadLookups();
  }

  updateGuestInfo() {
    // campaign id param
    this.dataService.getEventGuestBasicInfo(this.guestListId, this.contactId).subscribe(data => {
      this.sectionService.setGuestInfo(data)
      this.sectionService.setSelectedGuestName(data.fullName);
    });    
  }

  loadContact() {
    this.dataService.get(this.contactId)
    .subscribe(x => {
      this.guestInfoForm.patchValue(x);
      this.model=x;
    })
  }

  loadLookups(): void {
    this.appStateService.GetNationalities().subscribe((result) => {
      this.loading = false;
      this.nationalities = result;
    }, error => {
      this.loading = false;
    });
    this.appStateService.GetDocumentTypes().subscribe((result) => {
      this.loading = false;
      this.documentTypes = result;
    }, error => {
      this.loading = false;
    });
    this.appStateService.getCountries().subscribe((result) => {
      this.loading = false;
      this.issuingCountries = result;
    }, error => {
      this.loading = false;
    });
  }

    ////////////////// Save Changes ////////////////////
  public updateEventInfo(): void {
    if (this.guestInfoForm.valid) {
      const model = this.sectionService.trimValues(this.guestInfoForm.value);
      this.loading = true;
      this.dataService.updatePersonalInfo(model).subscribe((e: any) => {
        this.loadContact();
        this.sectionService.setSelectedGuestName(e.name);
        this.viewGuestInfo();
        this.updateGuestInfo();
        this.loading = false;
      }, () => {
        this.loading = false;
      }
      );
    }
  }

  /// Handle Edit Info Tab 
  public editInfo(): void {
    this.guestInfoForm.patchValue(this.model);
    this.editInfoMode = true;
  }

  public viewGuestInfo(): void {
    this.editInfoMode = false;
  }

  expirationDateChanged(): void { }

  getCountry(id): any {
    if (!this.issuingCountries) {
      return '...';
    }
    const country = this.issuingCountries.find(item => item.id === id);
    if (country) {
      return country.value;
    }
    else { return null; }
  }
  getNationality(id): any {
    if (!this.nationalities) {
      return '...';
    }
    const country = this.nationalities.find(item => item.id === id);
    if (country) {
      return country.nationality;
    }
    else { return null; }
  }
  getDocumentType(id): any {
    if (!this.documentTypes) {
      return '...';
    }
    const doc = this.documentTypes.find(item => item.id === id);
    if (doc) {
      return doc.name;
    }
    else { return null; }
  }

  checkPhone(control) {
    if (control.valid && control.dirty) {
      this.dataService.checkPhoneUniqueness({ mobileNumber: control.value }).subscribe((res: any) => {
        if (res) {
          control.setErrors({ notUnique: true });
        } else {
          this.guestInfoForm.updateValueAndValidity();
        }
      });
    }
  }

  checkEmail(control) {
    if (control.valid && control.dirty) {
      this.dataService.checkEmailUniqueness({ email: control.value }).subscribe((res: any) => {
        if (res) {
          control.setErrors({ notUnique: true });
        } else {
          this.guestInfoForm.updateValueAndValidity();
        }
      });
    }
  }

}
