import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalContactDetailsCustomFieldsComponent } from './global-contact-details-custom-fields.component';

describe('GlobalContactDetailsCustomFieldsComponent', () => {
  let component: GlobalContactDetailsCustomFieldsComponent;
  let fixture: ComponentFixture<GlobalContactDetailsCustomFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactDetailsCustomFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactDetailsCustomFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
