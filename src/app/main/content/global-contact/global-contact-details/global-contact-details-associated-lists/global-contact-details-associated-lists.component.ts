import { Component, OnInit } from '@angular/core';
import { GlobalContactService } from '../../global-contact.service';
import { GlobalContactDataService } from '../../global-contact-data.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppStateService } from 'app/main/content/services/app-state.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'irms-global-contact-details-associated-lists',
  templateUrl: './global-contact-details-associated-lists.component.html',
  styleUrls: ['./global-contact-details-associated-lists.component.scss'],
  animations:fuseAnimations
})
export class GlobalContactDetailsAssociatedListsComponent implements OnInit {
  assocList = [];
  id = 1;
  constructor(public sectionService: GlobalContactService,
    protected dataService: GlobalContactDataService,
    protected router: Router,
    protected route: ActivatedRoute) {
     }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
            this.id = params['id'];
            this.dataService.getContactAssociatedLists(this.id).subscribe(data => {
              this.assocList = data;
            });
          });
  }
}
