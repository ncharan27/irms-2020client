import { Injectable } from '@angular/core';
import { SectionService } from '../components/shared/section/section.service';
import { GlobalContactModel } from './global-contact.model';
import { GlobalContactDataService } from './global-contact-data.service';
import { QueryService } from '../services/query.service';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalContactService extends SectionService<GlobalContactModel> {
  private selectedContactName = new BehaviorSubject<any>('');
  constructor(
      protected dataService: GlobalContactDataService,
      queryService: QueryService,
      protected router: Router
  ) {
      super(dataService, queryService, router);
  }
  
  public getSelectedContactName(): Observable<any> {
    return this.selectedContactName.asObservable();
  }
  
  // Contact name observables
  public setSelectedContactName(name: string): void {
    this.selectedContactName.next(name);
  }
}