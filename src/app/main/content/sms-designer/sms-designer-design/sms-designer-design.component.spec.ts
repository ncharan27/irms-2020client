import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDesignerDesignComponent } from './sms-designer-design.component';

describe('SmsDesignerDesignComponent', () => {
  let component: SmsDesignerDesignComponent;
  let fixture: ComponentFixture<SmsDesignerDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDesignerDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDesignerDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
