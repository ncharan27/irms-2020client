import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDesignerPreviewComponent } from './sms-designer-preview.component';

describe('SmsDesignerPreviewComponent', () => {
  let component: SmsDesignerPreviewComponent;
  let fixture: ComponentFixture<SmsDesignerPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDesignerPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDesignerPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
