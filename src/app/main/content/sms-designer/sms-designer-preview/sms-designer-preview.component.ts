import { Component, OnInit, OnDestroy } from '@angular/core';
import { SmsDesignerService } from '../sms-designer.service';
import { Subscription, Subject } from 'rxjs';

@Component({
  selector: 'irms-sms-designer-preview',
  templateUrl: './sms-designer-preview.component.html',
  styleUrls: ['./sms-designer-preview.component.scss']
})
export class SmsDesignerPreviewComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  saveLoading = false;
  private unsubscribeAll: Subject<any>;
  templateData: any;

  constructor(public templateService: SmsDesignerService) {
    this.unsubscribeAll = new Subject();
    this.subscription = this.templateService.getTemplate().subscribe(x => {
      this.templateData = x;
   });
   }

  ngOnInit(): void {
    this.templateData = this.templateService.template;
  }
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

}
