import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { SectionService } from '../components/shared/section/section.service';
import { SmsDesignerModel } from './sms-designer.model';
import { QueryService } from '../services/query.service';
import { Router } from '@angular/router';
import { SmsDesignerDataService } from './sms-designer-data.service';

@Injectable({
  providedIn: 'root'
})
export class SmsDesignerService extends SectionService<SmsDesignerModel>{
  
  public templateId;
  public eventId;
  public campaignId;
  public fieldsFilled = new BehaviorSubject<boolean>(false);
  public unstagedChanges = new BehaviorSubject<boolean>(false);
  public invitationId = new BehaviorSubject<string>('');

  private templateData = new Subject<any>();
  private save = new Subject<any>();
  private sendTest = new Subject<any>();
  private refreshForm = new Subject<any>();
  private landingPage = new Subject<any>();
  private previewTemplate = new Subject<any>();

  private editingComponentState = 'component'; // Can take value 'component' or 'dialog' to indicate which component is currently active

  public template;
  rvspFormValue: any;
  constructor(
    protected dataService: SmsDesignerDataService,
    queryService: QueryService,
    protected router: Router
  ) {
    super(dataService, queryService, router);
    this.template = {
      senderName: '',
      body: '',
    };
  }
  setCurrentComponentState( state ): void {
    this.editingComponentState = state;
  }

  getCurrentComponentState(): any {
    return this.editingComponentState;
  }
  
  setTemplate(data: any): void {
    this.templateData.next(data);
    this.template = data;
  }

  getTemplate(): Observable<any> {
    return this.templateData.asObservable();
  }

  triggerSave(): void {
    this.save.next();
  }

  saveObs(): Observable<any> {
    return this.save.asObservable();
  }

 // Send test
 triggerSendTest(data: any): void {
  this.sendTest.next(data);
}

sendTestObs(): Observable<any> {
  return this.sendTest.asObservable();
}

// Refreshing Form
triggerRefresh(): void {
  this.refreshForm.next();
}

getRefreshTrigger(): Observable<any> {
  return this.refreshForm.asObservable();
}

// Landing Page Trigger
triggerLandingPage(): void {
  this.landingPage.next();
}

triggerLandingPageObs(): Observable<any> {
  return this.landingPage.asObservable();
}

// Preview Slider Trigger
triggerPreview(): void {
  this.previewTemplate.next();
}

triggerPreviewObs(): Observable<any> {
  return this.previewTemplate.asObservable();
}

initDefaultForm(): void {
  const sms = this.template;
  sms.senderName = '',
    sms.body = '';
  this.setTemplate(sms);
  this.rvspFormValue = {
    formContent: {
      welcome: {
        content: `<p style="text-align: left;"><span style="font-family: arial, helvetica, sans-serif;">You are invited to</span></p>
            <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
            <p style="text-align: left;"><span style="color: #000000; font-family: tahoma, arial, helvetica, sans-serif;">on 23rd April 2020, 6:00 PM</span></p>
            <h5 style="text-align: left;"><span style="color: #34495e;"><em><span style="font-family: tahoma, arial, helvetica, sans-serif;">Please let us know if you will be joining us</span></em></span></h5>`,
        button: 'Proceed'
      },
      rsvp: {
        content: `<p><span style="font-family: tahoma, arial, helvetica, sans-serif;">Do you accept the Invitation to the&nbsp;</span></p>
            <h2 style="text-align: left;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
            <p><span style="color: #34495e; font-family: 'arial black', sans-serif;">Select your Response:&nbsp;</span></p>`,
        acceptButton: `Yes I'll be there`,
        rejectButton: ` Nope I won't be there`
      },
      accept: {
        content: `<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
            <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We will see you at the&nbsp;</span></p>
            <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
            <p style="text-align: center;">&nbsp;</p>`,
      },
      reject: {
        content: `<p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">Thank you for your response!</span></p>
            <p style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;">We were expecting you to see at the&nbsp;&nbsp;</span></p>
            <h2 style="text-align: center;"><span style="background-color: #236fa1; color: #ecf0f1; font-family: 'arial black', sans-serif;">&nbsp;The Fanciest Event Ever&nbsp;</span></h2>
            <p style="text-align: center;">Perhaps we'll see you some other time! <br> Stay fancy as you are!</p>`
      }
    },
    formTheme: {
      buttons: {
        background: '#576268',
        text: '#fff',
      },
      background: {
        color: '#fff',
        image: '',
        style: '',
      },
    },
    formSettings: {
      title: ''
    }
  };
}
}
