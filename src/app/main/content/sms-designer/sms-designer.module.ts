import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmsDesignerComponent } from './sms-designer.component';
import { AuthGuard } from 'app/auth/guard/auth.guard';
import { RoleType } from 'app/constants/constants';
import { SmsDesignerDesignComponent } from './sms-designer-design/sms-designer-design.component';
import { SmsDesignerPreviewComponent } from './sms-designer-preview/sms-designer-preview.component';
import { SmsDesignerFormComponent } from './sms-designer-form/sms-designer-form.component';
import { SmsDesignerNavbarComponent } from './sms-designer-navbar/sms-designer-navbar.component';
import { SmsDesignerSendTestSmsComponent } from './sms-designer-send-test-sms/sms-designer-send-test-sms.component';
import { MainModule } from 'app/main/main.module';
import { ContentModule } from 'app/layout/components/content/content.module';
import { FuseNavigationModule } from '@fuse/components';
import { LayoutModule } from '@angular/cdk/layout';
import { EditorModule } from '@tinymce/tinymce-angular';
import { RouterModule } from '@angular/router';
import { SmsDesignerService } from './sms-designer.service';
import { RsvpFormEditorModule } from '../components/shared/rsvp-form-editor/rsvp-form-editor.module';
import { CanExitGuard } from '../services/can-exit.guard';

const routes = [
  {
    path: '',
    redirectTo: `:tempId`,
    pathMatch: 'full'
  },
  {
    path: ':tempId',
    component: SmsDesignerComponent, 
    canLoad: [AuthGuard], 
    data: { expectedRoles: [RoleType.TenantAdmin] },
    children: [
      {
        path: 'rsvp-form',
        canActivateChild: [AuthGuard],
        component: SmsDesignerFormComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,
        canDeactivate: [CanExitGuard]
      },
      {
        path: 'design',
        canActivateChild: [AuthGuard],
        component: SmsDesignerDesignComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,        
        canDeactivate: [CanExitGuard]
      },
      {
        path: 'preview',
        canActivateChild: [AuthGuard],
        component: SmsDesignerPreviewComponent,
        data: { expectedRoles: [RoleType.TenantAdmin] },
        skipLocationChange: true,
      },
    ]
  }];

@NgModule({
  declarations: [
    SmsDesignerComponent,
    SmsDesignerDesignComponent,
    SmsDesignerPreviewComponent,
    SmsDesignerFormComponent,
    SmsDesignerNavbarComponent],
  imports: [
    MainModule,
    ContentModule,
    RsvpFormEditorModule,
    CommonModule,
    // FuseNavigationModule,
    LayoutModule,
    EditorModule,
    // RouterModule.forChild(routes)
  ],
  entryComponents: [SmsDesignerSendTestSmsComponent],
  providers: [SmsDesignerService],
  exports: [
    SmsDesignerDesignComponent,
  ],
})
export class SmsDesignerModule { }
