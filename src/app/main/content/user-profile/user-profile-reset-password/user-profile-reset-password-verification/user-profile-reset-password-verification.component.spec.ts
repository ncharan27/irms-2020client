import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileResetPasswordVerificationComponent } from './user-profile-reset-password-verification.component';

describe('UserProfileResetPasswordVerificationComponent', () => {
  let component: UserProfileResetPasswordVerificationComponent;
  let fixture: ComponentFixture<UserProfileResetPasswordVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileResetPasswordVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileResetPasswordVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
