import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileAccountChangePasswordModalComponent } from './user-profile-account-change-password-modal.component';

describe('UserProfileAccountChangePasswordModalComponent', () => {
  let component: UserProfileAccountChangePasswordModalComponent;
  let fixture: ComponentFixture<UserProfileAccountChangePasswordModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileAccountChangePasswordModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileAccountChangePasswordModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
