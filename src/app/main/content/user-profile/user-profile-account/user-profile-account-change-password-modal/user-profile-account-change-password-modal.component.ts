import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { RegexpPattern } from 'app/constants/constants';
import { UserProfileDataService } from '../../user-profile-data.service';

@Component({
  selector: 'irms-user-profile-account-change-password-modal',
  templateUrl: './user-profile-account-change-password-modal.component.html',
  styleUrls: ['./user-profile-account-change-password-modal.component.scss']
})
export class UserProfileAccountChangePasswordModalComponent implements OnInit {

  public passChangeForm = this.fb.group({
    oldPassword: ['', [Validators.required, Validators.minLength(8),Validators.pattern(RegExp(RegexpPattern.password))]],
    newPassword: ['', [Validators.required, Validators.minLength(8),Validators.pattern(RegExp(RegexpPattern.password))]],
    confirmNewPassword: ['', [Validators.required, Validators.minLength(8),Validators.pattern(RegExp(RegexpPattern.password))]]},
    { validator: this.isMatch('newPassword', 'confirmNewPassword') });
  isLoading: boolean;
  
  constructor(
    public dataService: UserProfileDataService,
    public fb: FormBuilder,
    public dialogRef: MatDialogRef<UserProfileAccountChangePasswordModalComponent>,) { }

  ngOnInit() {
  }

  confirm(){
    this.isLoading = true;
    let modal = {
      oldPassword: this.passChangeForm.controls.oldPassword.value,
      newPassword: this.passChangeForm.controls.newPassword.value,
      confirmNewPassword: this.passChangeForm.controls.confirmNewPassword.value,
    }
    this.dataService.updatePassword(modal).subscribe(res=>{
      if(res){
        this.isLoading = false;
        this.dialogRef.close();
      }
    },err => this.isLoading = false)

  }

  cancel(){
    this.dialogRef.close();

  }

  private isMatch(first, second) {
    return (group: FormGroup): { [key: string]: any } => {
      if ((group.controls[first].value !== group.controls[second].value) &&
        ((group.controls[first].value !== '' && group.controls[first].value !== null) && (group.controls[second].value !== '' && group.controls[second].value !== null))
      ) {
        return {
          isMatch: true
        };
      }
      return null;
    };
  }

}
