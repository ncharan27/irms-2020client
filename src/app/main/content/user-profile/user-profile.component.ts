import { Component, OnInit } from '@angular/core';
import { UserProfileService } from './user-profile.service';
import { UserProfileDataService } from './user-profile-data.service';
import { ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { CurrentUserService } from '../services/current-user.service';

@Component({
  selector: 'irms-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  animations: fuseAnimations
})
export class UserProfileComponent implements OnInit {
  
  user = {
    fullName: "John Doe",
    avatarId: "http://localhost:2500/assets/images/avatars/Velazquez.jpg",
    role: "Admin"
  }
  public tabGroup = [
    {
      label: 'Account',
      link: './account'
    },
    // {
    //   label: 'Change Password',
    //   link: './reset'
    // }
  ];

  constructor(
    public service: UserProfileService,
    public dataService: UserProfileDataService,
    public currentUser: CurrentUserService

  ) { }

  ngOnInit() {
    this.currentUser.getCurrentUser().subscribe(data => {
      //API Integration Below

    });
  }

}
