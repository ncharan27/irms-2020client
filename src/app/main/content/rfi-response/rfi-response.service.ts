import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RfiResponseService {
  private step = new BehaviorSubject<number>(0);

  constructor() { 
  }

  getStep(): Observable<number> {
    return this.step.asObservable();
  }

  setStep(step): void {
    this.step.next(step);
  }

  getStepInstantValue(): number {
    return this.step.value;
  }
}
