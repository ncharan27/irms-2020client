import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RfiResponseService } from './rfi-response.service';
import { RfiResponseDataService } from './rfi-response-data.service';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { FormService } from '../components/shared/form/form.service';
import { rfiType } from 'app/constants/constants';

@Component({
  selector: 'irms-rfi-response',
  templateUrl: './rfi-response.component.html',
  styleUrls: ['./rfi-response.component.scss']
})
export class RfiResponseComponent implements OnInit {
  responseMediaId = '';
  message = 'Loading...';
  isLoading = true;
  image = '';

  step = 0;
  saveLoading = false;
  welcome: any;
  thanks: any;
  theme: any;
  submit: any;
  questions: any[];
  answers: FormControl[] = [];
  rfiFormType: rfiType;
  id = '';
  mediaType: any;
  formFormatted: any = {};
  isListAnalysis: boolean;
  redirectRsvp: any;
  constructor(
    private route: ActivatedRoute,
    private dataService: RfiResponseDataService,
    private sectionService: RfiResponseService,
    protected formPreview: FormService,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.responseMediaId = params['tempId'];
      if (!this.responseMediaId) {
        this.message = 'Please provide a valid id';
      }
      this.dataService.get(this.responseMediaId).subscribe(response => {
        if (response.error) {
          this.message = response.error;
        } else {

          this.id = response.campaignInvitationResponseId;
          this.mediaType = response.mediaType;
          const questions = response.questions.map(question => {
            const questionObject = JSON.parse(question.question);
            questionObject.response = question.response;
            return questionObject;
          });
          
          // check is rfi is of list analysis type; if it is then sort missing required fields to top of form
          let sortedQuestions = [];
          if (response.rfiType === rfiType.listAnalysis) {
            this.sortQuestions(questions).forEach(x=>{sortedQuestions.push(x)});
            this.isListAnalysis = true;
          }

          this.formFormatted.formContent = {
            welcome: JSON.parse(response.welcome),
            questions: this.isListAnalysis ? sortedQuestions : questions,
            submit: JSON.parse(response.submit),
            thanks: JSON.parse(response.thanks),
          };
          this.formFormatted.formTheme = JSON.parse(response.formTheme);
          this.formFormatted.formSettings = JSON.parse(response.formSettings);
          this.formPreview.setForm(this.formFormatted);
          if (response.backgroundImageUrl) {
            this.formFormatted.formTheme.background.image = response.backgroundImageUrl;
          }
          this.isLoading = false;
        }
      });
    });
    
    if (this.route.snapshot.queryParams['redirect_rsvp']) {
      // if RFI was redirected from an invitation/RSVP
      this.redirectRsvp = this.route.snapshot.queryParams['redirect_rsvp'];     
    }

    this.sectionService.getStep().subscribe(step => {
      this.step = step;
    });
  }

  submitData(formData): void {
    this.saveLoading = true;
    // submit
    if (formData === null) {
      this.saveLoading = false;
      return;
    }
    // 

    const answerValues = [];
    Object.keys(formData).forEach(quesId => {
      answerValues.push({
        id: quesId,
        answer: (typeof formData[quesId] === 'string') ? formData[quesId] : JSON.stringify(formData[quesId])
      });
    });
    const regExp = RegExp(`[0-9]{2}\/[0-9]{2}\/[0-9]{4}`);
    answerValues.forEach(x => {
      if (regExp.test(x.answer)) {
        x.answer = `${x.answer.substring(3, 5)}/${x.answer.substring(0, 2)}/${x.answer.substring(6, 10)}`;
        let a: any = new Date(x.answer);
        a.setHours(10, 0, 0, 0);
        a = a.toISOString();
        x.answer = a;
      }
    });
    const data = {
      id: this.id,
      mediaType: this.mediaType,
      answers: answerValues
    };

    this.dataService.save(data).subscribe(x => {
      this.saveLoading = false;
      this.formPreview.setCurrentSection('thanks');
      // Redirecting back to source RSVP if redirection occured before
      if (this.redirectRsvp != null) {
        setTimeout(() => { 
          this.router.navigate([`response/${this.redirectRsvp}`]);
        }, 4000);
      }
    },
      error => {
        this.saveLoading = false;
      });
  }

  sortQuestions(questions): any {
    const questionsRequiredNoValue = []; // to track required questions with missing value
    const questionsOptionalNoValue = []; // to track optional questions with missing value
    const questionsHasValue = []; // to track all other questions with existing value
    const sortedQuestions = []; // final sort result
    questions.forEach((question, index) => {
      if (question.required && (question.response === undefined 
        || question.response === null 
        || question.response === '' )) {
          
        questionsRequiredNoValue.push(index);

      } else if (!question.required && (question.response === undefined 
        || question.response === null 
        || question.response === '' )) {

        questionsOptionalNoValue.push(index);

      } else {
        questionsHasValue.push(index);
      }
    });
    if (questionsRequiredNoValue.length > 0) {
      questionsRequiredNoValue.forEach(index => sortedQuestions.push(questions[index]));
    }
    if (questionsOptionalNoValue.length > 0) {
      questionsOptionalNoValue.forEach(index => sortedQuestions.push(questions[index]));
    }
    if (questionsHasValue.length > 0) {
      questionsHasValue.forEach(index => sortedQuestions.push(questions[index]));
    }
    return sortedQuestions;
  }
}
