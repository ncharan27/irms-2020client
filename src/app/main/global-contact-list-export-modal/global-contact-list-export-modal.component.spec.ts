import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GlobalContactListExportModalComponent } from './global-contact-list-export-modal.component';


describe('GlobalContactListExportModalComponent', () => {
  let component: GlobalContactListExportModalComponent;
  let fixture: ComponentFixture<GlobalContactListExportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalContactListExportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalContactListExportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
