import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GlobalContactDataService } from '../content/global-contact/global-contact-data.service';


@Component({
  selector: 'irms-global-contact-list-export-modal',
  templateUrl: './global-contact-list-export-modal.component.html',
  styleUrls: ['./global-contact-list-export-modal.component.scss']
})
export class GlobalContactListExportModalComponent implements OnInit {
  formGroup: FormGroup;
  all = true;
  fieldNames = ['Title', 'FullName', 'PreferredName', 'Gender', 'Email',
    'AlternativeEmail', 'MobileNumber', 'SecondaryMobileNumber',
    'Nationality', 'DocumentType', 'DocumentNumber', 'IssuingCountry',
    'ExpirationDate', 'Organization', 'Position', 'IsGuest',
    'CreatedBy', 'CreatorEmail', 'CreatedOn', 'ModifiedBy',
    'ModifierEmail', 'ModifiedOn'];
  customFieldNames = [];

  public custList;


  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<GlobalContactListExportModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, protected dataService: GlobalContactDataService) {
    this.formGroup = this.formBuilder.group({
      title: [true],
      fullName: [true],
      preferredName: [true],
      gender: [true],
      email: [true],
      alternativeEmail: [true],
      mobileNumber: [true],
      workNumber: [true],
      eventName: [true],
      secondaryMobileNumber: [true],
      nationality: [true],
      documentType: [true],
      documentNumber: [true],
      issuingCountry: [true],
      expirationDate: [true],
      organization: [true],
      position: [true],
      isGuest: [true],
      createdBy: [true],
      creatorEmail: [true],
      createdOn: [true],
      modifiedBy: [true],
      modifierEmail: [true],
      modifiedOn: [true],
      custFields: new FormArray([])
    });
  }

  ngOnInit() {
    this.dataService.getAllCustomFields().subscribe(data => {
      this.custList = data;
      this.customFieldNames = data.map(value => value.name);
      this.addFields(data);
      if (this.data && this.data.isFromContactModule) {
        this.formGroup.controls['eventName'].setValue(false);
      }
    });
  }

  addFields(data) {
    data.forEach(element => {
      this.t.controls.push(new FormControl(true));
    });
  }


  close(): void {
    this.dialogRef.close();
  }

  onAllChange(event): void {
    if (!event) {
      this.formGroup.reset();
    } else {
      Object.keys(this.formGroup.controls).forEach(key => {
        if (key === 'custFields') {
          this.t.controls.forEach(element => {
            element.setValue(true);
          });
        } else {
          this.formGroup.controls[key].setValue(true);
        }
      });
    }
  }

  confirm(): void {
    const columns = [];
    const customFields = [];
    Object.keys(this.formGroup.controls).forEach(key => {
      if (key === 'custFields') {
        this.t.controls.forEach((element, i) => {
          if (element.value) {
            customFields.push(this.custList[i].id);
          }
        });
      } else if (this.formGroup.controls[key].value) {
        columns.push(key[0].toUpperCase() + key.substring(1));
      }
    });
    this.dialogRef.close({
      columns: columns,
      customFieldIds: customFields
    });
  }

  get f(): any { return this.formGroup.controls; }
  get t(): any { return this.f.custFields as FormArray; }
}