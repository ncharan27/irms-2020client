import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { RoleType } from '../../constants/constants';
import { TokenService } from '../services/token.service';
import { AuthConfig } from '../auth.config';
import { AuthenticationService } from '../services/authentication.service';
import { appInjector } from '../../main/app-injector';
import { ToasterService } from 'angular5-toaster/dist/angular5-toaster';


@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private config: AuthConfig, private tokenService: TokenService, private authService: AuthenticationService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const expectedRoles: Array<RoleType> = route.data.expectedRoles;
    return this.checkUser(expectedRoles, state.url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): boolean {
    const url = `/${route.path}`;
    const expectedRoles: Array<RoleType> = route.data.expectedRoles;
    return this.checkUser(expectedRoles, url);
  }

  private checkUser(expectedRoles: Array<RoleType>, url: string): boolean {
    const token = this.tokenService.getToken();
    // by pass two factor authentication
    const otpStatus = true; // this.tokenService.getOtpStatus();
    if (token && token.token && otpStatus) {
    // if (token && token.token) {
      const decodeToken = token.decodeToken();
      const tokenRole: RoleType = RoleType[<string>decodeToken.role];
      if (expectedRoles.includes(tokenRole)) {
        return true;
      }
    }

    if (token) {
      const toasterService = appInjector().get(ToasterService);
      toasterService.pop('error', null, 'Insufficient permissions.');
    }
    else {
      // Navigate to the login page with extras
      const redirectUrl = this.config.guards.loggedInGuard.redirectUrl;
      this.router.navigate([redirectUrl], { queryParams: { returnUrl: url } });
    }
    return false;
  }
}
